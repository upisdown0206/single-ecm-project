import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import AppRouter from "components/AppRouter";
import NotiStackProvider from "components/common/NotiStackProvider";

function App() {
  return (
    <NotiStackProvider>
      <Router>
        <AppRouter />
      </Router>
    </NotiStackProvider>
  );
}

export default App;
