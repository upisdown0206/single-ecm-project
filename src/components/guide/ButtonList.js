import React from "react";
import Button from "components/common/Button";

const ButtonList = () => {
  return (
    <>
      <h1 style={{ fontSize: "40px" }}>button</h1>
      <Button>확인</Button>
      <Button outline>확인</Button>
      <Button color="primary">확인</Button>
      <Button color="primary" outline>
        확인
      </Button>
      <Button color="disabled">확인</Button>
      <Button color="disabled" outline>
        확인
      </Button>
      <Button size="small" color="text-only-gray">
        확인
      </Button>
      <Button size="small" color="text-only-secondary">
        확인
      </Button>
      <Button size="small" color="text-only-primary">
        확인
      </Button>

      <div>
        <Button fullWidth>확인</Button>
        <Button color="primary" fullWidth>
          확인
        </Button>
        <Button color="disabled" fullWidth>
          확인
        </Button>
      </div>
    </>
  );
};

export default ButtonList;
