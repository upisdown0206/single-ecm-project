import React from 'react';
import styled from 'styled-components';
import TabMove from "../common/TabMove";
import Counter from "../common/Counter";
import LoaderComponent from "../common/Loader";
import GearSpinner from "../common/GearSpinner";
import SpinnerComponent from "../common/Spinner";
import MessageBar from "../common/MessageBar";
import GuideTooltip from "../common/GuideTooltip";
import RoundTag from "../common/RoundTag";
import TabInTab from "../common/TabInTab";
import TabInTabButton from "../common/TabInTabButton";
import SignatureCanvasWrap from "../common/SignatureCanvasWrap";
import CarouselType from "../common/CarouselType";
import {IconListSelectThin, IconFail, IconRefresh2} from "../common/Icons";
import imgA from 'asset/images/nfc.gif';
import NoticeAlarm from "../common/NoticeAlarm";
import BMSpRequirementItem from "../workOrder/BM/BMSpRequirementItem";
import EmptyCommon from "../common/EmptyCommon";
import FmTab from "../common/framerMotion/FmTab";
import FmMenu from "../common/framerMotion/FmMenu";
import SwitchButton from "../common/SwitchButton";
import TabAniType from "../common/framerMotion/TabAniType";
const PageListWrap = styled.div`
padding:20px;
overflow-y:auto;
position:absolute;
top:0;
bottom:0;
left:0;
right:0;
h1{
  font-size:18px;
  margin-bottom:10px;
  margin-top:30px;
  &:first-child{
    margin-top:0;
  }
}
ul{
  padding:10px;
  // background:#f2f2f2;
  li{
    margin:10px 0 20px;
    a{
      font-size:12px;
      line-height:15px;
      color:#333333;
      font-weight:500;
      display:flex;
      align-items:center;
      justify-content: space-between;
      span{
        font-size:9px;
        line-height:10px;
        float:right;
        padding:3px;
        line-height:10px;
        border-radius:3px;
        &.sprint1{
          background: #68D6E8;
          color:#ffffff;
        }
        &.sprint2{
          background: #B280FF;
          color:#ffffff;
        }
      }

    }
    &.done{
      a{
        text-decoration:line-through;
      }
    }
  }
  `
  const TextareaBox = styled.pre`
  background:#000;
  width:100%;
  padding:10px;
  min-height:50px;
  margin-top:10px;
  color:#fff;
  font-size:11px;
  `

const ComponentAll = () => {
  return (
  <PageListWrap>

    <FmMenu />
    <FmTab />
    <br />
    <SwitchButton />

    <TabAniType />
      <h1>drag to delete</h1>
      <BMSpRequirementItem />

      <h1>alarm Type</h1>
      {/*사용시 style=position 삭제,top 삭제 */}
      <NoticeAlarm
        whiteType
        alarmTitle={"WO 발생"}
        alarmText={"[PM] WO-123675415:PSTART0205:7999 "}
        style={{position:"relative", top:"0"}}
      />
      <NoticeAlarm
        blackType
        alarmTitle={"WO 발생"}
        alarmText={"[PM] WO-123675415:PSTART0205:7999 "}
        style={{position:"relative", top:"0"}}
      />
      <h1>Carousel Type</h1>
      <CarouselType />
      <h1>Sign Type</h1>
      <SignatureCanvasWrap />
      <h1>Tab</h1>
      <div style={{padding:"20px", background:"#F5F8FA"}}>
        <TabMove div={"div2"}/>
        <TabMove div={"div3"}/>
      </div>

      <TabInTab div={"div3"}>
        <TabInTabButton type="complete" complete title="Failure" />
        <TabInTabButton type="active" title="Check" />
        <TabInTabButton title="Cause &amp; Resolution" />
      </TabInTab>

      <TabInTab>
        <TabInTabButton type="complete" complete title="수량요청" />
        <TabInTabButton type="active" title="SP Issue" />
      </TabInTab>
      <TextareaBox>
      &lt;TabMove /&gt; <br />
      &lt;TabInTab /&gt;
      </TextareaBox>

      <h1>Counter</h1>
      <Counter type="typeCenter" />
      <Counter type="typeWide" size={"12px"} />
      <TextareaBox>
        &lt;Counter /&gt; <br />
        &lt;Counter type="typeWide" size={"12px"} /&gt;
      </TextareaBox>

      <h1>NFC 리딩</h1>
      <div>
        <img src={imgA} />
      </div>

      <h1>Empty</h1>
      <EmptyCommon />
      <h1>Loader</h1>
      <LoaderComponent style={{position: "relative"}} />
      <GearSpinner />
      <SpinnerComponent />
      <TextareaBox>
        &lt;LoaderComponent /&gt; <br />
        &lt;GearSpinner /&gt; <br />
        &lt;SpinnerComponent /&gt; <br />
      </TextareaBox>

      <h1>RoundTag</h1>
      <RoundTag tagName={"round 1"}/>&nbsp;
      <RoundTag tagName={"round 2"}/>
      <br /><br />
      <TextareaBox >
      &lt;RoundTag tagName="" /&gt; <br />
      </TextareaBox>

      <h1>Guide Tooltip</h1>
      <GuideTooltip text={"안내 문구를 작성 할 수 있습니다."} style={{position:"relative",animation:"none",opacity:1}}/>
      <br />
      <GuideTooltip text={"새로추가하시겠습니까?"} style={{position:"relative",animation:"none",opacity:1}}/>

      <TextareaBox>
      &lt; GuideTooltip text={"안내 문구를 작성 할 수 있습니다."} <br />
      position={"클래스 생성"} <br />
      &gt;
      </TextareaBox>

      <h1>Message Bar</h1>
      <MessageBar messageText={"메세지를 작성 할 수 있습니다."} style={{animation:"none",position:"relative"}}/><br />

      <MessageBar
        style={{animation:"none",position:"relative"}}
        iconType={
          <span className="icon-notice-success">
                <IconListSelectThin
                  strokeColor={"#ffffff"}
                  size={"15px"}
                />
              </span>
        }
        messageText={"5건중 3건 업로드 성공!"}
      />

      <div>
        <MessageBar
          style={{animation:"none",position:"relative"}}
          iconType={
            <span className="icon-notice-success">
                      <IconListSelectThin
                        strokeColor={"#ffffff"}
                        size={"15px"}
                      />
                    </span>
          }
          messageText={"Success!"}
        />
      </div>
      <div>
        <MessageBar
          style={{animation:"none",position:"relative"}}
          iconType={
            <span className="icon-notice-success">
                      <IconListSelectThin
                        strokeColor={"#ffffff"}
                        size={"15px"}
                      />
                    </span>
          }
          messageText={"승인되었습니다"}
        />
      </div>

      <MessageBar
        fullWidth
        style={{animation:"none",position:"relative"}}
        iconType={
          <span className="icon-notice-fail">
            <IconFail/>
          </span>
        }
        messageText={"2건의 업로드가 실패하였습니다! "}
        refresh={<span className="btn-try-again">다시시도<IconRefresh2 fill={"#E0205C"}/></span>}
      />
      <MessageBar
        fullWidth
        style={{animation:"none",position:"relative"}}
        messageText={"WO-123675415 OP가 완료되었습니다"}
      />


      <TextareaBox>
      &lt;MessageBar messageText={"메세지를 작성 할 수 있습니다."} &gt;
      </TextareaBox>
    </PageListWrap>
      );
};

  export default ComponentAll;