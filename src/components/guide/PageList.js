import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const GuideList = styled.div`{
  overflow-y:auto;
  position:absolute;
  left:0;
  right:0;
  top:0;
  bottom:0;
`

const PageListWrap = styled.div`
  padding:20px;
  h1{
    font-size:18px;
    margin-bottom:10px;
  }
  ul{
    padding:10px;
    background:#f2f2f2;
    li{
      margin:10px 0 20px;
      a{
        font-size:12px;
        line-height:15px;
        color:#333333;
        font-weight:500;
        display:flex;
        align-items:center;
        justify-content: space-between;
        span{
          font-size:9px;
          line-height:10px;
          float:right;
          padding:3px;
          line-height:10px;
          border-radius:3px;
          &.sprint1{
            background: #68D6E8;
            color:#ffffff;
          }
          &.sprint2{
            background: #B280FF;
            color:#ffffff;
          }
        }
        
      }
      &.done{
      a{
        text-decoration:line-through;
      }
    }
 } 
`;
const PageList = () => {
  return (
    <GuideList>
      <PageListWrap>
        <h1>Common</h1>
        <ul>
          <li>
            <Link to={"./"}>
              Main (technician)
              <br />
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./main-warehouse"}>
              Main (창고담당자)
              <br />
              Menu list(포함)
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./loader-component"}>
              Loader
              <br />
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./spinner-component"}>
              Spinner
              <br />
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./gear-component"}>
              Gear Spinner
              <br />
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
        </ul>
      </PageListWrap>
      <PageListWrap>
        <h1>설비관리 / 기준정보(조회)</h1>
        <ul>
          <li>
            <Link to={"./search-equipment"}>
              기준정보/ 설비관리/ 리스트{" "}
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./search-equipment-form"}>
              기준정보/ 설비관리/ 검색 <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./form-detail/equipmentId"}>
              기준정보/ 설비관리/ 검색 데이터 리스트 <br />
              기준정보/ 설비관리/ 리스트-선택-상세 (포함)
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./search-sp-inventory"}>
              기준정보/ sp재고조회
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./search-sp-inventory-form"}>
              기준정보/ sp재고조회/ 검색
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./search-sp-inventory-detail/BMBOLT000111111111111"}>
              기준정보/ sp재고조회/ 디테일 A
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./search-sp-inventory-detail"}>
              기준정보/ sp재고조회/ 디테일 B
              <span className="sprint1">Sprint1</span>
            </Link>
          </li>
        </ul>
      </PageListWrap>
      <PageListWrap>
        <h1>Work Order List</h1>
        <ul>
          <li>
            <Link to={"./work-order-list"}>
              BM/ WO목록 <span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./work-order-list-search-form"}>
              BM/ 검색<span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./work-order-selectbox-modal"}>
              WO Type Modal<span className="sprint1">Sprint1</span>
            </Link>
          </li>
          <li>
            <Link to={"./work-order-detail/:woNo"}>
              BM/ WO Detail<span className="sprint1">Sprint1</span>
            </Link>
          </li>
        </ul>
        <br />
        <ul>
          <li>
            <Link to={"./work-order-bm-operation-hour"}>
              BM/ Operation hour
              <br />
              BM/Operation hour-datepick
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./work-order-bm-push-history"}>
              BM/ push history<span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./reading-nfc-inprogress"}>
              reading/ nfc/ inprogress<br />
              reading/ nfc/ success
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
        </ul>
        <br />
        <ul>
          <li>
            <Link to={"./"}>
              BM/ Safety Plan/ 1.목록미선택
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Safety Plan/ 2.전체선택
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 1.옵션선택<span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 1.1 옵션선택상세
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 1.2 옵션선택완료
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 1.2 옵션선택완료
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 2.2 사진선택완료
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 3 옵션<span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 3 옵션선택<span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 4 옵션<span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              BM/ Diagnosis/ 3 옵션선택<span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./"}>
              dialogs /삭제<span className="sprint2">Sprint2</span>
            </Link>
          </li>
        </ul>
      </PageListWrap>

      <PageListWrap>
        <h1>일상점검</h1>
        <ul>
          <li>
            <Link to={"./daily-check"}>
              일상점검/ offline/ empty
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-download"}>
              일상점검/ offline/ alert/ 다운로드
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-waiting-inspection"}>
              일상점검/ offline/ 점검대기
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-waiting-inspection-list"}>
              일상점검/ 점검디테일/목록, 선택, 다음목록
              <br />
              Filter & Sort 적용
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-datail-filter-search-form"}>
              일상점검/ 점검디테일/ Filter & Sort 1 ,Sort 2
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-online-list"}>
              일상점검/ online/ list
              <br />
              filter-on
              <br />
              list-select
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-filter-search-form"}>
              일상점검/ online/ filter
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-modal-comments"}>
              일상점검/ 점검디테일/ 코멘트1
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li>
            <Link to={"./daily-check-select"}>
              + 버튼 클릭 시 check detail Result 모달
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li className="done">
            <Link to={"./daily-check-voice-recording"}>
              일상점검/ 점검디테일/ 음성녹음1 (사용안함)
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
          <li className="done">
            <Link to={"./network-disconnect"}>
              일상점검/ alert/ 인터넷연결 (스프린트 외)
              <span className="sprint2">Sprint2</span>
            </Link>
          </li>
        </ul>
      </PageListWrap>
    </GuideList>
  );
};

export default PageList;
