import React from "react";
import styled from "styled-components";

import LabelDivTypeA from "components/search/LabelDivTypeA";
import LabelDivTypeB from "components/search/LabelDivTypeB";
import LabelDivTypeC from "components/search/LabelDivTypeC";
import LabelInputTypeA from "components/search/LabelInputTypeA";
import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeC from "components/search/LabelInputTypeC";
import SearchResultCardInventoryTypeA from "components/search/SearchResultCardInventoryTypeA";
import SearchResultCardInventoryTypeB from "components/search/SearchResultCardInventoryTypeB";
import SearchResultCardTypeA from "components/search/SearchResultCardTypeA";
import SearchResultCardTypeB from "components/search/SearchResultCardTypeB";

const Wrapper = styled.div`
  padding: 10px;
  background: #f1f1f1;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 10px;
`;

const WhiteBox = styled.div`
  padding: 5px;
  background: white;
  border: 1px solid black;
`;

// const HSeperator = styled.div`
//   margin: 0 10px;
//   width: 1px;
//   height: 100%;
//   background: black;
// `;
const WSeperator = styled.div`
  margin: 10px 0;
  height: 1px;
  width: 100%;
  background: black;
`;
const SubTitle = styled.h2`
  font-size: 1rem;
`;
const Description = styled.p``;

const ComponentGuideSeo = () => {
  return (
    <>
      <SubTitle>Search</SubTitle>
      <Wrapper>
        <WhiteBox>
          <SubTitle>components/search/LabelDivTypeA</SubTitle>
          <Description>props : label, value</Description>
          <WSeperator />
          <LabelDivTypeA label={"label"} value={"value"} />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/LabelDivTypeB</SubTitle>
          <Description>props : label, value</Description>
          <WSeperator />
          <LabelDivTypeB label={"label"} value={"value"} />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/LabelDivTypeC</SubTitle>
          <Description>props : label, value</Description>
          <WSeperator />
          <LabelDivTypeC label={"label"} value={"value"} />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/LabelInputTypeA</SubTitle>
          <Description>
            props : labelText, keywords, keywordType, onInputClick,
            onToggleFocus
          </Description>
          <WSeperator />
          <LabelInputTypeA labelText={"label"} />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/LabelInputTypeB</SubTitle>
          <Description>props : labelText, onToggleFocus</Description>
          <WSeperator />
          <LabelInputTypeB labelText={"label"} />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/LabelInputTypeC</SubTitle>
          <Description>props : labelText, onToggleFocus</Description>
          <WSeperator />
          <LabelInputTypeC labelText={"label"} />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/SearchResultCardInventoryTypeA</SubTitle>
          <Description>
            props : itemCode, storeroom, description, quantity,
            quantityAvailable, thumbnail
          </Description>
          <WSeperator />
          <SearchResultCardInventoryTypeA
            itemCode="itemCode"
            storeroom="storeroom"
            description="description"
            quantity="000"
            quantityAvailable="000"
            thumbnail="thumbnail"
          />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/SearchResultCardInventoryTypeB</SubTitle>
          <Description>
            props : itemCode, storeroom, description, quantity,
            quantityAvailable, thumbnail
          </Description>
          <WSeperator />
          <SearchResultCardInventoryTypeB
            itemCode="itemCode"
            storeroom="storeroom"
            description="description"
            quantity="000"
            quantityAvailable="000"
            thumbnail="thumbnail"
          />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/SearchResultCardTypeA</SubTitle>
          <Description>props : value1, value2, value3, value4</Description>
          <WSeperator />
          <SearchResultCardTypeA
            value1="value1"
            value2="value2"
            value3="value3"
            value4="value4"
          />
        </WhiteBox>
        <WhiteBox>
          <SubTitle>components/search/SearchResultCardTypeB</SubTitle>
          <Description>props : value1, value2, value3, value4</Description>
          <WSeperator />
          <SearchResultCardTypeB
            value1="value1"
            value2="value2"
            value3="value3"
            value4="value4"
          />
        </WhiteBox>
      </Wrapper>
    </>
  );
};

export default ComponentGuideSeo;
