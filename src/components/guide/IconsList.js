import React from "react";
import styled from "styled-components";
import {
  IconHome,
  IconAlarm,
  IconGear,
  IconSearchEqpt,
  IconDailyCheck,
  IconWorkOrder,
  IconPerson,
  IconMenuUnion,
  IconHamburger,
  IconCommBack,
  IconSearchSmall,
  IconSearchMedium,
  IconMainClock,
  IconMainRelease,
  IconTitleGear,
  IconTitleWorkOrder,
  IconCalendar,
  IconDelete12,
  IconSelectArrow,
  IconListSelect,
  IconWaitingTransmission,
  IconUpload,
  IconDownload,
  IconOffline,
  IconAddBorderBox,
  IconComment,
  IconMic,
  IconClosePopup,
  IconTrashCan,
  IconVoiceNext,
  IconVoicePrev,
  IconVoicePlay,
  IconHistory,
  IconAssignment,
  IconAssignmentNone,
  IconOPCompleted,
  IconLight,
  IconMainReleaseFull,
  IconPlus,
  IconMinus,
  IconInventoryMovement,
  IconInventoryCount,
  IconNFCTag,
  IconFilter,
  IconDailyInspection,
  IconWarehousing,
  IconRelease,
  IconLink,
  IconLocation,
  IconBox,
  IconPackage,
  IconTransport,
  IconBoldCheck,
  IconApproval,
  IconDocument,
  IconBarcode,
  IconRightArrow,
  IconRefresh,
  IconSettingFull,
  IconStart,
  IconCamera,
  IconHomeLine,
  IconInventory,
  IconBigNone1,
  IconBigNone2,
  IconBigNone3,
  IconBigNone4,
  IconBigNone5,
} from "components/common/Icons";

const Title = styled.h1`
  background: #000;
  color: #fff;
  margin-bottom: 10px;
  padding: 4px;
`;

const Wrapper = styled.div`
  > * {
    margin: 4px;
  }
`;

const Textarea = styled.pre`
  background: #333;
  color: white;
  width: 100%;
  padding: 10px;
  white-space: pre;
  line-height: 24px;
  box-sizing: border-box;
  margin-bottom: 15px;
`;

const IconsList = () => {
  return (
    <>
      <h1 style={{ fontSize: "40px", marginTop: "30px" }}>Icon</h1>
      <Wrapper>
        <Title>공통</Title>
        <IconHome />
        <IconAlarm />
        <IconGear />
        <IconMenuUnion />
        <IconHamburger />
        <IconCommBack />
        <IconSearchSmall />
        <IconSearchMedium />
        <IconOffline />
        <IconOffline width="51px" height="51px" fill="#42C9DE" />
        <IconAddBorderBox />
        <IconFilter />
        <IconSettingFull />

        <Textarea>
          &lt;IconHome /&gt; <br />
          &lt;IconAlarm /&gt; <br />
          &lt;IconGear /&gt; <br />
          &lt;IconMenuUnion /&gt; <br />
          &lt;IconHamburger /&gt; <br />
          &lt;IconCommBack /&gt; <br />
          &lt;IconSearchSmall /&gt; <br />
          &lt;IconSearchMedium /&gt; <br />
          &lt;IconOffline /&gt; <br />
          &lt;IconOffline width="51px" height="51px" fill="#42C9DE" /&gt; <br />
          &lt;IconAddBorderBox /&gt; <br />
          &lt;IconFilter /&gt; <br />
          &lt;IconSettingFull /&gt; <br />
        </Textarea>
      </Wrapper>

      <div style={{ height: "40px" }}></div>

      <Wrapper>
        <Title>contents</Title>
        <IconMainClock />
        <IconMainClock size={"14px"} />
        <IconMainRelease />
        <IconMainReleaseFull />
        <IconMainRelease size={"28px"} fill="#04B395" />
        <IconMainRelease size={"28px"} fill="#ED3A72" />
        <IconPlus />
        <IconMinus />
        <IconAssignment />
        <IconAssignmentNone />
        <IconOPCompleted />
        <IconLight />
        <IconLink />
        <IconLocation />
        <IconLocation width={"18px"} height={"18px"} />
        <IconBox />
        <IconPackage />
        <IconTransport />
        <span style={{ background: "#000000", display: "inline-block" }}>
          <IconTransport fill={"#ffffff"} />
        </span>
        <IconBoldCheck />
        <IconApproval />
        <IconDocument />
        <IconRightArrow />
        <IconRefresh />
        <IconStart />
        <Textarea>
          &lt;IconMainClock /&gt; <br />
          &lt;IconMainClock size={"14px"} /&gt; <br />
          &lt;IconMainRelease /&gt; <br />
          &lt;IconMainReleaseFull /&gt; <br />
          &lt;IconMainRelease size={"28px"} fill="#04B395" /&gt; <br />
          &lt;IconMainRelease size={"28px"} fill="#ED3A72" /&gt; <br />
          &lt;IconPlus /&gt; <br />
          &lt;IconMinus /&gt; <br />
          &lt;IconAssignment /&gt; <br />
          &lt;IconAssignmentNone /&gt; <br />
          &lt;IconOPCompleted /&gt; <br />
          &lt;IconLight /&gt; <br />
          &lt;IconLink /&gt; <br />
          &lt;IconLocation /&gt; <br />
          &lt;IconBox /&gt; <br />
          &lt;IconPackage /&gt; <br />
          &lt;IconTransport /&gt; <br />
          &lt;IconTransport fill="#ffffff" /&gt; <br />
          &lt;IconBoldCheck /&gt; <br />
          &lt;IconApproval /&gt; <br />
          &lt;IconDocument /&gt; <br />
          &lt;IconRightArrow /&gt; <br />
          &lt;IconRefresh /&gt; <br />
          &lt;IconStart /&gt; <br />
        </Textarea>
      </Wrapper>

      <Wrapper>
        <span style={{ background: "#000000", display: "inline-block" }}>
          <IconTitleGear />
        </span>
        <IconTitleGear fill="#000000" />

        <span style={{ background: "#000000", display: "inline-block" }}>
          <IconTitleWorkOrder />
        </span>

        <IconTitleWorkOrder fill="#000000" />
        <IconCalendar />
        <span style={{ background: "#000000", display: "inline-block" }}>
          <IconCalendar fill="#ffffff" />
        </span>
        <IconDelete12 />
        <IconSelectArrow />
        <IconListSelect />
        <IconComment />
        <IconMic />
        <span style={{ background: "#000000", display: "inline-block" }}>
          <IconMic fill="#ffffff" />
        </span>
        <span style={{ background: "#000000", display: "inline-block" }}>
          <IconHistory />
        </span>
        <IconHistory fill="#68D6E8" />
        <Textarea>
          &lt;IconTitleGear /&gt; <br />
          &lt;IconTitleGear fill="#000000" /&gt; <br />
          &lt;IconTitleWorkOrder /&gt; <br />
          &lt;IconTitleWorkOrder fill="#000000" /&gt; <br />
          &lt;IconCalendar /&gt; <br />
          &lt;IconCalendar fill="#ffffff" /&gt; <br />
          &lt;IconDelete12 /&gt; <br />
          &lt;IconSelectArrow /&gt; <br />
          &lt;IconListSelect /&gt; <br />
          &lt;IconComment /&gt; <br />
          &lt;IconMic /&gt; <br />
          &lt;IconMic fill="#ffffff" /&gt; <br />
          &lt;IconHistory /&gt; <br />
          &lt;IconHistory fill="#68D6E8" /&gt; <br />
        </Textarea>
      </Wrapper>

      <div style={{ height: "40px" }}></div>

      <Wrapper>
        <IconWaitingTransmission />
        <IconUpload />
        <IconDownload />
        <span style={{ background: "#ED3A72", display: "inline-block" }}>
          <IconWaitingTransmission fill="#ffffff" />
          <IconUpload fill="#ffffff" />
          <IconDownload fill="#ffffff" />
        </span>

        <Textarea>
          &lt;IconWaitingTransmission /&gt; <br />
          &lt;IconUpload /&gt; <br />
          &lt;IconDownload /&gt; <br />
          &lt;IconWaitingTransmission fill="#ffffff" /&gt; <br />
          &lt;IconUpload fill="#ffffff" /&gt; <br />
          &lt;IconDownload fill="#ffffff" /&gt; <br />
        </Textarea>
      </Wrapper>

      <div style={{ height: "40px" }}></div>

      <Wrapper>
        <Title>팝업</Title>
        <IconMic fill="#000000" />
        <IconClosePopup />
        <IconTrashCan />
        <IconVoiceNext />
        <IconVoicePlay />
        <IconVoicePrev />
        <Textarea>
          &lt;IconMic fill="#000000" /&gt; <br />
          &lt;IconClosePopup /&gt; <br />
          &lt;IconTrashCan /&gt; <br />
          &lt;IconVoiceNext /&gt; <br />
          &lt;IconVoicePlay /&gt; <br />
          &lt;IconVoicePrev /&gt; <br />
        </Textarea>
      </Wrapper>

      <div style={{ height: "40px" }}></div>

      <Wrapper>
        <Title>big size</Title>
        <IconSearchEqpt />
        <IconSearchEqpt width="28" height="28" />
        <IconDailyCheck />
        <IconDailyCheck width="28" height="28" />
        <IconWorkOrder />
        <IconWorkOrder width="28" height="28" />
        <IconInventoryMovement />
        <IconInventoryMovement width="28" height="28" />
        <IconInventoryCount />
        <IconInventoryCount width="28" height="28" />
        <IconPerson />
        <span style={{ background: "#ED3A72", display: "inline-block" }}>
          <IconNFCTag />
        </span>
        <IconDailyInspection />
        <IconDailyInspection width="28" height="28" />
        <IconWarehousing />
        <IconWarehousing width="28" height="28" />
        <IconRelease />
        <IconRelease width="28" height="28" />
        <IconBarcode />
        <IconCamera />
        <IconHomeLine />
        <IconInventory />
        <IconInventory width="28px" height="28px" />
        <IconBigNone1 />
        <IconBigNone1 size="28px" />
        <IconBigNone2 />
        <IconBigNone2 size="28px" />
        <IconBigNone3 />
        <IconBigNone3 size="28px" />
        <IconBigNone4 />
        <IconBigNone4 size="28px" />
        <IconBigNone5 />
        <IconBigNone5 size="28px" />
        <Textarea>
          &lt;IconSearchEqpt /&gt; <br />
          &lt;IconSearchEqpt width="28" height="28" /&gt; <br />
          &lt;IconDailyCheck /&gt; <br />
          &lt;IconDailyCheck width="28" height="28" /&gt; <br />
          &lt;IconWorkOrder /&gt; <br />
          &lt;IconWorkOrder width="28" height="28" /&gt; <br />
          &lt;IconInventoryMovement /&gt; <br />
          &lt;IconInventoryMovement width="28" height="28" /&gt; <br />
          &lt;IconInventoryCount /&gt; <br />
          &lt;IconInventoryCount width="28" height="28" /&gt; <br />
          &lt;IconPerson /&gt; <br />
          &lt;IconNFCTag /&gt; <br />
          &lt;IconDailyInspection /&gt; <br />
          &lt;IconDailyInspection width="28" height="28" /&gt; <br />
          &lt;IconWarehousing /&gt; <br />
          &lt;IconWarehousing width="28" height="28" /&gt; <br />
          &lt;IconRelease /&gt; <br />
          &lt;IconRelease width="28" height="28" /&gt; <br />
          &lt;IconBarcode /&gt; <br />
          &lt;IconCamera /&gt; <br />
          &lt;IconHomeLine /&gt; <br />
          &lt;IconInventory /&gt; <br />
        </Textarea>
      </Wrapper>
    </>
  );
};

export default IconsList;
