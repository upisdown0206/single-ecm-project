import React from "react";

import {
  IconRightArrow,
  IconKey,
  IconDeliveryTabRequest,
  IconBox,
} from "components/common/Icons";
import "./DeliveryCardPanel.scss";

const DeliveryCardPanel = ({
  onClick,
  deliveryState,
  deliveryId,
  actDttm,
  woId,
  deliveryFrom,
  deliveryTo,
  deliveryRequestNo,
  deliveryMethod,
  deliveryItemCount,
}) => {
  return (
    <div className="delivery-card-panel" onClick={onClick}>
      <div className="row spb" style={{ marginBottom: "3px" }}>
        <div className="col">
          <span className="title">{deliveryId}</span>
          <span className="date">{actDttm}</span>
          <span className="date time">12AM</span>
        </div>
        <div className="col">
          <div className="box-text">{woId}</div>
        </div>
      </div>
      <div className="row" style={{ marginBottom: "8px" }}>
        <div className="col">
          <span className="desc">
            ELECTRICAL, GGM, INTERNATIONAL LIMITED (H/O) MODELASEDFASDFASDF
          </span>
        </div>
      </div>
      <div className="row" style={{ marginBottom: "13px" }}>
        <div className="value-with-icon">
          <IconKey width="18" height="18" />
          <span className="icon-value">{deliveryRequestNo}</span>
        </div>
        <div className="value-with-icon">
          <IconDeliveryTabRequest width="18" height="18" />
          <span className="icon-value">{deliveryMethod}</span>
        </div>
        <div className="value-with-icon">
          <IconBox width="18" height="18" />
          <span className="icon-value">{deliveryItemCount}</span>
        </div>
      </div>
      <div className="row spb bt-dotted">
        <div className="col flex-col">
          <span className="label">From</span>
          <span className="value">{deliveryFrom}</span>
        </div>
        <div className="circle-icon">
          <IconRightArrow width="27" height="27" />
        </div>
        <div className="col flex-col">
          <span className="label">To</span>
          <span className="value">{deliveryTo}</span>
        </div>
      </div>
    </div>
  );
};

export default DeliveryCardPanel;
