import React, { useState } from "react";

import {
  IconClosePopup,
  IconLightSolid,
  IconLightSolidDis,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import BarcodeLottie from "components/common/BarcodeLottie";
import barcodeExmple from "asset/images/barcode-example.png";

import "./DeliveryBarcode.scss";

const DeliveryBarcode = ({
  deliveryId,
  itemId,
  itemStatus,
  onClose,
  onShowMessage,
}) => {
  const [lightMode, setLightMode] = useState(false);
  const onClick = () => {
    setLightMode(!lightMode);
  };
  const onSubmit = () => {
    const status = itemStatus === "wait" ? "ready" : "wait";
    onShowMessage(`'${itemId}'의 출고가 완료되었습니다.`);
    onClose();
  };
  return (
    <div className="delivery-barcode">
      <div className="delivery-barcode-camera">
        <img src={barcodeExmple} alt="barcode" onClick={onSubmit} />
      </div>
      <div className="delivery-barcode-action">
        <div className="scanning-wrapper">
          <div className="scanning-header">
            <h3 className="header-text">Barcode Scanning</h3>
            <IconBase onClick={onClose}>
              <IconClosePopup />
            </IconBase>
          </div>
          <BarcodeLottie />
        </div>
        <IconBase className="ab-icon" onClick={onClick}>
          {lightMode ? (
            <IconLightSolidDis width="24" height="24" />
          ) : (
            <IconLightSolid width="24" height="24" />
          )}
        </IconBase>
      </div>
    </div>
  );
};

export default DeliveryBarcode;
