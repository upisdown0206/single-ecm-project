import React, { useState } from "react";
import { format } from "date-fns";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import DateCalendarPicker from "components/common/DateCalendarPicker";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import CircleWithIcon from "components/common/CircleWithIcon";

import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeD from "components/search/LabelInputTypeD";

import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";
import SelectInputDiv from "components/search/SelectInputDiv";
import DimedModal from "components/base/DimedModal";

import MultiSelect from "components/common/select/MultiSelect";

import LabelInputCircleRadio from "components/workOrder/LabelInputCircleRadio";

import {
  IconCommBack,
  IconLocation,
  IconInputClock,
  IconCalendar,
  IconDelete12,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./DeliverySearchForm.scss";

const DeliverySearchForm = ({ onBackClick }) => {
  const [mode, setMode] = useState("init");
  const [subMode, setSubMode] = useState("");
  const onInnerClose = () => {
    setMode("");
  };

  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");
  const [searchExpandMode, setSearchExpandMode] = useState(false);

  const [keywords, setKeywords] = useState([
    {
      id: "1",
      type: "address",
      text: "LG사이언스파크",
      subText: "서울 강서구 마곡동",
    },
    {
      id: "2",
      type: "address",
      text: "신한은행 LG사이언스파크 출장소",
      subText: "서울 강서구 마곡동",
    },
  ]);

  const timeSelectItems = [
    {
      id: "t1",
      value: "09AM",
    },
    {
      id: "t2",
      value: "10AM",
    },
    {
      id: "t3",
      value: "11AM",
    },
    {
      id: "t4",
      value: "12AM",
    },
    {
      id: "t5",
      value: "1PM",
    },
  ];

  const onInputAllClearClick = () => {
    setKeywords([]);
    setTimeSelectedIds([]);
  };

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const [scheduledFromDt, setScheduledFromDt] = useState("");
  const [scheduledToDt, setScheduledToDt] = useState("");

  const [isShowFirstCalenderBox, setIsShowFirstCalendarBox] = useState(false);
  const [fromToType, setFromToType] = useState("FROM");

  const onFirstCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
  };

  const [toStoreRoom] = useState("init value");

  const onStoreRoomClick = (initValue) => {
    setFormType("address");
    setMode("FORM_DETAIL");
    setSearchInitValue("");
    setSearchExpandMode(true);
  };

  const onItemClick = () => {
    setFormType("itemcode");
    setMode("FORM_DETAIL");
    setSearchInitValue("");
    setSearchExpandMode(false);
  };

  const result = [
    {
      id: "delivery",
      value: "Delivery",
    },
    {
      id: "direct",
      value: "Direct",
    },
  ];

  const [selectedId, setSelectedId] = useState("delivery");
  const [timeSelectedIds, setTimeSelectedIds] = useState(["t1", "t2"]);

  return (
    <>
      <ModalFullPageAni
        className="delivery-search-form"
        modeState={mode}
        isBaseComponent={true}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Search" />}
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + ${isKeyboardFocus ? `0px` : `79px`})`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeD
              labelText="Item Code"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="itemcode"
              onInputClick={onItemClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB labelText="WO No" onToggleFocus={onToggleFocus} />
          </div>
          <div className="label-input">
            <LabelInputTypeB
              labelText="Delivery Request No"
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <div className="label">Delivery Location</div>
            <div className="store-rooms">
              {/* <div className="normal-div">
                <SelectInputDiv
                  labelText=""
                  placeholder="From"
                  value=""
                  onClick={() => setSubMode("DELIVERY_SEARCH/FROM_STOREROOM")}
                />
              </div> */}
              <div
                className="store-room input-box"
                onClick={() => onStoreRoomClick(toStoreRoom)}
              >
                <IconLocation width="24" height="24" />
                <input
                  type="text"
                  placeholder="To"
                  value={toStoreRoom}
                  readOnly
                />
              </div>
            </div>
          </div>
          <div className="label-input">
            <div className="label">Delivery Type</div>
            <LabelInputCircleRadio
              labelText=""
              items={result}
              radioGroupName="deliveryType"
              selectedId={selectedId}
              setSelectedId={setSelectedId}
            />
          </div>
          <div className="label-input">
            <div className="label">Delivery Date</div>
            <div className="date-input-row">
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="FROM"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={
                      scheduledFromDt && format(scheduledFromDt, "yyyy.MM.dd")
                    }
                    placeholder="From"
                    readOnly
                  />
                </div>
              </div>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="TO"
                  style={{ marginLeft: "8px" }}
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={scheduledToDt && format(scheduledToDt, "yyyy.MM.dd")}
                    placeholder="To"
                    readOnly
                  />
                </div>
              </div>
            </div>
            <div className="date-input-row">
              <SelectInputDiv
                className={timeSelectedIds.length > 0 && "re-padding"}
                beforeIcon={
                  <IconBase className="time-icon">
                    <IconInputClock />
                  </IconBase>
                }
                placeholder="Time"
                value={
                  timeSelectItems.filter((item) =>
                    timeSelectedIds.includes(item.id)
                  ).length > 0 ? (
                    <div className="circles">
                      {timeSelectItems
                        .filter((item) => timeSelectedIds.includes(item.id))
                        .map((item) => (
                          <CircleWithIcon
                            key={item.id}
                            className="circle-item"
                            text={item.value}
                            icon={
                              <IconBase
                                className="delete-icon"
                                onClick={(e) => {
                                  e.stopPropagation();
                                  setTimeSelectedIds(
                                    timeSelectedIds.filter(
                                      (id) => id !== item.id
                                    )
                                  );
                                }}
                              >
                                <IconDelete12 />
                              </IconBase>
                            }
                          />
                        ))}
                    </div>
                  ) : null
                }
                onClick={() => setSubMode("DELIVERY_SEARCH/DELIVERY_TIME")}
              />
            </div>
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
        {isShowFirstCalenderBox && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={scheduledFromDt}
            setFromDate={setScheduledFromDt}
            toDate={scheduledToDt}
            setToDate={setScheduledToDt}
            onClose={() => setIsShowFirstCalendarBox(false)}
          />
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={onInnerClose}
          initValue={searchInitValue}
          isExpandMode={searchExpandMode}
        />
      </ModalFullPageAni>
      {subMode === "DELIVERY_SEARCH/DELIVERY_TIME" && (
        <DimedModal className="delivery-search-select-modal">
          <MultiSelect
            title="Time"
            onClose={() => setSubMode("")}
            items={timeSelectItems}
            selectedIds={timeSelectedIds}
            setSelectedIds={setTimeSelectedIds}
          />
        </DimedModal>
      )}
    </>
  );
};

export default DeliverySearchForm;
