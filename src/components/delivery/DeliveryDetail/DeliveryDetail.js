import React, { useState } from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";

import {
  IconCommBack,
  IconLocation,
  IconDeliveryTabRequest,
  IconDeliveryTabSent,
  IconDeliveryTabComplete,
  IconBox,
  IconPlus,
  IconMinus,
  IconBarcodeTarget,
  IconMainReleaseFull,
  IconPlusBox,
  IconReset,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import { useSnackbar } from "notistack";

import HeaderTitle from "components/common/HeaderTitle";
import FixedFooter from "components/base/FixedFooter";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import DeliveryBarcode from "components/delivery/DeliveryBarcode";
import DeliverySignature from "components/delivery/DeliverySignature";

import sampleSign from "asset/images/sample-sign.png";

import "./DeliveryDetail.scss";

const DeliveryDetail = ({
  deliveryState,
  deliveryId,
  actDttm,
  woId,
  deliveryFrom,
  deliveryTo,
  deliveryRequestNo,
  deliveryMethod,
  deliveryItemCount,
  deliveryItems,
  deliveryDesc,
  onBackClick,
}) => {
  const [mode, setMode] = useState("");
  const onClose = () => setMode("");

  const onDeliveryStart = () => {
    // start 처리
    onBackClick();
  };

  const [ellipsisToggle, setEllipsisToggle] = useState(false);
  const onEllipsisToggle = () => {
    setEllipsisToggle(!ellipsisToggle);
  };

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = (inMessage, isOK = true) => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar(inMessage, {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 108 }}
          onClick={() => closeSnackbar(key)}
        >
          <div className="ok-wrapper">
            {isOK && (
              <IconMainReleaseFull fill="#04B395" width="22" height="22" />
            )}
            <span className="text">{message}</span>
          </div>
        </div>
      ),
    });
  };

  const [selectInfo, setSelectInfo] = useState(null);

  const [sign, setSign] = useState(
    deliveryState === "complete" ? sampleSign : null
  );

  return (
    <>
      <div className="delivery-detail">
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack fill="#ffffff" bgFill="#424952" />
            </IconBase>
          }
          center={
            <HeaderTitle
              text={`Delivery ${deliveryState === "request" ? "Request" : ""}${
                deliveryState === "sent" ? "Sent" : ""
              }${deliveryState === "complete" ? "Complete" : ""}`}
              blackMode={true}
            />
          }
          blackMode={true}
        />

        <main
          className="delivery-detail-main"
          style={{
            height:
              deliveryState === "complete"
                ? `calc(100vh - 56px)`
                : `calc(100vh - (56px + 87px))`,
            marginTop: `56px`,
            overflowY: `auto`,
          }}
        >
          <div className="sticky-header flex-spb">
            <div className="col">
              <div className="text">{deliveryId}</div>
              <div className="sub-text">{actDttm}</div>
            </div>
            <div className="col">
              <div className="store">
                <IconLocation width="18" height="18" />
                <span className="text">{deliveryTo}</span>
              </div>
            </div>
          </div>
          <div
            className={cx(
              "step-path",
              deliveryState === "sent" && "half",
              deliveryState === "complete" && "all"
            )}
          >
            <div
              className={cx(
                "circle-path",
                "request",
                deliveryState === "request" && "active",
                (deliveryState === "sent" || deliveryState === "complete") &&
                  "done"
              )}
            >
              <div className="path">
                {deliveryState === "request" ? (
                  <IconDeliveryTabRequest width="30" height="30" />
                ) : (
                  <div className="default-point" />
                )}
              </div>
              <div className="path-name">request</div>
            </div>
            <div
              className={cx(
                "circle-path",
                "sent",
                deliveryState === "sent" && "active",
                deliveryState === "complete" && "done"
              )}
            >
              <div className="path">
                {deliveryState === "sent" ? (
                  <IconDeliveryTabSent width="30" height="30" />
                ) : (
                  <div className="default-point" />
                )}
              </div>
              <div className="path-name">sent</div>
            </div>
            <div
              className={cx(
                "circle-path",
                "complete",
                deliveryState === "complete" && "active"
              )}
            >
              <div className="path">
                {deliveryState === "complete" ? (
                  <IconDeliveryTabComplete width="30" height="30" />
                ) : (
                  <div className="default-point" />
                )}
              </div>
              <div className="path-name">complete</div>
            </div>
          </div>
          <div className="dashboard-panel">
            <div className="dashboard-inner">
              <div className="inner-box">
                <div className="col">
                  <IconBase className="rectangle-icon">
                    <IconBox width="30" height="30" />
                  </IconBase>
                </div>
                <div className="col flex-col">
                  <span className="panel-title">Quantity</span>
                  <div className="panel-content">
                    <span className="label">Reserved</span>
                    <span className="value">2</span>
                    <div className="splitter" />
                    <span className="label">Delivery</span>
                    <span className="value">0</span>
                  </div>
                </div>
              </div>
              <div className="bg-trick">
                <div className="top" />
                <div className="bottom" />
              </div>
            </div>
          </div>
          <div className="content-panel">
            <div className="content-row">
              <div className="col">
                <div className="label">Item Description</div>
                <div className="value">
                  ELECTRICAL, GGM, INTERNATIONAL LIMITED (H/O)
                  MODELASEDFASDFASDF
                </div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">WO No</div>
                <div className="value">{woId}</div>
              </div>
              <div className="col">
                <div className="label">Delivery Request No</div>
                <div className="value">{deliveryRequestNo}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Condition Code</div>
                <div className="value">
                  <div className="circle">NEW</div>
                </div>
              </div>
              <div className="col">
                <div className="label">Delivery Type</div>
                <div className="value">
                  <div className="circle">{deliveryMethod}</div>
                </div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Delivery Time</div>
                <div className="value">9 AM</div>
              </div>
              <div className="col">
                <div className="label">Delivery Location</div>
                <div className="value">DL001</div>
              </div>
            </div>
            {/* <div className="content-row">
              <div className="col flex-row spb same-children">
                <div className="label">Description</div>
                <div className="value flex-row spb">
                  <span
                    className={cx(
                      "ellipsis-text",
                      !ellipsisToggle && "ellipsis"
                    )}
                  >
                    {deliveryDesc}
                  </span>
                  <IconBase
                    className="ellipsis-icon"
                    onClick={onEllipsisToggle}
                  >
                    {ellipsisToggle ? (
                      <IconMinus size="16" />
                    ) : (
                      <IconPlus size="16" />
                    )}
                  </IconBase>
                </div>
              </div>
            </div> */}
            <div className="content-row flex-col">
              <div className="col flex-row spb no-bb">
                <div className="label flex-row">
                  <IconBarcodeTarget width="24" height="24" />
                  <span>배송대상선택</span>
                </div>
                <div className="value more-detail">
                  <span className="value-labeled">총</span>
                  <span className="value-import">
                    {
                      deliveryItems.filter((item) => item.itemStatus === "wait")
                        .length
                    }
                  </span>
                  <span className="value-normal">{`/${deliveryItems.length}`}</span>
                </div>
              </div>
              <div className="col flex-row no-bb">
                <ul className="delivery-items">
                  {deliveryItems.map((item) => (
                    <li key={item.itemId} className="delivery-item">
                      <div
                        className={cx(
                          "delivery-item-box",
                          item.itemStatus === "ready" && "active"
                        )}
                        onClick={() => {
                          if (item.itemStatus !== "wait") return;
                          setMode("DELIVERY_DETAIL/BARCODE");
                          setSelectInfo(item);
                        }}
                      >
                        <div className="item-status">
                          {item.itemStatus === "wait" && "출고준비"}
                          {item.itemStatus === "ready" && "준비완료"}
                        </div>
                        <div className="item-id">{item.itemId}</div>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            {(deliveryState === "sent" || deliveryState === "complete") && (
              <div className="content-row">
                <div className="col no-bb">
                  <div
                    className="label flex-row spb"
                    style={{ marginBottom: "10px" }}
                  >
                    <span>Signature</span>
                    {deliveryState === "sent" && sign && (
                      <div className="reset" onClick={() => setSign(null)}>
                        <IconReset />
                        <span className="reset-text">Reset</span>
                      </div>
                    )}
                  </div>
                  <div className="value">
                    <div
                      className="sign-pad-box"
                      onClick={() => {
                        if (sign) return;
                        setMode("DELIVERY_DETAIL/SIGNATURE");
                      }}
                    >
                      {sign ? (
                        <img className="sign-img" src={sign} alt="signature" />
                      ) : (
                        <IconBase className="sign-pad-icon">
                          <IconPlusBox width="60" height="60" />
                        </IconBase>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </main>
        {(deliveryState === "request" || deliveryState === "sent") && (
          <FixedFooter className="footer-white">
            <div
              className={cx(
                "save-button",
                deliveryState === "request" &&
                  deliveryItems.filter((item) => item.itemStatus === "wait")
                    .length === 0 &&
                  "active",
                deliveryState === "sent" && sign && "active"
              )}
              onClick={onDeliveryStart}
            >
              {deliveryState === "request" && `Delivery Start`}
              {deliveryState === "sent" && `Delivery Complete`}
            </div>
          </FixedFooter>
        )}
      </div>
      <ModalFullPageAni
        modeState={mode}
        pageName="DELIVERY_DETAIL/BARCODE"
        motion="vertical"
      >
        <DeliveryBarcode
          onClose={onClose}
          deliveryId={deliveryId}
          onShowMessage={onShowMessage}
          {...selectInfo}
        />
      </ModalFullPageAni>
      <ModalFullPageAni
        modeState={mode}
        isDimmed={true}
        pageName="DELIVERY_DETAIL/SIGNATURE"
        motion="vertical"
      >
        <DeliverySignature onClose={onClose} setSign={setSign} />
      </ModalFullPageAni>
    </>
  );
};

export default DeliveryDetail;
