import React, { useState } from "react";
import cx from "classnames";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";

import DeliveryCardPanel from "components/delivery/DeliveryCardPanel";
import DeliveryDetail from "components/delivery/DeliveryDetail";

import {
  IconHamburger,
  IconDeliveryTabRequest,
  IconDeliveryTabSent,
  IconDeliveryTabComplete,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./DeliveryLanding.scss";

const DeliveryLanding = ({ onSearchClick }) => {
  const [mode, setMode] = useState("init");
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "owningDept", text: "QCA" },
    { id: "2", type: "fromto", text: "2020-08-11 ~ 2020-08-11" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);
  const [results] = useState([
    {
      deliveryState: "request",
      deliveryId: "QCAD0001",
      actDttm: "2020.05.01",
      woId: "87844",
      deliveryFrom: "Central",
      deliveryTo: "BF 1",
      deliveryRequestNo: "1919",
      deliveryMethod: "Delivery",
      deliveryItemCount: "2",
      deliveryItems: [
        {
          itemStatus: "wait",
          itemId: "12345",
        },
        {
          itemStatus: "wait",
          itemId: "67891",
        },
      ],
      deliveryDesc: "request-초기상태(전체 wait)",
    },
    {
      deliveryState: "request",
      deliveryId: "QCAD0002",
      actDttm: "2020.05.03",
      woId: "897845",
      deliveryFrom: "Central",
      deliveryTo: "Gumi",
      deliveryRequestNo: "5155",
      deliveryMethod: "Direct",
      deliveryItemCount: "4",
      deliveryItems: [
        {
          itemStatus: "wait",
          itemId: "aaaaa",
        },
        {
          itemStatus: "wait",
          itemId: "bbbbb",
        },
        {
          itemStatus: "wait",
          itemId: "ccccc",
        },
        {
          itemStatus: "ready",
          itemId: "ddddd",
        },
      ],
      deliveryDesc: "request-진행중일때(바코드 진행한 것 ready)",
    },
    {
      deliveryState: "request",
      deliveryId: "QCAD0003",
      actDttm: "2020.05.05",
      woId: "897846",
      deliveryFrom: "Central",
      deliveryTo: "Paju_N",
      deliveryRequestNo: "215155",
      deliveryMethod: "Delivery",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc:
        "request-전체 진행되었을 때(바코드 진행한 것 ready, start 활성화)",
    },
    {
      deliveryState: "sent",
      deliveryId: "QCAD0005",
      actDttm: "2020.06.01",
      woId: "87845",
      deliveryFrom: "Central",
      deliveryTo: "BF 2",
      deliveryRequestNo: "1202",
      deliveryMethod: "Delivery",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "sent-",
    },
    {
      deliveryState: "sent",
      deliveryId: "QCAD0006",
      actDttm: "2020.06.03",
      woId: "89555",
      deliveryFrom: "Central",
      deliveryTo: "BF 3",
      deliveryRequestNo: "55445",
      deliveryMethod: "Delivery",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "sent-",
    },
    {
      deliveryState: "sent",
      deliveryId: "QCAD0007",
      actDttm: "2020.06.05",
      woId: "89886",
      deliveryFrom: "Central",
      deliveryTo: "Paju_N",
      deliveryRequestNo: "111155",
      deliveryMethod: "Delivery",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "sent-",
    },
    {
      deliveryState: "sent",
      deliveryId: "QCAD0008",
      actDttm: "2020.06.07",
      woId: "897847",
      deliveryFrom: "Central",
      deliveryTo: "Central",
      deliveryRequestNo: "5595",
      deliveryMethod: "Direct",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "sent-",
    },
    {
      deliveryState: "complete",
      deliveryId: "QCAD0009",
      actDttm: "2020.07.01",
      woId: "87844",
      deliveryFrom: "Central",
      deliveryTo: "BF 1",
      deliveryRequestNo: "1919",
      deliveryMethod: "Delivery",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "complete-",
    },
    {
      deliveryState: "complete",
      deliveryId: "QCAD0010",
      actDttm: "2020.07.03",
      woId: "897845",
      deliveryFrom: "Central",
      deliveryTo: "Gumi",
      deliveryRequestNo: "5155",
      deliveryMethod: "Direct",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "complete-",
    },
    {
      deliveryState: "complete",
      deliveryId: "QCAD0011",
      actDttm: "2020.07.06",
      woId: "897846",
      deliveryFrom: "Central",
      deliveryTo: "Paju_N",
      deliveryRequestNo: "215155",
      deliveryMethod: "Delivery",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "complete-",
    },
    {
      deliveryState: "complete",
      deliveryId: "QCAD0012",
      actDttm: "2020.07.07",
      woId: "897847",
      deliveryFrom: "Central",
      deliveryTo: "Central",
      deliveryRequestNo: "5595",
      deliveryMethod: "Direct",
      deliveryItemCount: "3",
      deliveryItems: [
        {
          itemStatus: "ready",
          itemId: "99999",
        },
        {
          itemStatus: "ready",
          itemId: "88888",
        },
        {
          itemStatus: "ready",
          itemId: "77777",
        },
      ],
      deliveryDesc: "complete-",
    },
  ]);

  const [prevScrollTop, setPrevScrollTop] = useState(0);
  const [hide, setHide] = useState(false);
  const handleScroll = (e) => {
    const scrollTop = e.currentTarget.scrollTop;
    const deltaY = scrollTop - prevScrollTop;
    const tmpHide = scrollTop !== 0 && deltaY >= 0;
    setHide(tmpHide);
    setPrevScrollTop(scrollTop);
  };

  // request / sent / complete
  const [tabMode, setTabMode] = useState("request");
  const [selectInfo, setSelectInfo] = useState(null);

  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <div className="delivery-landing">
          <FixedHeader
            left={
              <IconBase
                onClick={
                  isMenuOpen ? undefined : () => setIsMenuOpen(!isMenuOpen)
                }
              >
                {!isMenuOpen && <IconHamburger />}
              </IconBase>
            }
            center={
              <div className="equipment-title">
                <HeaderTitle text="Delivery">
                  {results && <CircleText text={results.length} />}
                </HeaderTitle>
              </div>
            }
          />

          <BaseMain
            className="delivery-main"
            isMenuOpen={isMenuOpen}
            onMenuClick={() => setIsMenuOpen(!isMenuOpen)}
            style={{
              height: `calc(100vh - (56px + 60px + 55px))`,
              marginTop: `calc(56px + 55px)`,
            }}
            onScroll={handleScroll}
          >
            <FixedWrapper style={{ top: "56px" }}>
              <SearchConditionPanelTypeB
                onSearchClick={onSearchClick}
                keywords={keywords}
                setKeywords={setKeywords}
                isBorderBot={false}
              />
            </FixedWrapper>
            <div className={cx("delivery-header", !hide && "sticky-header")}>
              <div className="inner">
                <div
                  className={cx(
                    "delivery-tab",
                    tabMode === "request" && "active"
                  )}
                  onClick={() => setTabMode("request")}
                >
                  <IconDeliveryTabRequest width="30" height="30" />
                  <span className="tab-text">Request</span>
                </div>
                <div
                  className={cx("delivery-tab", tabMode === "sent" && "active")}
                  onClick={() => setTabMode("sent")}
                >
                  <IconDeliveryTabSent width="30" height="30" />
                  <span className="tab-text">Sent</span>
                </div>
                <div
                  className={cx(
                    "delivery-tab",
                    tabMode === "complete" && "active"
                  )}
                  onClick={() => setTabMode("complete")}
                >
                  <IconDeliveryTabComplete width="30" height="30" />
                  <span className="tab-text">Complete</span>
                </div>
                <div className={cx("animation-bg", tabMode)} />
              </div>
            </div>
            {tabMode === "request" && (
              <ul
                className="delivery-items"
                style={{ marginTop: `calc(10px + ${!hide ? "59px" : "0px"})` }}
              >
                {results
                  .filter((item) => item.deliveryState === "request")
                  .map((item) => (
                    <li key={item.deliveryId} className="delivery-item">
                      <DeliveryCardPanel
                        onClick={() => {
                          setMode("DELIVERY_DETAIL");
                          setSelectInfo(item);
                        }}
                        {...item}
                      />
                    </li>
                  ))}
              </ul>
            )}
            {tabMode === "sent" && (
              <ul
                className="delivery-items"
                style={{ marginTop: `calc(10px + ${!hide ? "59px" : "0px"})` }}
              >
                {results
                  .filter((item) => item.deliveryState === "sent")
                  .map((item) => (
                    <li key={item.deliveryId} className="delivery-item">
                      <DeliveryCardPanel
                        onClick={() => {
                          setMode("DELIVERY_DETAIL");
                          setSelectInfo(item);
                        }}
                        {...item}
                      />
                    </li>
                  ))}
              </ul>
            )}
            {tabMode === "complete" && (
              <ul
                className="delivery-items"
                style={{ marginTop: `calc(10px + ${!hide ? "59px" : "0px"})` }}
              >
                {results
                  .filter((item) => item.deliveryState === "complete")
                  .map((item) => (
                    <li key={item.deliveryId} className="delivery-item">
                      <DeliveryCardPanel
                        onClick={() => {
                          setMode("DELIVERY_DETAIL");
                          setSelectInfo(item);
                        }}
                        {...item}
                      />
                    </li>
                  ))}
              </ul>
            )}
          </BaseMain>
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        </div>
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="DELIVERY_DETAIL">
        <DeliveryDetail onBackClick={() => setMode("")} {...selectInfo} />
      </ModalFullPageAni>
    </>
  );
};

export default DeliveryLanding;
