import React, { useState } from "react";
import Lottie from "react-lottie";
import success from "asset/lottie/success.json";
import styled from "styled-components";

const LottieFix = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const lottieOptions = {
  animationData: success,
  loop: true,
  autoplay: true,
  rendererSettings: {
    className: "add-class",
    preserveAspectRatio: "xMidYMid slice",
  },
};

const SuccessLottie = ({ style }) => {
  return (
    <LottieFix style={style}>
      <Lottie
        options={lottieOptions}
        isClickToPauseDisabled={false}
        style={{ width: "140px", height: "140px" }}
        eventListeners={[
          {
            eventName: "complete",
            callback: () => console.log("the animation completed"),
          },
        ]}
      />
    </LottieFix>
  );
};

export default SuccessLottie;
