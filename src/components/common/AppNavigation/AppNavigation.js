import React from "react";
import { Link, withRouter } from "react-router-dom";
import cx from "classnames";

import IconBase from "components/common/IconBase";
import {
  IconHomeRegular,
  IconHome,
  IconAlarm,
  IconAlarmSolid,
  IconGear,
  IconGearSolid,
} from "components/common/Icons";

import "./AppNavigation.scss";

const AppNavigation = withRouter(({ location }) => {
  const { pathname } = location;
  const alarmNewFlag = true;
  return (
    <nav className="app-nav">
      <ul>
        <li>
          <Link
            to="/alarm"
            className={cx("icon", pathname === "/alarm" && "selected")}
          >
            <IconBase className={cx("alarm-relative", alarmNewFlag && "new")}>
              {pathname === "/alarm" ? <IconAlarmSolid /> : <IconAlarm />}
            </IconBase>

            <span>Alarm</span>
          </Link>
        </li>
        <li>
          <Link
            to="/"
            className={cx(
              "icon",
              pathname !== "/alarm" && pathname !== "/setting" && "selected"
            )}
          >
            {pathname !== "/alarm" && pathname !== "/setting" ? (
              <IconHome />
            ) : (
              <IconHomeRegular />
            )}

            <span>Home</span>
          </Link>
        </li>
        <li>
          <Link
            to="/setting"
            className={cx("icon", pathname === "/setting" && "selected")}
          >
            {pathname === "/setting" ? <IconGearSolid /> : <IconGear />}

            <span>Setting</span>
          </Link>
        </li>
      </ul>
    </nav>
  );
});

export default AppNavigation;
