import React,{useState} from 'react';
import classnames from 'classnames';
import './SwitchButton.scss'

const SwitchButton = () => {
  const [isOn, setIsOn] = useState(false);
  const onSwitchToggle = () =>{
    setIsOn(!isOn);
  }
  return (
    <div className={classnames("switch-btn", isOn ? "on" : "off")} onClick={onSwitchToggle}>
      <span className="round-handle">&nbsp;</span>
    </div>
  );
};

export default SwitchButton;