import React, { useState } from "react";
import classNames from "classnames";

import {
  IconPackage,
  IconTransport,
  IconBoldCheck,
} from "components/common/Icons";

import "./TabMove.scss";

const TabButton = ({ onClick, title, icon, active }) => {
  return (
    <button className={classNames("tab-button", active)} onClick={onClick}>
      <span>{icon}</span>
      <span>{title}</span>
    </button>
  );
};

const TabMove = ({ className, div, ...rest }) => {
  const [activeTab, setActiveTab] = useState("tab0");
  const handleTab = (tabIndex) => {
    setActiveTab(tabIndex);
  };


  return (
    <div className={classNames("tab-common",div,activeTab)}>
      <TabButton
        title="출고요청"
        onClick={()=>handleTab("tab0")}
        icon={(activeTab === "tab0" ? <IconPackage fill="#ffffff" />:<IconPackage /> )}
        active={(activeTab === "tab0" && "active")}
      />
      <TabButton
        title="출고중"
        onClick={()=>handleTab("tab1")}
        icon={(activeTab === "tab1" ? <IconTransport fill="#ffffff" />:<IconTransport /> )}
        active={(activeTab === "tab1" && "active")}
      />
      <TabButton
        title="출고완료"
        onClick={()=>handleTab("tab2")}
        icon={(activeTab === "tab2" ? <IconBoldCheck fill="#ffffff" />:<IconBoldCheck /> )}
        active={(activeTab === "tab2" && "active")}
      />
    </div>
  );
};

export default TabMove;
