import React from "react";
import {IconListSelect} from "../Icons";

const TabInTabButton = ({ type, title, complete, ...rest }) => {
  return (
    <button className={type} {...rest}>
      {title}
      {complete && <IconListSelect fill={"#2C3238"} size={"10px"}/>}
    </button>
  );
};

export default TabInTabButton;
