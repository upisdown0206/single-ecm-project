import React from "react";
import classnames from "classnames";
import "./SwitchButtonAlter.scss";

const SwitchButtonAlter = ({ isOn = false }) => {
  return (
    <div className={classnames("switch-btn", isOn ? "on" : "off")}>
      <span className="round-handle">&nbsp;</span>
    </div>
  );
};

export default SwitchButtonAlter;
