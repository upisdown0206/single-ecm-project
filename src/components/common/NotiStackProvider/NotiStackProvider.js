import React from "react";
import { SnackbarProvider } from "notistack";

import "./NotiStackProvider.scss";

const NotiStackProvider = ({ children }) => {
  return (
    <SnackbarProvider
      maxSnack={1}
      dense
      preventDuplicate
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      content={(key, message) => (
        <div className="notistack" id={key}>
          <span>{message}</span>
        </div>
      )}
    >
      {children}
    </SnackbarProvider>
  );
};

export default NotiStackProvider;
