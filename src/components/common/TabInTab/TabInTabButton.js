import React from "react";
import { IconListSelect } from "components/common/Icons";

const TabInTabButton = ({ type, title, complete, ...rest }) => {
  return (
    <button className={type} {...rest}>
      {title}
      {complete && <IconListSelect size="10px" fill="#2C3238" />}
    </button>
  );
};

export default TabInTabButton;
