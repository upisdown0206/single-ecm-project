import React from "react";
import './TabInTab.scss'
const TabInTab = ({ children}) => {
  return (
    <div className={"tab-in-tab"}>
      {children}
    </div>
  );
};

export default TabInTab;
