import React from "react";
import cx from "classnames";

import "./HeaderTitle.scss";

const HeaderTitle = ({ text, children, style, blackMode = false }) => {
  return (
    <div className="header-title-wrapper" style={style}>
      <span className={cx("title", blackMode && "black-mode")}>{text}</span>
      {children}
    </div>
  );
};

export default HeaderTitle;
