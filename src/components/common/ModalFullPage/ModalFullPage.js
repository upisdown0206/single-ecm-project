import React from 'react';
import './ModalFullPage.scss'

const ModalFullPage = ({children}) => {
  return (
    <div className="modal-full-page">
      {children}
    </div>
  );
};

export default ModalFullPage;