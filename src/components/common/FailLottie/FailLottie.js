import React, { useState } from "react";
import Lottie from "react-lottie";
import fail from "asset/lottie/fail.json";
import styled from "styled-components";

const LottieFix = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const lottieOptions = {
  animationData: fail,
  loop: true,
  autoplay: true,
  rendererSettings: {
    className: "add-class",
    preserveAspectRatio: "xMidYMid slice",
  },
};

const FailLottie = ({ style }) => {
  return (
    <LottieFix style={style}>
      <Lottie
        options={lottieOptions}
        isClickToPauseDisabled={false}
        style={{ width: "140px", height: "140px" }}
        eventListeners={[
          {
            eventName: "complete",
            callback: () => console.log("the animation completed"),
          },
        ]}
      />
    </LottieFix>
  );
};

export default FailLottie;
