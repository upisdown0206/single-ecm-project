import React, {useState} from 'react';
import {IconListSelect} from "../../Icons";
import './TabAniType.scss'
import {motion} from "framer-motion";

const TabAniType = ({complete}) => {
  const [selected, setSelected] = useState(0);
  const [tabMenu, setTabMenu] = useState([
    {
      id:"1",
      title:"Failure",
      status:"complete"
    },
    {
      id:"2",
      title:"Check",
      status:""
    },
    {
      id:"3",
      title:"Cause & Resolution",
      status:""
    },
  ])
  return (
    <div className="ani-tab">
      <ul>
        {
          tabMenu.map(({title,status},i) =>(
            <li
              className={`title ${i === selected && "active"}`}
              onClick={() => setSelected(i)}
            >
              {
                i === selected && (
                 <div className="underline"></div>
                )
              }
              {title}
              {
                status === "complete" &&(
                  <IconListSelect size="10px" fill="#2C3238" />
                )
              }
            </li>
          ))
        }
      </ul>
    </div>
  );
};

export default TabAniType;