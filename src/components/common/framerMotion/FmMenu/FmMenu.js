import React, {useState} from 'react';
import './FmMenu.scss';
import styled from 'styled-components';
import FixedHeader from "../../../base/FixedHeader";
import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import BaseMain from "../../../base/BaseMain";
import {IconCommBack} from "../../Icons-bak";

const FmMenu = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  const [commonMenu, setCommonMenu] = useState([
    {
      id:"1",
      menu:"WO",
    },
    {
      id:"2",
      menu:"Daily Check",
    },
    {
      id:"3",
      menu:"Spare Parts",
    },
    {
      id:"4",
      menu:"Equipment",
    },
    {
      id:"5",
      menu:"Inventory",
    }
  ])
  return (
    <div style={{paddingTop:"80px"}}>
      <FixedHeader
        left={
          <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
            {!isMenuOpen && <IconHamburger />}
          </IconBase>
        }
      />
      {isMenuOpen && (
        <ul className="menu" >
          {
            commonMenu.map((item,idx)=>(
              <li key={idx}>
                {item.menu}
              </li>
            ))
          }
        </ul>
      )}
    </div>
  );
};

export default FmMenu;