import React,{useState} from 'react';
import {motion, AnimateSharedLayout} from "framer-motion";
import './FmTab.scss';
import TabInTab from "../../TabInTab";
import {IconListSelect} from "../../Icons";

const FmTab = ({complete}) => {
  const [selected, setSelected] = useState(0);
  const [tabMenu, setTabMenu] = useState([
    {
      id:"1",
      title:"수량요청",
      status:"complete"
    },
    {
      id:"2",
      title:"SP Issue",
      status:""
    }
  ])

  return (
    <AnimateSharedLayout>
      <div className="ani-tab-in-tab">
        {
          tabMenu.map(({title,status},i) =>(
            <motion.button
              animate
              key={i}
              className={`title ${i === selected && "active"}`}
              onClick={() => setSelected(i)}
            >
              {
                i === selected && (
                  <motion.div
                    layoutId="underline"
                    className="underline"
                    style={{background:"#E0205C"}}
                  />
                )
              }
              {title}
            </motion.button>
          ))
        }
      </div>
    </AnimateSharedLayout>
  );
};

export default FmTab;