import React from "react";
import "./Checkbox.scss";

const CheckBox = ({ value1, checked = false, text, children , onChange, ...rest }) => {
  return (
    <div className="check-box">
      <input type="checkbox" id={`cb${value1}`} checked={checked} onChange={onChange} {...rest} />
      <label htmlFor={`cb${value1}`}>{text}</label>
      {children}
    </div>
  );
};

export default CheckBox;
