import React, { useState } from "react";
import cx from "classnames";

import {
  IconArrowDownInput,
  IconArrowUpInput,
  IconClosePopup,
  IconListSelect,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import Button from "components/common/Button";

import "./InputSelector.scss";

const InputSelector = ({
  inputValueItem,
  setInputValueItem,
  title,
  items,
  className,
  placeholder = "선택",
}) => {
  //const [selectItems, setSelectItems] = useState(items);
  // TODO: rollback
  const [selectedId, setSelectedId] = useState(
    inputValueItem ? inputValueItem.id : null
  );

  const [isSelectorOpen, setIsSelectorOpen] = useState(false);

  const onOpen = () => {
    setSelectedId(inputValueItem ? inputValueItem.id : null);
    setIsSelectorOpen(true);
  };

  const onClose = () => {
    setIsSelectorOpen(false);
  };

  const onConfirm = () => {
    setInputValueItem(items.filter((item) => item.id === selectedId)[0]);
    setIsSelectorOpen(false);
  };

  const onSelect = (e) => {
    const {
      currentTarget: {
        dataset: { id },
      },
    } = e;

    setSelectedId(id);
  };

  return (
    <div className={cx("input-selector", className)}>
      <div
        className={cx("input-selector-box", isSelectorOpen && "focus")}
        onClick={onOpen}
      >
        {inputValueItem ? (
          <span className="input-value">{inputValueItem.value}</span>
        ) : (
          <span className="input-placeholder">{placeholder}</span>
        )}
        {isSelectorOpen ? (
          <IconBase>
            <IconArrowUpInput />
          </IconBase>
        ) : (
          <IconBase>
            <IconArrowDownInput />
          </IconBase>
        )}
      </div>

      {isSelectorOpen && (
        <div className="selector-popup">
          <div className="selector-title">
            <span className="title-text">{title}</span>
            <IconBase onClick={onClose}>
              <IconClosePopup />
            </IconBase>
          </div>
          <div className="selector-content">
            <ul className="selector-items">
              {items &&
                items.map((item, idx) => (
                  <li
                    key={idx}
                    className={cx(
                      "selector-item",
                      selectedId === item.id && "selected"
                    )}
                    data-id={item.id}
                    onClick={onSelect}
                  >
                    <span>{item.value}</span>
                    {selectedId === item.id && (
                      <IconBase className="selector-icon">
                        <IconListSelect />
                      </IconBase>
                    )}
                  </li>
                ))}
            </ul>
          </div>
          <div className="selector-button">
            <Button fullWidth onClick={onConfirm}>
              확인
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

export default InputSelector;
