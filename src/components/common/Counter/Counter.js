import React, { useState } from "react";
import { IconMinus, IconPlus } from "components/common/Icons";
import classNames from "classnames";
import "./Counter.scss";

const Counter = ({ type, size ,disabled=false}) => {
  const [number, setNumer] = useState(0);

  const onDecrease = (e) => {
    setNumer(number - 1);
    if (number === 0) {
      alert("Stop");
      setNumer(0);
    }
  };
  const onincrease = (e) => {
    setNumer(number + 1);
  };
  return (
    <div className={classNames("counter-wrap", type, disabled)}>
      {/*typeCenter, typeWide*/}
      <button onClick={onDecrease}>
        <IconMinus fill="#6B7682" size={size} />
      </button>
      <span className="count-number">{number}</span>
      <button onClick={onincrease}>
        <IconPlus fill="#6B7682" size={size} />
      </button>
    </div>
  );
};

export default Counter;
