import React, { useState } from "react";
import Lottie from "react-lottie";
import styled from "styled-components";
import EmptyAnimation from "./EmptyAnimation";

const LottieFix = styled.div`
  margin-top:40px;
  margin-bottom:20px;
`;

const lottieOptions = {
  animationData:EmptyAnimation,
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const EmptyCommon = ({ style }) => {
  return (
    <LottieFix style={style}>
      <Lottie
        options={lottieOptions}
        isClickToPauseDisabled={false}
        style={{ width: "280px", height: "280px" }}
        eventListeners={[
          {
            eventName: "complete",
            callback: () => console.log("the animation completed"),
          },
        ]}
      >
      </Lottie>
    </LottieFix>
  );
};

export default EmptyCommon;
