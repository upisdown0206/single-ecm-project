import React from "react";
import cx from "classnames";
import "./ModalFullPageAni.scss";
import useDelayUnmount from "lib/hooks/useDelayUnmount";

const ModalFullPageAni = ({
  isBaseComponent = false,
  modeState,
  pageName,
  children,
  motion = "horizon", //horizon, vertical, fade
  isDimmed = false,
  className,
}) => {
  const shouldRender = useDelayUnmount(modeState === pageName, 300);

  return (
    <>
      {(isBaseComponent || shouldRender) && (
        <div
          className={cx(
            "modal-full-page-ani",
            isDimmed && "dimmed",
            className,
            isBaseComponent
              ? modeState !== "init" &&
                  (modeState === pageName ? "base-mounted" : "base-unmounted")
              : modeState === pageName
              ? cx(
                  motion === "horizon" && "mounted",
                  motion === "vertical" && "upanddown-mounted",
                  motion === "fade" && "fadeIn"
                )
              : cx(
                  motion === "horizon" && "unmounted",
                  motion === "vertical" && "upanddown-unmounted",
                  motion === "fade" && "fadeOut"
                )
          )}
        >
          {children}
        </div>
      )}
    </>
  );
};

export default ModalFullPageAni;
