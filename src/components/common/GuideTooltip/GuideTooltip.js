import React from "react";
import classNames from "classnames";

import "./GuideTooltip.scss";

const GuideTooltip = ({ className, text, position, style }) => {
  return (
    <div className={classNames("tooltip-wrap", position)} style={style}>
      {text}
    </div>
  );
};

export default GuideTooltip;
