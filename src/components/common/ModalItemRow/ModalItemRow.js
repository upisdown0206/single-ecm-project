import React from "react";
import cx from "classnames";

import "./ModalItemRow.scss";

const ModalItemRow = ({ children, isRowFlexBox = false, ...rest }) => {
  return (
    <div className={cx("modal-item-row", isRowFlexBox && "flex-box")} {...rest}>
      {children}
    </div>
  );
};

export default ModalItemRow;
