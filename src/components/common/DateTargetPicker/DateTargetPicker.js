import React, { useState, useEffect } from "react";

import DatePicker, { registerLocale } from "react-datepicker";

import DimedModal from "components/base/DimedModal";

import Button from "components/common/Button";

import {
  IconCalendarCancel,
  IconArrowDown,
  IconArrowLeft,
  IconArrowRight,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import { getYear, getMonth, getDate, format } from "date-fns";
import ko from "date-fns/locale/ko";

import "react-datepicker/dist/react-datepicker.css";
import "./DateTargetPicker.scss";

registerLocale("ko", ko);

const DateTargetPicker = ({ inDt, setToDate, onClose }) => {
  /*
    TODO:
      - locale
      - after select period, handle year-month selection
  */

  const [currentDt, setCurrentDt] = useState(null);

  const [selectYearMonth, setSelectYearMonth] = useState(new Date());

  const [isSelectYearMonth, setIsSelectYearMonth] = useState(false);

  const onTodayClick = (changeYear, changeMonth) => {
    if (changeYear && changeMonth) {
      changeYear(getYear(new Date()));
      changeMonth(getMonth(new Date()));

      if (isSelectYearMonth) {
        setSelectYearMonth(new Date());
      }
    }
    setCurrentDt(new Date());
  };

  const onChange = (date) => {
    if (isSelectYearMonth) return;
    setCurrentDt(date);
  };

  const onSelect = (date) => {
    if (isSelectYearMonth) {
      const targetDate = new Date(
        getYear(date),
        getMonth(new Date()),
        getDate(new Date())
      );
      setSelectYearMonth(targetDate);
      setCurrentDt(targetDate);
      return;
    }
    setCurrentDt(date);
  };

  const onConfirm = () => {
    if (isSelectYearMonth) {
      setIsSelectYearMonth(false);
    } else {
      if (currentDt) setToDate(currentDt);

      onClose();
    }
  };

  const onCancel = () => {
    onClose();
  };

  const iniitialize = () => {
    // date
    setCurrentDt(inDt);
  };

  useEffect(() => {
    iniitialize();
  }, []);

  return (
    <DimedModal>
      <DatePicker
        renderDayContents={(day) => {
          return <div className="day-circle">{day}</div>;
        }}
        renderCustomHeader={({
          date,
          decreaseMonth,
          increaseMonth,
          changeYear,
          changeMonth,
        }) => (
          <div className="date-calendar-picker-header">
            <div className="header-row">
              <div className="left">
                <IconBase onClick={onCancel}>
                  <IconCalendarCancel />
                </IconBase>
              </div>
              <div className="center">
                <span className="target">
                  {currentDt ? format(currentDt, "yyyy.MM.dd") : "Date"}
                </span>
              </div>
              <div className="right">
                <button
                  className="btn-today"
                  onClick={() => onTodayClick(changeYear, changeMonth)}
                >
                  <span>오늘</span>
                </button>
              </div>
            </div>
            <div className="header-row">
              <button
                className="btn-select-cal"
                onClick={() => setIsSelectYearMonth(!isSelectYearMonth)}
              >
                <span>{`${getYear(date)}년 ${getMonth(date) + 1}월`}</span>
                <IconBase>
                  <IconArrowDown />
                </IconBase>
              </button>
              <div className="month-move-icons">
                {!isSelectYearMonth && (
                  <>
                    <IconBase className="move-icon" onClick={decreaseMonth}>
                      <IconArrowLeft />
                    </IconBase>
                    <IconBase className="move-icon" onClick={increaseMonth}>
                      <IconArrowRight />
                    </IconBase>
                  </>
                )}
              </div>
            </div>
          </div>
        )}
        useWeekdaysShort={true}
        dateFormat="yyyy.MM.dd(eee)"
        shouldCloseOnSelect={false}
        // todayButton={
        //   <button className="btn-today" onClick={onTodayClick}>
        //     <span>오늘</span>
        //   </button>
        // }
        selected={isSelectYearMonth ? selectYearMonth : currentDt}
        onSelect={onSelect}
        onChange={onChange}
        showYearPicker={isSelectYearMonth}
        yearItemNumber={15}
        inline
      >
        <div className="button-container">
          <Button size="medium" color="gray" fullWidth onClick={onConfirm}>
            확인
          </Button>
        </div>
      </DatePicker>
    </DimedModal>
  );
};

export default DateTargetPicker;
