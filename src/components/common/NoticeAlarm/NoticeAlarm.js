import React from 'react';
import classNames from 'classnames'
import {IconSearchEqpt} from "../Icons";
import './NoticeAlarm.scss'
const NoticeAlarm = ({alarmTitle,alarmText,blackType,whiteType, style}) => {
  return (
    <div
      className={classNames("notification-wrap",
        {blackType, whiteType})
      }
      style={style}
    >
      <div className="new-notice">
        {whiteType && <IconSearchEqpt />}
        {blackType &&  <IconSearchEqpt fill1={"#ffffff"} fill2={"#ffffff"} fill3={"#ffffff"} fill4={"#ffffff"} />}
      </div>
      <dl>
        <dt>{alarmTitle}</dt>
        <dd>{alarmText}</dd>
      </dl>
    </div>
  );
};

export default NoticeAlarm;