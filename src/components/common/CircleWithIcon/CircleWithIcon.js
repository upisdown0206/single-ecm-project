import React from "react";
import cx from "classnames";

import "./CircleWithIcon.scss";

const CircleWithIcon = ({ icon, text, className, ...rest }) => {
  return (
    <div className={cx("circle-with-icon", className)} {...rest}>
      <div className="text">{text}</div>
      {icon}
    </div>
  );
};

export default CircleWithIcon;
