import React from "react";
import Lottie from "react-lottie";
import barcode from "asset/lottie/barcode.json";

import "./BarcodeLottie.scss";

const lottieOptions = {
  animationData: barcode,
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const BarcodeLottie = ({ style }) => {
  return (
    <div className="barcode-lottie" style={style}>
      <Lottie
        options={lottieOptions}
        isClickToPauseDisabled={false}
        style={{ width: "228px", height: "228px" }}
        eventListeners={[
          {
            eventName: "complete",
            callback: () => console.log("the animation completed"),
          },
        ]}
      />
    </div>
  );
};

export default BarcodeLottie;
