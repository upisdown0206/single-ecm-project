import React from "react";
import classnames from "classnames";

import "./RoundTag.scss";

const RoundTag = ({ tagName, onClick, active=false }) => {
  return (
    <span className={classnames("tag-round", active)} onClick={onClick}>
      {tagName}
    </span>
  );
};

export default RoundTag;
