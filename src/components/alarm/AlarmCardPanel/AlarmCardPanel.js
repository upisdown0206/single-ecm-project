import React from "react";

import IconBase from "components/common/IconBase";
import {
  IconDailyCheckAlarm,
  IconWorkOrderAlarm,
} from "components/common/Icons";

import "./AlarmCardPanel.scss";

const AlarmCategoryIcon = ({ category }) => {
  if (category === "daily-check") {
    return (
      <IconBase className="alarm-icon pastel-e">
        <IconDailyCheckAlarm width="24" height="24" />
      </IconBase>
    );
  } else if (category === "work-order") {
    return (
      <IconBase className="alarm-icon pastel-f">
        <IconWorkOrderAlarm width="24" height="24" />
      </IconBase>
    );
  } else {
    return <IconBase className="alarm-icon pastel-g"></IconBase>;
  }
};

const AlarmCardPanel = ({}) => {
  // WO
  // 일상점검
  return (
    <div className="alarm-card-panel">
      <div className="title">
        <span className="date">2020년 10월 1일</span>
      </div>
      <div className="alarm-panel">
        <div className="alarm-category">
          <AlarmCategoryIcon category="daily-check" />
          <span className="category-text">일상점검</span>
        </div>
        <ul className="alarm-items">
          <li className="alarm-item">
            <div className="alarm-inform">
              <div className="inform-row">
                <span className="inform-title">일상점검 예약</span>
                <span className="inform-time">09:10</span>
              </div>
              <div className="inform-row">
                <span className="inform-content">
                  OOO에 대한 일상점검이 예약되어있습니다.
                </span>
              </div>
            </div>
          </li>
          <li className="alarm-item">
            <div className="alarm-inform">
              <div className="inform-row">
                <span className="inform-title">일상점검 예약</span>
                <span className="inform-time">09:00</span>
              </div>
              <div className="inform-row">
                <span className="inform-content">
                  OOO에 대한 일상점검이 예약되어있습니다.
                </span>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div className="alarm-panel">
        <div className="alarm-category">
          <AlarmCategoryIcon category="work-order" />
          <span className="category-text">WO</span>
        </div>
        <ul className="alarm-items">
          <li className="alarm-item">
            <div className="alarm-inform">
              <div className="inform-row">
                <span className="inform-title">BM</span>
                <span className="inform-time">10:04</span>
              </div>
              <div className="inform-row">
                <span className="inform-content">
                  WO-123456789:PSTART0205:7999
                </span>
              </div>
            </div>
          </li>
          <li className="alarm-item">
            <div className="alarm-inform">
              <div className="inform-row">
                <span className="inform-title">PM</span>
                <span className="inform-time">09:00</span>
              </div>
              <div className="inform-row">
                <span className="inform-content">
                  WO-123456789:PSTART0205:7999
                </span>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default AlarmCardPanel;
