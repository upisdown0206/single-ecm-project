import React from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import BaseMain from "components/base/BaseMain";

import HeaderTitle from "components/common/HeaderTitle";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";
import AlarmCardPanel from "components/alarm/AlarmCardPanel";

import "./AlarmLanding.scss";

const Splitter = ({ text = undefined }) => (
  <div className={cx("splitter", text && "with-text")}>{text}</div>
);

const AlarmLanding = () => {
  console.log("landing");
  return (
    <div className="alarm-landing">
      <FixedHeader center={<HeaderTitle text="Alarm" />} />
      <BaseMain
        style={{
          height: `calc(100vh - 56px - 60px)`,
          marginTop: `56px`,
        }}
      >
        <ul className="alarm-list-items">
          <li className="alarm-list-item">
            <AlarmCardPanel />
          </li>
          <Splitter />
          <li className="alarm-list-item">
            <AlarmCardPanel />
          </li>
          <Splitter text="최근 30일 동안의 알람만 확일하실 수 있습니다." />
        </ul>
      </BaseMain>
      <FixedFooter>
        <AppNavigation />
      </FixedFooter>
    </div>
  );
};

export default AlarmLanding;
