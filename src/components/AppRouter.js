import React from "react";
import { Route, Switch } from "react-router-dom";

import Main from "pages/Main";
import MainWarehouse from "pages/MainWarehouse";

import Login from "pages/Login";

import Equipment from "pages/Equipment";
import SpareParts from "pages/SpareParts";
import Inventory from "pages/Inventory";
import WorkOrder from "pages/WorkOrder";
import DailyCheck from "pages/DailyCheck";
import Setting from "pages/Setting";
import Alarm from "pages/Alarm";
import Delivery from "pages/Delivery";
import StockTaking from "pages/StockTaking";
import Return from "pages/Return";

import PageList from "components/guide/PageList";
import ComponentAll from "components/guide/ComponentAll";
import ComponentGuide from "components/guide/ComponentGuide";
// import WorkOrderBMSPRequirements from "./workOrder/WorkOrderBMSPRequirements";
// import BMSpRequiermentModalDetail from "./workOrder/BM/BMSpRequiermentModalDetail";
// import BmspInventory from "./workOrder/BM/BMSPSearch";
// import ReadingNFCInprogress from "components/workOrder/ReadingNFCInprogress";
// import WorkOrderBMSPIssue from "../../.backup/WorkOrderBMSPIssue";
// import BMSpIssueModalDetail from "./workOrder/BM/BMSpIssueModalDetail";
// import WorkOrderBMSPIssueDetail from "components/workOrder/WorkOrderBMSPIssueDetail";
import BMSpRequiermentModalDetail from "components/workOrder/BM/BMSpRequiermentModalDetail";
import WorkOrderBMSPIssue from "components/workOrder/WorkOrderBMSPIssue";

const AppRouter = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Main />
      </Route>
      <Route exact path="/main-warehouse">
        <MainWarehouse />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/workorder">
        <WorkOrder />
      </Route>
      <Route path="/equipment">
        <Equipment />
      </Route>
      <Route path="/spareparts">
        <SpareParts />
      </Route>
      <Route path="/inventory">
        <Inventory />
      </Route>
      <Route path="/daily-check">
        <DailyCheck />
      </Route>
      <Route path="/setting">
        <Setting />
      </Route>
      <Route path="/delivery">
        <Delivery />
      </Route>
      <Route path="/stock-taking">
        <StockTaking />
      </Route>
      <Route path="/return">
        <Return />
      </Route>
      <Route path="/alarm">
        <Alarm />
      </Route>

      <Route path="/pageList">
        <PageList />
      </Route>
      <Route path="/component-all">
        <ComponentAll />
      </Route>
      <Route path="/project-template">
        <ComponentGuide />
      </Route>

      <Route path="/current">
        <BMSpRequiermentModalDetail />
      </Route>
    </Switch>
  );
};

export default AppRouter;
