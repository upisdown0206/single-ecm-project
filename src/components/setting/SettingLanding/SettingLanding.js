import React from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import BaseMain from "components/base/BaseMain";

import HeaderTitle from "components/common/HeaderTitle";
import IconBase from "components/common/IconBase";
import {
  IconUpload,
  IconLocation,
  IconClock,
  IconUnion,
  IconSite,
} from "components/common/Icons";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./SettingLanding.scss";

const SettingLanding = () => {
  const randomNum = Math.floor(Math.random() * (6 - 1 + 1) + 1);
  const isMe = false;
  return (
    <div className="setting-landing">
      <FixedHeader center={<HeaderTitle text="Setting" />} />
      <BaseMain
        style={{
          height: `calc(100vh - 56px - 60px)`,
          marginTop: `56px`,
        }}
      >
        <div className="profile-header">
          <div className="ab-right">
            <div className="logout">Logout</div>
            <IconBase className="logout-icon">
              <IconUpload />
            </IconBase>
          </div>
          <div
            className={cx(
              "profile-circle-icon",
              !isMe && `bgcolor-${randomNum}`
            )}
          >
            <span>김</span>
          </div>
          <div className="profile-name">
            <span>김사원</span>
          </div>
          <div className="profile-sub-name">
            <span>PARKMJ</span>
          </div>
        </div>

        <div className="profile-content">
          <div className="content-row">
            <div className="content-main">
              <div className="content-col">
                <IconBase className="content-icon">
                  <IconLocation fill="#6B7682" />
                </IconBase>
                <span className="content-label">Default Buffer Zone</span>
              </div>
              <div className="content-col">
                <span className="content-value">BF1</span>
              </div>
            </div>
          </div>
          <div className="content-row">
            <div className="content-main">
              <div className="content-col">
                <IconBase className="content-icon">
                  <IconClock fill="#6B7682" />
                </IconBase>
                <span className="content-label">Time Zone</span>
              </div>
              <div className="content-col">
                <span className="content-value">Asia/Seoul</span>
              </div>
            </div>
          </div>
          <div className="content-row">
            <div className="content-main">
              <div className="content-col">
                <IconBase className="content-icon">
                  <IconUnion fill="#6B7682" />
                </IconBase>
                <span className="content-label">Department</span>
              </div>
            </div>
            <div className="content-sub">
              <span className="sub-label">Id</span>
              <span className="sub-value">AMNT01</span>
            </div>
            <div className="content-sub">
              <span className="sub-label">Name</span>
              <span className="sub-value">CWA - 전극 보수 1반</span>
            </div>
          </div>
          <div className="content-row">
            <div className="content-main">
              <div className="content-col">
                <IconBase className="content-icon">
                  <IconSite fill="#6B7682" />
                </IconBase>
                <span className="content-label">Site</span>
              </div>
            </div>
            <div className="content-sub">
              <span className="sub-label">Id</span>
              <span className="sub-value">G481</span>
            </div>
            <div className="content-sub">
              <span className="sub-label">Name</span>
              <span className="sub-value">LGCWA</span>
            </div>
          </div>
        </div>
      </BaseMain>
      <FixedFooter>
        <AppNavigation />
      </FixedFooter>
    </div>
  );
};

export default SettingLanding;
