import React, { useState } from "react";
import DimedModal from "components/base/DimedModal";
import imgNFCMoving from "asset/images/nfc.gif";

import "./ReadingNFCInprogress.scss";
import MessageBar from "components/common/MessageBar";
import {
  IconListSelectThin,
  IconClosePopup,
  IconNFCTag,
} from "components/common/Icons";

const ReadingNFCInprogress = ({ onClose }) => {
  return (
    <DimedModal>
      <MessageBar
        style={{ bottom: "398px" }}
        iconType={
          <span className="icon-notice-success">
            <IconListSelectThin strokeColor={"#ffffff"} size={"15px"} />
          </span>
        }
        messageText={"승인되었습니다"}
      />
      <div className="nfc-popup-wrapper">
        <div className="modal-header">
          <h1>
            {/*태킹완료 후 아이콘 사라짐*/}
            <IconNFCTag width={"24px"} height={"24px"} fill={"#2C3238"} />
            NFC Tagging
          </h1>
          <button onClick={onClose}>
            <IconClosePopup />
          </button>
        </div>
        <div className="img-nfc-wrap">
          <p>
            인식중...
            <br />
            핸드폰을 기기에 계속 태깅하세요
          </p>
          <img src={imgNFCMoving} />
        </div>
      </div>
    </DimedModal>
  );
};

export default ReadingNFCInprogress;
