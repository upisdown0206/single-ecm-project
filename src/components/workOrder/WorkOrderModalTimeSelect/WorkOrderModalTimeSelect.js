import React from "react";

import {
  IconClosePopup,
} from "components/common/Icons";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";

import "components/dailyCheck/dc-common/DailyCheckCommon.scss";
import "./WorkOrderModalTimeSelect.scss";
import Button from "../../common/Button";

const WorkOrderModalTimeSelect = () => {
  return (
    <DailyCheckModal
      title="Voice memo"
      background="bg-type"
      position="modal-fix-bottom"
    >
      <div className="fix-pop-wrap">
        <div className="modal-header">
          <h1>
            시작 시간
          </h1>
          <button className="btn-popup-close">
            <IconClosePopup />
          </button>
        </div>
        <div className="time-select-wrap">
          <div className="time-day-night">
            <ul>
              <li>&nbsp;</li>
              <li><a href="" className="active">AM</a></li>
              <li><a href="">PM</a></li>
              <li>&nbsp;</li>
            </ul>
          </div>
          <div className="time-hour">
            <ul>
              <li><a href="">1</a></li>
              <li><a href="">2</a></li>
              <li><a href="" className="active">3</a></li>
              <li><a href="">4</a></li>
              <li><a href="">5</a></li>
              <li><a href="">6</a></li>
              <li><a href="">7</a></li>
              <li><a href="">8</a></li>
              <li><a href="">9</a></li>
              <li><a href="">10</a></li>
              <li><a href="">11</a></li>
              <li><a href="">12</a></li>
            </ul>
          </div>
          <div className="time-minute">
            <ul>
              <li><a href="">1</a></li>
              <li><a href="">2</a></li>
              <li><a href="" className="active">3</a></li>
              <li><a href="">4</a></li>
              <li><a href="">5</a></li>
              <li><a href="">6</a></li>
              <li><a href="">7</a></li>
              <li><a href="">8</a></li>
              <li><a href="">9</a></li>
              <li><a href="">10</a></li>
              <li><a href="">11</a></li>
              <li><a href="">12</a></li>
            </ul>
          </div>
        </div>

        <div className="comm-fix-bottom">
          <Button fullWidth>
            확인
          </Button>
        </div>
      </div>
    </DailyCheckModal>
  );
};

export default WorkOrderModalTimeSelect;
