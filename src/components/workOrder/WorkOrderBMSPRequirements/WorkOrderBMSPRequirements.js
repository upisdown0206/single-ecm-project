import React, { useState } from "react";
import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseMain from "components/base/BaseMain";
import IconBase from "components/common/IconBase";
import {
  IconClosePopupBold,
  IconCommBack,
  IconTrashCan,
  IconPlus,
} from "components/common/Icons";
import "./WorkOrderBMSPRequirements.scss";
import Button from "../../common/Button";
import BMSpRequirementItem from "../BM/BMSpRequirementItem";
import BMSpRequiermentModalCheckType from "../BM/BMSpRequiermentModalCheckType";
import BMSpRequiermentModalDetail from "../BM/BMSpRequiermentModalDetail";
import ModalFullPageAni from "../../common/ModalFullPageAni";
import DimedModal from "../../base/DimedModal";
import OKCancelModal from "../../modal/OKCancelModal";
import MessageBar from "../../common/MessageBar";

const WorkOrderBMSPRequirements = ({ onClick }) => {
  const [mode, setMode] = useState("");
  const onClose = () => {
    setMode("");
  };
  const [isDeleteAdd, setIsDeleteAdd] = useState(false);
  const onDeleteClick = () => {
    setIsDeleteAdd(!isDeleteAdd);
  };
  const [isDeleteItem, setIsDeleteItem] = useState(false);
  const onDeleteItemClick = () => {
    setIsDeleteItem(!isDeleteItem);
    setIsDeleteAdd(!isDeleteAdd);
    setTimeout(() => setIsDeleteItem(false), 5000);
  };

  const [isChecked, setIsChecked] = useState(false);
  const onSelectClick = () => {
    setIsChecked(!isChecked);
  };

  const [isConfirm, setConfirm] = useState(false);
  const onsetConfirm = () => {
    setConfirm(!isConfirm);
    setRequest(!isRequest);
    setTimeout(() => setRequest(false), 5000);
    setTimeout(() => setConfirm(false), 5000);
  };

  const [isRequest, setRequest] = useState(false);
  const onsetRequest = () => {
    setRequest(!isRequest);
  };

  const [isNewItemAdd, setNewItemAdd] = useState(false);
  const onNewItemAdd = () => {
    setNewItemAdd(!isNewItemAdd);
  };

  const [isSPInventory, setIsSPInventory] = useState(false);
  const onSpInventory = () => {
    setIsSPInventory(!isSPInventory);
  };

  const [isHistory, setIsHistory] = useState(false);
  const onSPHistory = (e) => {
    setIsHistory(!isHistory);
  };

  const [isCheckClick, setCheckClick] = useState(false);

  return (
    <div className="work-order-bm-sp">
      <FixedHeader
        left={
          <IconBase onClick={onClick}>
            <IconCommBack />
          </IconBase>
        }
        center={
          isDeleteAdd ? (
            <>
              <IconTrashCan fill={"#2C3238"} />
              Delete
            </>
          ) : (
            "SP Requirements"
          )
        }
        right={
          <IconBase onClick={onDeleteClick}>
            {isDeleteAdd ? (
              <IconClosePopupBold fill={"#2C3238"} />
            ) : (
              <IconTrashCan fill={"#2C3238"} />
            )}
          </IconBase>
        }
      />
      <BaseMain
        style={{
          height: `calc(100vh - (56px + 88px))`,
          marginTop: `calc(56px)`,
          background: "white",
        }}
      >
        <div className="work-order-sp-requirements">
          <div className="wospr-header">
            <div className="row">
              <div className="wospr-title">WO-123675415</div>
            </div>
          </div>

          <div className="wospr-contents-list">
            <BMSpRequirementItem
              onClick={() => setMode("SP_REQUIREMENT/DETAIL")}
              isDeleteAdd={isDeleteAdd}
              isChecked={isChecked}
            />
            <button className="new-item-add" onClick={onNewItemAdd}>
              <IconPlus fill="#6B7682" />
              Add New
            </button>
          </div>
        </div>
      </BaseMain>
      <FixedFooter>
        {!isDeleteAdd ? (
          <div style={{ padding: "18px 20px" }}>
            <Button fullWidth color="primary" onClick={onsetRequest}>
              Reservation &amp; Transfer Request
            </Button>
          </div>
        ) : (
          <div className="comm-fix-bottom div2">
            <div className="total-type">
              <span>1</span>/ 2
            </div>
            <Button onClick={onDeleteItemClick} color="primary">
              지우기
            </Button>
          </div>
        )}
      </FixedFooter>

      {isNewItemAdd && (
        <BMSpRequiermentModalCheckType
          onClick={onNewItemAdd}
          onChange={onSelectClick}
        />
      )}

      <ModalFullPageAni
        motion="vertical"
        modeState={mode}
        pageName="SP_REQUIREMENT/DETAIL"
      >
        <BMSpRequiermentModalDetail onClose={onClose} />
      </ModalFullPageAni>

      {isRequest && (
        <DimedModal className="modal-top-max work-order-save-check-modal">
          <OKCancelModal
            title="자재요청 하시겠습니까?"
            onConfirm={onsetConfirm}
            onClose={onsetConfirm}
          >
            <p className="modal-content">2건의 자재요청을 진행합니다.</p>
          </OKCancelModal>
        </DimedModal>
      )}

      {isConfirm && (
        <MessageBar
          messageText="자제요청을 완료했습니다."
          style={{ bottom: "108px" }}
        />
      )}
      {isDeleteItem && (
        <MessageBar
          style={{ bottom: "108px" }}
          messageText={"1건이 삭제되었습니다. "}
          refresh={<span className="btn-try-again">되돌리기</span>}
        />
      )}
    </div>
  );
};

export default WorkOrderBMSPRequirements;
