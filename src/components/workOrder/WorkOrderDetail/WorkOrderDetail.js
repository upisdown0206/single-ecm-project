import React, { useState, useEffect } from "react";
import moment from "moment";
import cx from "classnames";

import WorkOrderBM from "components/workOrder/WorkOrderBM";

import ModalFullPage from "components/common/ModalFullPage";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import WorkOrderTitlePanel from "components/workOrder/WorkOrderTitlePanel";
import WorkOrderStepBox from "components/workOrder/WorkOrderStepBox";
import WorkOrderManage from "components/workOrder/WorkOrderManage";
import AttachedFileTextBox from "components/workOrder/AttachedFileTextBox";
import WorkOrderOperationHour from "components/workOrder/WorkOrderOperationHour";
import WorkOrderPushHistory from "components/workOrder/WorkOrderPushHistory";

import OKCancelModal from "components/modal/OKCancelModal";
import DimedModal from "components/base/DimedModal";

import {
  IconCommBack,
  IconHistory,
  IconPlus,
  IconMinus,
  IconArrowRight,
  IconWOEmployee,
  IconGrayPolygon,
  IconRingPhone,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./WorkOrderDetail.scss";

const WorkOrderDetail = ({ woNo, woType, onClose }) => {
  const [mode, setMode] = useState("init");
  const [popupMode, setPopupMode] = useState("");
  const onInnerClose = () => {
    setMode("");
  };
  const onModalClose = () => {
    setPopupMode("");
  };

  const [isbtnHistoryToggle, setbtnHistoryToggle] = useState(false);
  const [woBMProcessItems] = useState([
    {
      processId: "101",
      processName: "Safety Plan",
      processStatus: "done",
      isDone: false,
      type: "safetyPlan",
    },
    {
      processId: "201",
      processName: "Diagnosis",
      processStatus: "not-start",
      isDone: true,
      type: "diagnosis",
    },
  ]);
  const [woPMProcessItems] = useState([
    {
      processId: "101",
      processName: "Safety Plan",
      processStatus: "done",
      isDone: true,
      type: "safetyPlan",
    },
    {
      processId: "201",
      processName: "OP",
      processStatus: "done",
      isDone: true,
      type: "op",
    },
  ]);

  const [isAttachedExpand, setIsAttachedExpand] = useState(false);
  const [attachedFiles, setAttachedFiles] = useState([
    {
      id: "1",
      filename: "attached file 1 attached file 1 attached file 1",
    },
    {
      id: "2",
      filename: "attached file 2",
    },
    //   {
    //     id: "3",
    //     filename: "attached file 3",
    //   },
    //   {
    //     id: "4",
    //     filename: "attached file 4",
    //   },
  ]);

  const [isEmployeeExpand, setIsEmployeeExpand] = useState(false);
  const [employees, setEmployees] = useState([
    "홍길동",
    "가나다",
    "라마바",
    "사아자",
    "홍길동",
    "가나다",
    "사아자",
    "홍길동",
    "가나다",
    "사아자",
    "홍길동",
    "가나다",
    "사아자",
    "홍길동",
    "가나다",
  ]);

  const onBackClick = () => {
    // hide page
    onClose();
  };

  const btnHistoryToggle = (e) => {
    setbtnHistoryToggle(!isbtnHistoryToggle);
    setPopupMode("WORK_ORDER_PUSH_HISTORY");
  };

  const onEmployeeClick = (e) => {
    const {
      target: { tagName },
    } = e;
    // if (["button", "svg", "path"].includes(tagName)) {
    //   return;
    // }
    setPopupMode("WORK_ORDER_OPERATION_HOUR");
  };

  const onExpandToggleEmployee = (e) => {
    e.preventDefault();
    setIsAttachedExpand(!isAttachedExpand);
  };

  const onStepClick = (type) => {
    if (type === "safetyPlan") {
      setMode("WORK_ORDER_MANAGE/sp");
    } else if (type === "diagnosis") {
      setMode("WORK_ORDER_MANAGE/diagnosis");
    } else if (type === "op") {
      setMode("WORK_ORDER_MANAGE/op");
    }
  };

  const [actualProcessing, setActualProcesssing] = useState(false);
  //const [timerStatus, setTimerStatus] = useState("STOP"); // ACTIVE, PAUSE, STOP
  const [time, setTime] = useState(0);
  const [timeTick, setTimeTick] = useState(null); //<NodeJS.Timeout | null>

  const startTimer = () => {
    const timeTick = setInterval(() => {
      setTime((time) => time + 1);
    }, 1000);
    setTimeTick(timeTick);
  };

  const pauseTimer = () => {
    if (timeTick) {
      clearInterval(timeTick);
    }
  };

  const stopTimer = () => {
    pauseTimer();
    setTime(0);
  };

  const formatTime = () => {
    const getSeconds = `0${time % 60}`.slice(-2);
    const minutes = `${Math.floor(time / 60)}`;
    const getMinutes = `0${minutes % 60}`.slice(-2);
    const getHours = `0${Math.floor(time / 3600)}`.slice(-2);

    return `${getHours} : ${getMinutes} : ${getSeconds}`;
  };

  const onActualClick = () => {
    // 진행중이면 stop
    // 멈춰있으면 start
    if (actualProcessing) {
      stopTimer();
    } else {
      startTimer();
    }
    setActualProcesssing(!actualProcessing);
  };

  const [clearAlertModal, setClearAlertModal] = useState(false);
  const onCancelModal = () => {
    startTimer();
    setClearAlertModal(false);
  };
  const onConfirmModal = () => {
    stopTimer();
    setClearAlertModal(false);
    setActualProcesssing(false);
  };

  // implements ellipse icon, class
  const [showExpandIcon, setShowExpandIcon] = useState(false);
  const [expandEllipse, setExpandEllipse] = useState(false);
  const onToggleExpandEllipse = () => {
    setExpandEllipse(!expandEllipse);
  };

  let spanRef = React.createRef();
  useEffect(() => {
    if (spanRef && spanRef.current) {
      const offsetHeight = spanRef.current.offsetHeight;
      const lineHeight = parseInt(spanRef.current.style.lineHeight);
      const lineCount = offsetHeight / lineHeight;

      if (lineCount >= 4) {
        setShowExpandIcon(true);
      } else {
        setShowExpandIcon(false);
      }
    }
  }, [spanRef]);

  return (
    <>
      <ModalFullPageAni
        className="work-order-detail"
        isBaseComponent={true}
        modeState={mode}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Work Order Detail" />}
        />

        <main
          className="work-order-detail-main"
          style={{
            height: `calc(100vh - (56px))`,
            marginTop: `calc(56px)`,
          }}
        >
          {woType === "BM" ? (
            <WorkOrderTitlePanel
              progressState="In progress"
              woNo={woNo}
              woType="bm"
            />
          ) : (
            <WorkOrderTitlePanel
              progressState="In progress"
              woNo={woNo}
              woType="pm"
            />
          )}

          <div className="wod-step-tab">
            <WorkOrderStepBox
              woProcessItems={
                woType === "BM" ? woBMProcessItems : woPMProcessItems
              }
              onStepClick={onStepClick}
            />
          </div>

          <div className="wod-content">
            <div className="wod-content-items">
              <dl>
                <dt>
                  Description
                  {showExpandIcon && (
                    <button
                      className="btn-toggle"
                      onClick={onToggleExpandEllipse}
                    >
                      {expandEllipse ? (
                        <IconMinus size="16px" fill="#6B7682" />
                      ) : (
                        <IconPlus size="16px" />
                      )}
                    </button>
                  )}
                </dt>
                <dd>
                  <span
                    ref={spanRef}
                    style={{ lineHeight: "20px" }}
                    className={cx(!expandEllipse && "line-ellipse")}
                  >
                    [MES-BM] 04693ZZF2352 FLOATING COATER SYSTEM__FCS Slider1
                    Chuck 진공 Sensor ON 이상 SPX854 (2352 [MES-BM] 04693ZZF2352
                    FLOATING COATER COATER COATER COATER COATER
                    COATERCOATERCOATER
                  </span>
                </dd>
              </dl>
              <dl>
                <dt>Scheduled Date</dt>
                <dd>
                  <span className="date">2020-08-11</span>
                  <span className="time">14:30</span>
                  <div className="dash-type" />
                  <span className="date">2020-08-11</span>
                  <span className="time">14:30</span>
                </dd>
              </dl>
              <dl>
                <dt>MES Down time</dt>
                <dd>
                  <span className="date">2020-08-11</span>
                  <span className="time">14:30</span>
                  <div className="dash-type" />
                  <span className="date">2020-08-11</span>
                  <span className="time">14:30</span>
                </dd>
              </dl>
              <dl>
                <dt>
                  Attached Files
                  {attachedFiles && attachedFiles.length > 1 && (
                    <button
                      className="btn-toggle"
                      onClick={onExpandToggleEmployee}
                    >
                      {isAttachedExpand ? (
                        <IconMinus size="16px" fill="#6B7682" />
                      ) : (
                        <IconPlus size="16px" />
                      )}
                    </button>
                  )}
                </dt>
                <dd>
                  {/* TODO: Link to='viewer' */}
                  {attachedFiles &&
                  attachedFiles.length > 0 &&
                  isAttachedExpand ? (
                    <div className="flex-col">
                      {attachedFiles.map((item) => (
                        <AttachedFileTextBox
                          key={item.id}
                          text={item.filename}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="flex-row">
                      <AttachedFileTextBox text={attachedFiles[0].filename} />
                      {attachedFiles.length > 1 && (
                        <span className="plus-number-text">{`+${
                          attachedFiles.length - 1
                        }`}</span>
                      )}
                    </div>
                  )}
                </dd>
              </dl>
            </div>

            <div
              className="wod-content-items one-item"
              onClick={onEmployeeClick}
            >
              <dl>
                <dt>
                  <div className="icon-labeled">
                    <IconWOEmployee />
                    <span>Employee</span>
                  </div>
                  {/* {employees && employees.length > 6 && (
                  <button
                    className="btn-toggle"
                    onClick={() => setIsEmployeeExpand(!isEmployeeExpand)}
                  >
                    {isEmployeeExpand ? (
                      <IconMinus size="16px" fill="#6B7682" />
                    ) : (
                      <IconPlus size="16px" />
                    )}
                  </button>
                )} */}
                </dt>
                <dd>
                  <div className="wod-item-employees">
                    {isEmployeeExpand ? (
                      employees.map((item, idx) => (
                        <React.Fragment key={idx}>
                          <span className="wod-item-employee">{item}</span>
                          {(idx + 1) % 6 === 0 && <span className="newline" />}
                        </React.Fragment>
                      ))
                    ) : employees?.length > 6 ? (
                      <>
                        {employees.map(
                          (item, idx) =>
                            idx < 5 && (
                              <span key={idx} className="wod-item-employee">
                                {item}
                              </span>
                            )
                        )}
                        <span className="wod-item-employee">
                          {`+ ${employees.length - 5}`}
                        </span>
                      </>
                    ) : (
                      employees.map((item, idx) => (
                        <span key={idx} className="wod-item-employee">
                          {item}
                        </span>
                      ))
                    )}
                  </div>
                </dd>
              </dl>
              <IconBase className="ab-center">
                <IconArrowRight />
              </IconBase>
            </div>

            <div
              className="wod-content-items one-item"
              onClick={btnHistoryToggle}
            >
              {/* <button
              className={classNames(
                "time-history",
                `${isbtnHistoryToggle ? "active" : ""}`
              )}
              onClick={btnHistoryToggle}
            >
              <IconHistory
                fill={`${isbtnHistoryToggle ? "#ffffff" : "#68D6E8"}`}
                size="28px"
              />
            </button> */}
              <dl>
                <dt>
                  <div className="icon-labeled">
                    <IconHistory width="24" height="24" fill="#8996A3" />
                    <span>Equipment ID</span>
                  </div>
                </dt>
                <dd>
                  <span>P7TPHT0104</span>
                </dd>
              </dl>
              <IconBase className="ab-center">
                <IconArrowRight />
              </IconBase>
            </div>
          </div>
        </main>
        <FixedFooter>
          <div className="btn-round-wrapper">
            <div className="btn-ncf-tag">
              <div className="row">
                <span>Actual Time</span>
                <IconBase className="rectangle">
                  <IconRingPhone />
                </IconBase>
              </div>
              <div className="row">
                <div
                  className={cx("clear-button", actualProcessing && "active")}
                  onClick={
                    actualProcessing
                      ? () => {
                          pauseTimer();
                          setClearAlertModal(true);
                        }
                      : null
                  }
                >
                  Clear
                </div>
                <span className="ellapsed-time">{formatTime()}</span>
              </div>
              <IconBase
                className="absolute-right circle"
                onClick={onActualClick}
              >
                {actualProcessing ? (
                  <div className="actual-rectangle" />
                ) : (
                  <IconGrayPolygon width="30" height="30" fill="#ED3A72" />
                )}
              </IconBase>
            </div>
          </div>
        </FixedFooter>
        {clearAlertModal && (
          <DimedModal className="modal-top-max work-order-detail-clear-modal">
            <OKCancelModal
              title="Reset"
              onConfirm={onConfirmModal}
              onClose={onCancelModal}
            >
              <p className="modal-content">
                <span className="timer-time">{formatTime()}</span>
                에 대한 기록을 reset하시겠습니까?
                <br />
                확인을 누르면 기록이 삭제됩니다.
              </p>
            </OKCancelModal>
          </DimedModal>
        )}
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_MANAGE/sp">
        <WorkOrderManage
          woNo={woNo}
          woType={woType}
          tabPage="sp"
          onClose={onInnerClose}
        />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_MANAGE/diagnosis">
        <WorkOrderManage
          woNo={woNo}
          woType={woType}
          tabPage="diagnosis"
          onClose={onInnerClose}
        />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_MANAGE/op">
        <WorkOrderManage
          woNo={woNo}
          woType={woType}
          tabPage="op"
          onClose={onInnerClose}
        />
      </ModalFullPageAni>

      <ModalFullPageAni
        modeState={popupMode}
        pageName="WORK_ORDER_OPERATION_HOUR"
        motion="vertical"
      >
        <WorkOrderOperationHour onClose={onModalClose} />
      </ModalFullPageAni>

      <ModalFullPageAni
        modeState={popupMode}
        pageName="WORK_ORDER_PUSH_HISTORY"
        motion="vertical"
      >
        <WorkOrderPushHistory onClose={onModalClose} />
      </ModalFullPageAni>
      {/* {mode === "READING_NFC" && (
        <ModalFullPage>
          <ReadingNFCInprogress onClose={onInnerClose} />
        </ModalFullPage>
      )} */}
    </>
  );
};

export default WorkOrderDetail;
