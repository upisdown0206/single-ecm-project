import React from "react";
import cx from "classnames";

import { IconNextStep } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderStepButtonTypeA.scss";

const WorkOrderStepButtonTypeA = ({ content, className, onClick, ...rest }) => {
  return (
    <div className="work-order-step-button-typeA" onClick={onClick} {...rest}>
      <div className={cx("inner", className)}>
        <span className="content">{content}</span>
        <div className="absolute-icon">
          <IconBase>
            <IconNextStep />
          </IconBase>
        </div>
      </div>
    </div>
  );
};

export default WorkOrderStepButtonTypeA;
