import React from "react";

import "./WorkOrderBMDiagnosisFailureTypeA.scss";

const WorkOrderBMDiagnosisFailureTypeA = ({
  id,
  content,
  frequency,
  leadTime,
  woNo,
  description,
  isSelected,
}) => {
  return (
    <div className="work-order-bm-diagnosis-failure-typeA">
      <div className="infos">
        <div className="info-box">
          <div className="info-title">Frequency</div>
          <div className="info-content">
            <span className="value">{frequency}</span>
            <span className="unit">%</span>
          </div>
        </div>
        <div className="info-box">
          <div className="info-title">Lead Time</div>
          <div className="info-content">
            <span className="value">{leadTime}</span>
            <span className="unit">Min</span>
          </div>
        </div>
      </div>
      <div className="infos flex-col">
        <div className="info-line">
          <span className="label">WO No</span>
          <span className="content">WO-{woNo}</span>
        </div>
        <div className="info-line">
          <span className="label">Description</span>
          <span className="content">{description}</span>
        </div>
      </div>
    </div>
  );
};

export default WorkOrderBMDiagnosisFailureTypeA;
