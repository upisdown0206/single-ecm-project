import React, { useState } from "react";
import cx from "classnames";

import SelectInputDiv from "components/search/SelectInputDiv";
import LabelInputTypeE from "components/search/LabelInputTypeE";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";
import DimedModal from "components/base/DimedModal";
import SingleSelect from "components/common/select/SingleSelect";
import ScrollableModal from "components/base/ScrollableModal";
import CounterInput from "components/common/CounterInput";

import { IconArrowRightBig, IconComment } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderBMSPIssueItemPanel.scss";

const WorkOrderBMSPIssueItemPanel = ({ ...rest }) => {
  const [mode, setMode] = useState("init");
  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");
  const [searchExpandMode, setSearchExpandMode] = useState(false);
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const [keywords, setKeywords] = useState([
    {
      id: "1",
      type: "equipment",
      text: "P7TCVD01-0101",
      subText: "Bolt, Mobile, A109K-4000H",
    },
  ]);

  const onItemClick = () => {
    setFormType("itemCode");
    setMode("FORM_DETAIL");
    setSearchInitValue("");
    setSearchExpandMode(false);
  };

  const categorySelectItems = [
    {
      id: "c1",
      value: "Category 1",
    },
    {
      id: "c2",
      value: "Category 2",
    },
    {
      id: "c3",
      value: "Category 3",
    },
    {
      id: "c4",
      value: "Category 4",
    },
  ];
  const [selectedCategoryId, setSelectedCategoryId] = useState("");

  const reasonSelectItems = [
    {
      id: "r1",
      value: "Reason 1",
    },
    {
      id: "r2",
      value: "Reason 2",
    },
    {
      id: "r3",
      value: "Reason 3",
    },
    {
      id: "r4",
      value: "Reason 4",
    },
  ];
  const [selectedReasonId, setSelectedReasonId] = useState("");

  const [qty, setQty] = useState({
    repairQty: 0,
    discardQty: 0,
    notDeassignQty: 0,
    unlQty: 0,
    additionalQty: 0,
  });
  const getIssueQty = () => {
    const sum =
      qty.repairQty +
      qty.discardQty +
      qty.notDeassignQty +
      qty.unlQty +
      qty.additionalQty;
    return sum;
  };
  return (
    <>
      <div className="work-order-bm-sp-issue-item-panel" {...rest}>
        <div className="row">
          <div className="panel">
            <div className="qty-box">
              <div className="label">Issue Qty</div>
              <div className="value">{getIssueQty()}</div>
            </div>
            <div className="splitter" />
            <div className="qty-box">
              <div className="label">EBOM Qty</div>
              <div className="value">2</div>
            </div>
          </div>
        </div>
        <div className="row">
          <LabelInputTypeE
            labelText="Equipment"
            keywords={keywords}
            setKeywords={setKeywords}
            searchType="equipment"
            onInputClick={onItemClick}
            setKeyboardFocus={setIsKeyboardFocus}
            necessary={<span className={"necessary-dot"}></span>}
          />
        </div>
        <div className="row">
          <LabelInputTypeE
            labelText="Serial No (IN)"
            keywords={keywords}
            setKeywords={setKeywords}
            searchType="serialNoIn"
            onInputClick={onItemClick}
            setKeyboardFocus={setIsKeyboardFocus}
          />
        </div>
        <div className="row">
          <LabelInputTypeE
            labelText="Serial No (Out)"
            keywords={keywords}
            setKeywords={setKeywords}
            searchType="serialNoOut"
            onInputClick={onItemClick}
            setKeyboardFocus={setIsKeyboardFocus}
          />
        </div>
        <div className="row flex-spb border-bottom">
          <div className="col">
            <div className="label">
              Quantity<span className={"necessary-dot"}></span>
            </div>
          </div>
          <div
            className="col"
            onClick={() => setMode("SP_ISSUE_DETAIL/ADD_QUANTITY")}
          >
            <div className="add-text">Edit</div>
            <IconBase className="add-icon">
              <IconArrowRightBig />
            </IconBase>
          </div>
        </div>
        <div className="row-alter">
          <SelectInputDiv
            labelText="Replacement Category"
            placeholder="Placeholder"
            value={
              categorySelectItems.find((item) => item.id === selectedCategoryId)
                ?.value
            } //test
            onClick={() => setMode("SP_ISSUE_DETAIL/SELECT_CATEGORY")}
          />
        </div>
        <div className="row-sub">
          <div className="label-sub">Reason for Replacement</div>
          <SelectInputDiv
            labelText=""
            className={
              selectedCategoryId && selectedCategoryId !== "" && "active"
            }
            placeholder="Placeholder"
            value={
              reasonSelectItems.find((item) => item.id === selectedReasonId)
                ?.value
            } //test
            onClick={() => {
              if (!selectedCategoryId || selectedCategoryId === "") return;
              setMode("SP_ISSUE_DETAIL/SELECT_REASON");
            }}
          />
        </div>
        <div className="comment-panel">
          <div className="comment-header">
            <IconBase className="comment-icon">
              <IconComment width="24" height="24" />
            </IconBase>
            <div className="comment-title">Comment</div>
          </div>
          <textarea className="comment-ta" placeholder="Placeholder" />
        </div>
      </div>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={() => setMode("")}
          initValue={searchInitValue}
          isExpandMode={searchExpandMode}
        />
      </ModalFullPageAni>
      {mode === "SP_ISSUE_DETAIL/ADD_QUANTITY" && (
        <ScrollableModal onTouchHandler={() => setMode("")}>
          <div className="add-quantity-modal">
            <div className="row flex-spb">
              <div className="label">Repair Qty</div>
              <CounterInput
                value={qty.repairQty}
                setValue={(value) =>
                  setQty({
                    ...qty,
                    repairQty: value,
                  })
                }
                className="counter-input"
                onIncrease={() =>
                  setQty({
                    ...qty,
                    repairQty: qty.repairQty + 1,
                  })
                }
                onDecrease={() =>
                  setQty({
                    ...qty,
                    repairQty: qty.repairQty - 1 < 0 ? 0 : qty.repairQty - 1,
                  })
                }
              />
            </div>
            <div className="row flex-spb">
              <div className="label">Discard Qty</div>
              <CounterInput
                value={qty.discardQty}
                setValue={(value) =>
                  setQty({
                    ...qty,
                    discardQty: value,
                  })
                }
                className="counter-input"
                onIncrease={() =>
                  setQty({
                    ...qty,
                    discardQty: qty.discardQty + 1,
                  })
                }
                onDecrease={() =>
                  setQty({
                    ...qty,
                    discardQty: qty.discardQty - 1 < 0 ? 0 : qty.discardQty - 1,
                  })
                }
              />
            </div>
            <div className="row-alter flex-spb">
              <div className="label">Not Deassign Qty</div>
              <CounterInput
                value={qty.notDeassignQty}
                setValue={(value) =>
                  setQty({
                    ...qty,
                    notDeassignQty: value,
                  })
                }
                className="counter-input"
                onIncrease={() =>
                  setQty({
                    ...qty,
                    notDeassignQty: qty.notDeassignQty + 1,
                  })
                }
                onDecrease={() =>
                  setQty({
                    ...qty,
                    notDeassignQty:
                      qty.notDeassignQty - 1 < 0 ? 0 : qty.notDeassignQty - 1,
                  })
                }
              />
            </div>
            <div className="row-sub">
              <div className="label-sub">Reason for Replacement</div>
              <textarea
                className={cx("comment-ta", qty.notDeassignQty > 0 && "active")}
                placeholder="Placeholder"
              />
            </div>
            <div className="row flex-spb">
              <div className="label">UNL Qty</div>
              <CounterInput
                value={qty.unlQty}
                setValue={(value) =>
                  setQty({
                    ...qty,
                    unlQty: value,
                  })
                }
                className="counter-input"
                onIncrease={() =>
                  setQty({
                    ...qty,
                    unlQty: qty.unlQty + 1,
                  })
                }
                onDecrease={() =>
                  setQty({
                    ...qty,
                    unlQty: qty.unlQty - 1 < 0 ? 0 : qty.unlQty - 1,
                  })
                }
              />
            </div>
            <div className="row flex-spb">
              <div className="label">Additional Qty</div>
              <CounterInput
                value={qty.additionalQty}
                setValue={(value) =>
                  setQty({
                    ...qty,
                    additionalQty: value,
                  })
                }
                className="counter-input"
                onIncrease={() =>
                  setQty({
                    ...qty,
                    additionalQty: qty.additionalQty + 1,
                  })
                }
                onDecrease={() =>
                  setQty({
                    ...qty,
                    additionalQty:
                      qty.additionalQty - 1 < 0 ? 0 : qty.additionalQty - 1,
                  })
                }
              />
            </div>
          </div>
        </ScrollableModal>
      )}
      {mode === "SP_ISSUE_DETAIL/SELECT_CATEGORY" && (
        <DimedModal className="sp-issue-detail-select-modal">
          <SingleSelect
            title="Replacement Category"
            items={categorySelectItems}
            onClose={() => setMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === selectedCategoryId) {
                setSelectedCategoryId(null);
              } else {
                setSelectedCategoryId(e.currentTarget.id);
              }
              setMode("");
            }}
            selectedId={selectedCategoryId} //test
          />
        </DimedModal>
      )}
      {mode === "SP_ISSUE_DETAIL/SELECT_REASON" && (
        <DimedModal className="sp-issue-detail-select-modal">
          <SingleSelect
            title="Reason for Replacement"
            items={reasonSelectItems}
            onClose={() => setMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === selectedReasonId) {
                setSelectedReasonId(null);
              } else {
                setSelectedReasonId(e.currentTarget.id);
              }
              setMode("");
            }}
            selectedId={selectedReasonId} //test
          />
        </DimedModal>
      )}
    </>
  );
};

export default WorkOrderBMSPIssueItemPanel;
