import React, { useState } from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseMain from "components/base/BaseMain";

import HeaderTitle from "components/common/HeaderTitle";
import { IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import ModalFullPageAni from "components/common/ModalFullPageAni";

import WorkOrderBMSPIssueCard from "components/workOrder/WorkOrderBMSPIssueCard";
import WorkOrderBMSPIssueDetail from "components/workOrder/WorkOrderBMSPIssueDetail";

import "./WorkOrderBMSPIssue.scss";

const data = [
  {
    idx: 10,
    id: "BMM12345",
    location: "BF 1",
    status: "NEW",
    description: "Bolt, Mobile, A109K-4000H dfdsfdsfdssd ddddd ddd dd",
    reservedQty: "992",
    remainQty: "000",
    issueQty: 3,
    toBFQty: 3,
    toStoreroomQty: 3,
    toSupplierQty: 3,
  },
  {
    idx: 11,
    id: "BMM12346",
    location: "BF 1",
    status: "NEW",
    description: "aaaa aaaa aaa aa, A109K-4000H dfdsfdsfdssd ddddd ddd dd",
    reservedQty: "990",
    remainQty: "000",
    issueQty: 2,
    toBFQty: 2,
    toStoreroomQty: 2,
    toSupplierQty: 2,
  },
  {
    idx: 12,
    id: "BMM12347",
    location: "BF 1",
    status: "NEW",
    description: "bbbbbb bbbbbb bb, A109K-4000H dfdsfdsfdssd ddddd ddd dd",
    reservedQty: "985",
    remainQty: "000",
    issueQty: 7,
    toBFQty: 7,
    toStoreroomQty: 7,
    toSupplierQty: 7,
  },
];

const WorkOrderBMSPIssue = ({ onClick }) => {
  const [mode, setMode] = useState("");
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const [result] = useState(data);
  const [selectedId, setSelectedId] = useState(null);
  return (
    <>
      <div className="work-order-bm-sp-issue">
        <FixedHeader
          left={
            <IconBase onClick={onClick}>
              <IconCommBack />
            </IconBase>
          }
          center={
            <div className="equipment-title">
              <HeaderTitle text="SP Issue" />
            </div>
          }
        />
        <BaseMain
          className="work-order-bm-sp-issue-main"
          style={{
            height: isKeyboardFocus
              ? `calc(100vh - 56px)`
              : `calc(100vh - (56px + 87px))`,
            marginTop: `56px`,
          }}
        >
          <div className="sticky-header">
            <h1 className="detail-wo">
              <span className="wo-text">123675415</span>
            </h1>
          </div>
          <ul className="detail-items">
            {result.map((item) => (
              <li
                key={item.id}
                className="detail-item"
                onClick={() => {
                  setSelectedId(item.id);
                  setMode("SP_ISSUE_DETAIL");
                }}
              >
                <WorkOrderBMSPIssueCard
                  {...item}
                  onFocus={() => setIsKeyboardFocus(true)}
                  onBlur={() => setIsKeyboardFocus(false)}
                />
              </li>
            ))}
          </ul>
        </BaseMain>
        {!isKeyboardFocus && (
          <FixedFooter className="footer-white">
            <div className={cx("primary-button", "active")}>Save</div>
          </FixedFooter>
        )}
      </div>
      <ModalFullPageAni
        modeState={mode}
        pageName="SP_ISSUE_DETAIL"
        motion="vertical"
      >
        <WorkOrderBMSPIssueDetail
          selectedId={selectedId}
          onBackClick={() => setMode("")}
        />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrderBMSPIssue;
