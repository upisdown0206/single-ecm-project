import React, { useState } from "react";

import { useSnackbar } from "notistack";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";

import LabelDiv from "components/workOrder/LabelDiv";
import AttachedFileTextBox from "components/workOrder/AttachedFileTextBox";
import LabelInputCircleRadio from "components/workOrder/LabelInputCircleRadio";
import WorkOrderOPResultRegister from "components/workOrder/WorkOrderOPResultRegister";

import { IconCommBack, IconEdit } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./WorkOrderOPDetail.scss";

const WorkOrderOPDetail = ({ opIndex, onClose }) => {
  const [mode, setMode] = useState("");

  const [attachedFiles] = useState([
    {
      id: "1",
      filename:
        "attached file 1 attached file 1 attached file 1 attached file 1",
    },
    {
      id: "2",
      filename: "attached file 2",
    },
  ]);

  const [dangerGrade] = useState([
    {
      id: "d1",
      value: "1",
      checked: false,
    },
    {
      id: "d2",
      value: "2",
      checked: false,
    },
    {
      id: "d3",
      value: "3",
      checked: true,
    },
    {
      id: "d4",
      value: "4",
      checked: false,
    },
    {
      id: "d5",
      value: "5",
      checked: false,
    },
  ]);

  const [dangerGradeId, setDangerGradeId] = useState("d1");

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = () => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar("점검결과를 저장하였습니다", {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 80 }}
          onClick={() => closeSnackbar(key)}
        >
          <span>{message}</span>
        </div>
      ),
    });
  };
  const onSave = () => {
    setMode("");
    onShowMessage();
  };

  return (
    <>
      <div className="work-order-op-detail">
        <FixedHeader
          left={
            <IconBase onClick={onClose}>
              <IconCommBack />
            </IconBase>
          }
          center={
            <div className="inner-op">
              <span className="op-index">{opIndex}</span>
              <div className="op-flex">
                <IconEdit />
                <HeaderTitle text="Detail" style={{ marginLeft: "4px" }} />
              </div>
            </div>
          }
        />
        <main
          className="work-order-op-detail-main"
          style={{
            height: `calc(100vh - 56px - 87px)`,
            marginTop: `calc(56px)`,
          }}
        >
          <div className="detail-view">
            <div className="form-title" style={{ marginBottom: "34px" }}>
              <div className="title-text">S/P Requirements</div>
              <div
                className="title-button"
                onClick={() => setMode("SP_REQUIREMENT")}
              >
                <span>SP등록하기</span>
                <IconBase className="rotate180">
                  <IconCommBack width="24px" height="24px" fill="#8996A3" />
                </IconBase>
              </div>
            </div>
            <div className="form-title">
              <div className="title-text import">점검결과입력</div>
              <div
                className="title-button"
                onClick={() => setMode("INSERT_RESULT")}
              >
                <span>결과입력하기</span>
                <IconBase className="rotate180">
                  <IconCommBack width="24px" height="24px" fill="#8996A3" />
                </IconBase>
              </div>
            </div>
            {/* <LabelInputCircleRadio
              labelText="위험등급"
              items={dangerGrade}
              radioGroupName="dangerGrade"
              selectedId={dangerGradeId}
              setSelectedId={setDangerGradeId}
            /> */}
          </div>
          <div className="detail-view" style={{ marginTop: "8px" }}>
            <LabelDiv
              label="OP No"
              value="OP00001038"
              style={{ marginBottom: "24px" }}
            />
            <LabelDiv
              label="OP Description"
              value="L Battery Module Assembly #1"
              style={{ marginBottom: "24px" }}
            />
            <div className="view-flex" style={{ marginBottom: "24px" }}>
              <LabelDiv label="Result" value="OK" style={{ width: "100%" }} />
              <LabelDiv
                label="Man"
                value="-"
                style={{ width: "100%", marginLeft: "16px" }}
              />
            </div>
            <div className="view-flex" style={{ marginBottom: "24px" }}>
              <LabelDiv
                label="Work Hours"
                value="1:00"
                style={{ width: "100%" }}
              />
              <LabelDiv
                label="Work Schedule"
                value="Y"
                style={{ width: "100%", marginLeft: "16px" }}
              />
            </div>
            <LabelDiv
              label="Owning Department"
              value="CWA - 전국 보수 1반 - pg"
              style={{ marginBottom: "24px" }}
            />
            <LabelDiv
              label="Equipment ID"
              value="P1NBMAM05"
              style={{ marginBottom: "24px" }}
            />
            <LabelDiv
              label="Equipment Description"
              value="L Bettery Module Assembloy #1"
              style={{ marginBottom: "24px" }}
            />
            <LabelDiv
              label="Hazard Grade"
              value="A"
              style={{ marginBottom: "24px" }}
            />
            {/* <LabelDiv
              label="Scheduled Date"
              value={
                <div className="scheduled-date">
                  <span className="from">2020.03.02 15:30:00</span>
                  <div className="seperator" />
                  <span className="to">2020.03.02 17:00:00</span>
                </div>
              }
              style={{ marginBottom: "24px" }}
            >
              <div className="ab-top-right">
                <span className="rectangle-box">90 Min</span>
              </div>
            </LabelDiv> */}
            <LabelDiv
              label="Attached File"
              value={
                <div className="flex-col">
                  {attachedFiles.map((item) => (
                    <AttachedFileTextBox key={item.id} text={item.filename} />
                  ))}
                </div>
              }
              isShowBorderBottom={false}
            />
          </div>
        </main>
        <FixedFooter className="footer-white">
          <div className="save-button" onClick={onClose}>
            저장
          </div>
        </FixedFooter>
      </div>

      <ModalFullPageAni
        modeState={mode}
        pageName="INSERT_RESULT"
        motion="vertical"
      >
        <WorkOrderOPResultRegister
          onClose={() => setMode("")}
          onSave={onSave}
        />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrderOPDetail;
