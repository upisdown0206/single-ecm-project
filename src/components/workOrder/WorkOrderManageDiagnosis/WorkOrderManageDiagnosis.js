import React, { useState } from "react";
import cx from "classnames";

import FixedFooter from "components/base/FixedFooter";
import DimedModal from "components/base/DimedModal";

import { IconTabCheck, IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import PictureBox from "components/common/PictureBox";
import CircleText from "components/common/CircleText";

import WorkOrderStepButtonTypeA from "components/workOrder/WorkOrderStepButtonTypeA";
import WorkOrderToDoItemTypeE from "components/workOrder/WorkOrderToDoItemTypeE";
import WorkOrderToDoItemTypeC2 from "components/workOrder/WorkOrderToDoItemTypeC2";
import WorkOrderBMDiagnosisCheckTypeA from "components/workOrder/WorkOrderBMDiagnosisCheckTypeA";
import WorkOrderToDoItemTypeD from "components/workOrder/WorkOrderToDoItemTypeD";
import WorkOrderBMDiagnosisCauseTypeB from "components/workOrder/WorkOrderBMDiagnosisCauseTypeB";
import WorkOrderStepButtonTypeB from "components/workOrder/WorkOrderStepButtonTypeB";

import "./WorkOrderManageDiagnosis.scss";

const WorkOrderManageDiagnosis = ({
  onClickNextSP,
  onClickNextSPIssue,
  isSave,
  onSaveClick,
  onShowMessage,
}) => {
  const [mode, setMode] = useState("init");
  const [activeTabinTab, setActiveTabinTab] = useState("diagnosis/failure");
  const handleTabInTab = (tabinTabIndex) => {
    setActiveTabinTab(tabinTabIndex);
  };

  const [showUndefined, setShowUndefined] = useState(false);
  /* Cause and Resolution Picture */
  const [causeAndResolutionPictures, setCauseAndResolutionPictures] = useState([
    {
      id: "1",
      url:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAmYAAAFgCAYAAAARnbUKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABdFSURBVHgB7d1plKV1ndjx3711a22ql2robrobaJZmFxqQBkHASCKgYgzMwUlcJ0YdjSZkhmNGzJw5mcmczExM5shBx8iMMickbkGMjcqMCyooO4jQLA3S0HtXb9VVXeutW5XnqaaUcYRhAOH/PM/nc849zRveVNe999u///LUpjMBAMArrh4AACShMTQ0FAAAvPJMzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABLRCKC6RoYiHrgt4sm1Eesfjtj8eMTAjojp6UhJR98R0Zm9UjN9QG9MLVkWrfx16BExtWhpTM1fEAAvVG1wcDCtT2Dg12vvrojbvhlx93cjHrk7iiDVMPtVWocdFc0TTo7W0SdkkdYXAP8Ywgyq4uG7Im78XMRDd0Q0J6JIihRmz9Q87uSYPGV1TGaRBvB8CDMouzzIrr86m47dE0VV1DCb1Vq+IppnvS6ax68KgOcizKCs+jdGXPvHET+9NYqu6GE2q7nq9Jh43UWWOIFnJcygjG76X/unZKPDUQZlCbPcdM+caJ5xToyfd2EA/DKnMqFM9u2J+MyVET+5JUhTbWQ4Om6+KeqbN8T4my6LqXnzA2CWe8ygLHZujviDt4uygmiseyi6P39V1Ad2BcAsYQZlsH5txJW/EbF9Q1Ac9YHd0X3t1dG2cX0A5IQZFN1j90X88b/ef1kshVMf2BM9n7sqi7MnA0CYQZE99UjEH747Yqwcm/wra3o6uq/7TLRteCKAahNmUFT5nrI/fX/2pT4VFF9tfCy6v/CXM8ubQHUJMyiifNnyv/xWxKAv8TKpjY5E97Wfmok0oJqEGRTRlz+ZTcy2BOWTn9LsXPPlAKpJmEHR/OCGiO98MSiv9gfvjfYHivsILeCFE2ZQJHu2R9zwF0H5dd50Q9TGRgOoFmEGRXL9py1hVkRteF90fntNANUizKAo8lOY378+qI72+26Pev+2AKpDmEFRXP+poGKmpqLze98IoDqEGRRBPi275etB9TQeeSDqQ4MBVIMwgyJY81dBdTXu+GEA1SDMoAjuvzWoro57b4tacyKA8hNmkLr7fuAkZsXVRoY9RxMqQphB6m7/VkDjwfsCKD9hBqlb5wuZLMzWrQ2g/IQZpKx/Y8SOzQH5hbP5czSBchNmkLInTEn4hbYnfxZAuQkzSNmGRwNm1fu3BlBuwgxStu2pgFm1kX0BlJswg5Ttck0Gv9C2eWMA5SbMIGV7dwf8nEtmofSEGaRsbDhgltv/ofyEGaRsfDRgVm18PIByE2YAhTEdQLkJM0hZ15yAWdPtHQGUmzCDlHV2B8ya7uwKoNyEGaSswxcxzzDngADKTZhByo44IWDW1LwFAZSbMIOU9fYFzJrqOyiAchNmkLLlRwXMai1ZGkC5CTNI2XGvDpg1dfAhAZSbMIOULcq+iHvtKyKLsgV9MbXQUiaUnTCD1J1yXkBr+YoAyk+YQerOvChg8lWnBVB+wgxSd/QpEXPmBtWVX5MxufL4AMpPmEHqunoiXndpUF2to46JqNUCKD9hBkVw/m8G1TVxzhsCqAZhBkWwaFnESWcH1dM8+fSYmu+iYagKYQZFcdnl2XKWt2zVTPwThz+gSnzKQ1GsOC7itRcH1dFcdYZpGVSMMIMieefvOaFZEflJzInXXRhAtQgzKJKe3oi3XR6U38S5b8imZZ76AFUjzKBoXn9ZxKvPD8qruer0aJ72mgCqR5hBEb3vjyIOXBqUT76nbPzCSwKoJmEGRZTvM/v45yO65gTlMd3dE6Pv+XBMd3UHUE3CDIrqoGURv3t1RGdXUA4j7/qQU5hQccIMiuy40yMuvyootun2jhh9x2/H1MHLA6g2YQZF96qzIj76PyPmHxgUT75sOfrOD8bkUccGgDCDMsgf1/Txax0IKJjp+Qti5ANXROvQwwMgJ8ygLA5esf9AQL68SfImjzgmRt57eUwtWBgAs2qDg4PTAZTLVz8d8c1rI8ZGogw6+o6IzuxVBtONRoyf/+Zonnle9glcC4BnEmZQVjs2R3zxzyPuuCmKrixh1lpxVIy98dKYWnRwAPwqwgzK7kdrIm7Mljg3rouiKnqYTR2yIsZe/6ZoHb4yAJ6LMIOquP/WiL+9bv+fBVPUMMtDbPy8C2YmZQDPhzCDqunPljjv+W7Ej78RsX5tFEGRwqy17LBonrAqWsefFFPzbewH/nGEGVRZvg9tw6MRD92ZRdqDEdueihjcE6lJNcyme+fOxFfr4OUzy5WTK4/3OCXgRRFmQDRHazGwoR6Dm+sxvnlvjDw1FGObskCbbEUKVpzbFoed24iXw5Nj9XhouB7rsz93NGuxfaIWw61nnJ7s6Ix5vd2xYm4jDlxwQBzdMxUnzpmKnrqPUuDFe3k+6YCkTGXBsetn9dhyb3v0r22L4Z3PvNIwf/bm4v3/2RZJOHDBeLRWTMSvw6bxWnxjVyN+tLcRj4xmcTr5dIS1P/3qeZb/cfDp19OO7p6KVb2teP38Vpw7bzLqbsIAXgBhBhWy67G22HRXe/ZqRHOkuuUwlMXXDTsb8Td7GnHP0EtTn+uyqMtfX+5vj4M6puO1c1vxjiUTcUI2UQN4voQZVED/Q23x6Dc7Z8KsygayIPvctvb4YhZPP5+M/RrsmNgffvlrZTZJ+/CyibigbzIA/iHCDEps56Nt8fDXsyB7vNpBlk/Irt3eHtdsaY+J6Zd3UvhYNkX79493xeFdU/GnR47FSXNM0IBnJ8yghPJlyofXdMYT32uPqrtjsC2uXN8Vm8df2aXb/DDBZWt74t1LmvHebIlzUYfDAsDfJ8ygZHaua4u7/6o7xgaqvfs8n5L9j00d8YX+tOL0r7Ol1O/sacR/PWIsVvemceoVSIcwgxLJly0f/UZHVF1+0vK9j3bHU2P1SFE+vXvXw90ze8/yF8AsYQYlMJVNhx78v9nS5c2WLu8aaouPPNY1s9E/dVdv7ohdzVr8p8PGo831GkAmzX9OAs/b9HQWI3/ZJcoyNw804rcfLUaUzcqXWj+4rvvvXmILVJYwgwLLJ2W3XdUdW+8z/M43+f+7xzpjeKp4gfPDvW3xHx7vivFpcQZVJ8ygwH7yvzuj/yFRtnZ4/5UUzQKHTR5nv/9EZwDVJsygoB5Z0xkbfmz5Mt9I/5HHuwu1fPlsvr6rMbPvDKguYQYFtO3+Rjxyoy/wXH76cst4eZYA8zC7dW+1LwSGKhNmUDAju+rx0y91BRGf3NQRTyZ6JcaLke832zrh4xmqyDsfCuahGzqzOLNJ/GejtfiLLeWcGg61anGl/WZQScIMCmTTne2x6S6b/XMfWNcTZXbbYFt8b8DfNVSNMIMCeehrpii5r+5sn7ndv+z+ZENHjBTw+g/ghRNmUBBP3tJuCTMzloXKpypycnHDWD2+0m9qBlUizKAg1n3LtCz3rd2NmSsyquKarR0xHUBVCDMogI23mZbN+nTF7vna2azFjbtMzaAqhBkUwEYb/mfcMdQWGys0LZv1lR0uEoaqEGaQuOEd9ehfK8xyX6tooNw52BZbx31cQxV4p0PittwjynKt6Yibdlf3Rvwbdvo9gCoQZpC47aZlM+7d1xajFb464s4hj2mCKhBmkLBWM2LnOl/Iue/tqXag3pOF2Zg7zaD0hBkkbM8TomxWPjGrsma2lPvTYR/ZUHbe5ZCw3cLs5x4bMS16UJhB6XmXQ8LcXbbf1om6RxNl1o8KdSg7YQYJy6/KICp5d9mvsnUigJLzqQ8JG93tLZrb1fRzyK0fMzGDsvNpBwmbnDApym0wMZvR8tBMKD1hBglrjgaZ5lSQGWoFUHLCDBI2NWlSxC9MOAABpSfMIGFtDWtX/EJ7ze8DlJ0wg4S19wSZLp9UM3o9nQtKz8cdJKzmHTpjcYdJUa677ucAZedjHxLWu8Ru79ySDrv/c0sFKpSeMIOEdS3wRZw7sluY5Q7p8vsAZSfMIGEmZvv1NaZjroMQ2cRMoELZCTNI2PzDfBHPOqbHz+KUXqEOZSfMIGELDm9F3aRoxmkHiJJT/Qyg9IQZJKytPWLhUb6Mc2fNq/bPYfXcVjTcLwulJ8wgcQceLcxyq7JpUZX3mZ1b8TCFqhBmkLhDz5oMIjqyadGb+qobJxf2+T2AKhBmkLjuBVOmZk+7aGEzquiceZOxvNPhB6gCYQYFcNg51QySX7a6txVHVPBOszctFOZQFcIMCuCQ05vRNd/pzNwlB1ZrSW9Z53S89UBhDlUhzKAIahEr3zAeRLxryUTMq9AhgA8snQigOoQZFMSKcyaj92D7jPJDAFceWo1Yyadllx1kWgZVIsygINo6puPES03Ncm/JlvaOq8CTAH73EH/fUDXCDApk8asm48BjbATP71n9+GHljpaLF07GG12RAZUjzKBgTnvPWLT3OAjw6t5WvK+k+6/yJczLl9tbBlUkzKBguvum4vR/MxZEfPDgZpxawgd7f/zQ8SzO7CeEKhJmUECLTpiMY99s/1FP23T8tyPGYlF7eSaIH142Ea9fYAkTqkqYQUEde/FEHPIaJ/byZb/PHjMa80twhcalBzVnwgyoLmEGBXbKO8ZmDgRU3bE9U9nkbDy66sWNs/yxS/95hSiDqhNmUGD1RsTq943FkpPE2TnzJ+OqlWMxp614cZafvvzsMWPRqDnUAVUnzKDg2rKlvDM+OBqHn2dZ89x5rbgmC5xFHcUJnLcvbsafHTk+cwUIgDCDEqhl7+ST/9VYHHuxAwGnHtCKLxw3MrP3LHX/dtlE/P5h4yZlwM8JMyiRY988Eed+dCR6Flb7iz6Psr89aTh+a0maU8Ql2UTvuuNG4yM2+gO/RJhByfQd2YpzPjocR51f7S/9tmxt8D8eOh5XrxxLanr2zmzpcs2JIzMX5AL8strg4KAZOpTU3o31uOfz3TG4udj/BsvvbMuvB3mhBiZrcd329rh2W3vsa70yu7lOz0Isn5CtnivIgGcnzKACtv20EY+s6YyBDcUMtBcbZrO2jNfimq0dcdOeRuxpvjyBtjoLsg9lQXamIAOeB2EGFTLwVD2euLkj+h9uxNhAcc4BvlRhNiufmn07i7Mvbm/E/cNt8VKb25iOtyycjAv6JmcmZQDPlzCDCprO3vX9DzaySVpbNkVriz1PvvRx8lJ6qcPsmTZlU7Rb9jbiO1moPTxSj90vcJJ2XM9UnJZF2D9dMBmn9E5Fp5OWwAsgzICYzOIkn6bt216PTTvGo39rK7YPjse+qWZMxyv/EXH2Od1x9rnd8XJYl8XZtol6PDBSi53ZnxvG69F8xo+gkb362qdnDhQs6ZiKlV1TcUwWZb0leCQU8MoTZlBhg1PjcfPIU3Hv+La4f7w/1jcHYvvkcKTmigVnxBV9ZwZA2TUCqJRNk0PxzX0/izXDj8VdY1sCgHQIM6iIH41ujKsG7o5bsz9b0wblACkSZlByeYj9ye7b4u6xrQFA2oQZlNRjzd3xsR3fnwkzAIpBmEEJfXbvT7Ip2Y9iZGoyACgOYQYl0t8aiQ/3/038cGRDAFA8wgxKYuPkYFy65frY0BwMAIpJmEEJ3DO2Lf7l1q/N3EsGQHEJMyi4H2TLlu/ZviZG7ScDKDxhBgX2wHh//ObWGxJ4aBIAL4V6AIWU7yl72xZRBlAmwgwKKN9LdsmW62P31FgAUB7CDAroD3feGhudvgQoHWEGBfN/htbGdUMPBgDlI8ygQDZPDsWf77kzACgnYQYF8ondt1vCBCgxYQYFkQfZF4YeCgDKS5hBQfzZntsCgHITZlAA+Z1lXxl6JAAoN2EGBXDVnrsCgPITZpC4/Gb/748+FQCUnzCDxH17+InY2BwKAMpPmEHibti3LgCoBmEGibt7fGsAUA3CDBK2vjngQlmAChFmkLD7x/sDgOoQZpCwteM7AoDqEGaQsHwpE4DqEGaQsI0t+8sAqkSYQcJ2t0YDgOoQZpCwfa1mAFAdwgwSNjwtzACqRJhBwmoBQJUIM0hYb70jAKgOYQYJ6643goiDGnMCoAqEGSSsu2Zilju8fX4AVIEwg4St6lwURPTUTA6BahBmkLCFbd1BxGldSwKgCoQZJOzYjoVRdSs7FkTN+VSgIoQZJOzs7uVRdce2HxgAVSHMIGHLG3NjUaMnquzCOUcGQFUIM0jc+d0rospea2oIVIgwg8S99YBjoqpO7Voci91hBlSIMIPEre5eGn31rqiid/WeFABVIswgcd21Rrx97olRNfk5zLN7LGMC1SLMoADeO29VVM1lvcfHIY25AVAlwgwKYEljTlxUsdOJv9N3RgBUjTCDgriy76xor1XjLfu2bFp2mGkZUEHCDApiZUdfZfaaXWFaBlSUMIMC+Vg2NSv78zOvWHCmvWVAZQkzKJB59c74o4XnRVkdmgXZhxacGgBVJcygYC7pPSb++QFHR9nUoxZfWvovoqfWHgBVJcyggD5x0PlxaHu5lvv+YOFr4/D2+QFQZcIMCqi33hHXL700FpTkiQCXzz89PjDfEiaAMIOCyjfI//XBF8fcemcU2VuyZdnfW3hWACDMoNBWdy2Nryy9ZGZ/VhFd0HNEfHLRPwsA9hNmUHAndy6KNcsui+XtvVEkb80mZfnEL38WKAD7CTMogdO6lsRXl/5GHFKQAwG/s2B1fGbxRQHA3yXMoCTyO8C+lsXZGxN+pma+H+5Tiy6Ij/a9JgD4+2qDg4PTAZTKf999R3xu8P7Y1RqNVJzVtSw+ufgNbvUHeA7CDEpq4+RgfGL37fGloYfjlTS/3hUfW/iaePfckwKA5ybMoOS+O/JkfGbg3rhldGO8nPJly/fPWxXvm3/KzKOkAPiHCTOoiDvGNsc1Az+JG4cfj1+nxW1z4p1zT4z3Z0FW9DvWAF5uwgwqZsvkUHw7m6L9v6FH48dZrL0U8gB729zjZu4lO7tredRqxbxXDeCVJsygwrZPDscDE/1x++jmeHB8RzwxORAbm4PxXB8K+WOgFjV6YlXn4nh118Hxqs5FM/8NwIsnzAAAEuEeMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBE/H8etM8Bvd+38QAAAABJRU5ErkJggg==",
    },
    {
      id: "2",
      url:
        "https://media.vlpt.us/images/jacoblee19/post/75c33f16-6dc3-4653-b46e-b2ecad61e496/github.jpg",
    },
  ]);
  /* Failure and Check Picture */
  const [pictures, setPictures] = useState([
    {
      id: "1",
      url:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAmYAAAFgCAYAAAARnbUKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABdFSURBVHgB7d1plKV1ndjx3711a22ql2robrobaJZmFxqQBkHASCKgYgzMwUlcJ0YdjSZkhmNGzJw5mcmczExM5shBx8iMMickbkGMjcqMCyooO4jQLA3S0HtXb9VVXeutW5XnqaaUcYRhAOH/PM/nc849zRveVNe999u///LUpjMBAMArrh4AACShMTQ0FAAAvPJMzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABIhzAAAEiHMAAASIcwAABLRCKC6RoYiHrgt4sm1Eesfjtj8eMTAjojp6UhJR98R0Zm9UjN9QG9MLVkWrfx16BExtWhpTM1fEAAvVG1wcDCtT2Dg12vvrojbvhlx93cjHrk7iiDVMPtVWocdFc0TTo7W0SdkkdYXAP8Ywgyq4uG7Im78XMRDd0Q0J6JIihRmz9Q87uSYPGV1TGaRBvB8CDMouzzIrr86m47dE0VV1DCb1Vq+IppnvS6ax68KgOcizKCs+jdGXPvHET+9NYqu6GE2q7nq9Jh43UWWOIFnJcygjG76X/unZKPDUQZlCbPcdM+caJ5xToyfd2EA/DKnMqFM9u2J+MyVET+5JUhTbWQ4Om6+KeqbN8T4my6LqXnzA2CWe8ygLHZujviDt4uygmiseyi6P39V1Ad2BcAsYQZlsH5txJW/EbF9Q1Ac9YHd0X3t1dG2cX0A5IQZFN1j90X88b/ef1kshVMf2BM9n7sqi7MnA0CYQZE99UjEH747Yqwcm/wra3o6uq/7TLRteCKAahNmUFT5nrI/fX/2pT4VFF9tfCy6v/CXM8ubQHUJMyiifNnyv/xWxKAv8TKpjY5E97Wfmok0oJqEGRTRlz+ZTcy2BOWTn9LsXPPlAKpJmEHR/OCGiO98MSiv9gfvjfYHivsILeCFE2ZQJHu2R9zwF0H5dd50Q9TGRgOoFmEGRXL9py1hVkRteF90fntNANUizKAo8lOY378+qI72+26Pev+2AKpDmEFRXP+poGKmpqLze98IoDqEGRRBPi275etB9TQeeSDqQ4MBVIMwgyJY81dBdTXu+GEA1SDMoAjuvzWoro57b4tacyKA8hNmkLr7fuAkZsXVRoY9RxMqQphB6m7/VkDjwfsCKD9hBqlb5wuZLMzWrQ2g/IQZpKx/Y8SOzQH5hbP5czSBchNmkLInTEn4hbYnfxZAuQkzSNmGRwNm1fu3BlBuwgxStu2pgFm1kX0BlJswg5Ttck0Gv9C2eWMA5SbMIGV7dwf8nEtmofSEGaRsbDhgltv/ofyEGaRsfDRgVm18PIByE2YAhTEdQLkJM0hZ15yAWdPtHQGUmzCDlHV2B8ya7uwKoNyEGaSswxcxzzDngADKTZhByo44IWDW1LwFAZSbMIOU9fYFzJrqOyiAchNmkLLlRwXMai1ZGkC5CTNI2XGvDpg1dfAhAZSbMIOULcq+iHvtKyKLsgV9MbXQUiaUnTCD1J1yXkBr+YoAyk+YQerOvChg8lWnBVB+wgxSd/QpEXPmBtWVX5MxufL4AMpPmEHqunoiXndpUF2to46JqNUCKD9hBkVw/m8G1TVxzhsCqAZhBkWwaFnESWcH1dM8+fSYmu+iYagKYQZFcdnl2XKWt2zVTPwThz+gSnzKQ1GsOC7itRcH1dFcdYZpGVSMMIMieefvOaFZEflJzInXXRhAtQgzKJKe3oi3XR6U38S5b8imZZ76AFUjzKBoXn9ZxKvPD8qruer0aJ72mgCqR5hBEb3vjyIOXBqUT76nbPzCSwKoJmEGRZTvM/v45yO65gTlMd3dE6Pv+XBMd3UHUE3CDIrqoGURv3t1RGdXUA4j7/qQU5hQccIMiuy40yMuvyootun2jhh9x2/H1MHLA6g2YQZF96qzIj76PyPmHxgUT75sOfrOD8bkUccGgDCDMsgf1/Txax0IKJjp+Qti5ANXROvQwwMgJ8ygLA5esf9AQL68SfImjzgmRt57eUwtWBgAs2qDg4PTAZTLVz8d8c1rI8ZGogw6+o6IzuxVBtONRoyf/+Zonnle9glcC4BnEmZQVjs2R3zxzyPuuCmKrixh1lpxVIy98dKYWnRwAPwqwgzK7kdrIm7Mljg3rouiKnqYTR2yIsZe/6ZoHb4yAJ6LMIOquP/WiL+9bv+fBVPUMMtDbPy8C2YmZQDPhzCDqunPljjv+W7Ej78RsX5tFEGRwqy17LBonrAqWsefFFPzbewH/nGEGVRZvg9tw6MRD92ZRdqDEdueihjcE6lJNcyme+fOxFfr4OUzy5WTK4/3OCXgRRFmQDRHazGwoR6Dm+sxvnlvjDw1FGObskCbbEUKVpzbFoed24iXw5Nj9XhouB7rsz93NGuxfaIWw61nnJ7s6Ix5vd2xYm4jDlxwQBzdMxUnzpmKnrqPUuDFe3k+6YCkTGXBsetn9dhyb3v0r22L4Z3PvNIwf/bm4v3/2RZJOHDBeLRWTMSvw6bxWnxjVyN+tLcRj4xmcTr5dIS1P/3qeZb/cfDp19OO7p6KVb2teP38Vpw7bzLqbsIAXgBhBhWy67G22HRXe/ZqRHOkuuUwlMXXDTsb8Td7GnHP0EtTn+uyqMtfX+5vj4M6puO1c1vxjiUTcUI2UQN4voQZVED/Q23x6Dc7Z8KsygayIPvctvb4YhZPP5+M/RrsmNgffvlrZTZJ+/CyibigbzIA/iHCDEps56Nt8fDXsyB7vNpBlk/Irt3eHtdsaY+J6Zd3UvhYNkX79493xeFdU/GnR47FSXNM0IBnJ8yghPJlyofXdMYT32uPqrtjsC2uXN8Vm8df2aXb/DDBZWt74t1LmvHebIlzUYfDAsDfJ8ygZHaua4u7/6o7xgaqvfs8n5L9j00d8YX+tOL0r7Ol1O/sacR/PWIsVvemceoVSIcwgxLJly0f/UZHVF1+0vK9j3bHU2P1SFE+vXvXw90ze8/yF8AsYQYlMJVNhx78v9nS5c2WLu8aaouPPNY1s9E/dVdv7ohdzVr8p8PGo831GkAmzX9OAs/b9HQWI3/ZJcoyNw804rcfLUaUzcqXWj+4rvvvXmILVJYwgwLLJ2W3XdUdW+8z/M43+f+7xzpjeKp4gfPDvW3xHx7vivFpcQZVJ8ygwH7yvzuj/yFRtnZ4/5UUzQKHTR5nv/9EZwDVJsygoB5Z0xkbfmz5Mt9I/5HHuwu1fPlsvr6rMbPvDKguYQYFtO3+Rjxyoy/wXH76cst4eZYA8zC7dW+1LwSGKhNmUDAju+rx0y91BRGf3NQRTyZ6JcaLke832zrh4xmqyDsfCuahGzqzOLNJ/GejtfiLLeWcGg61anGl/WZQScIMCmTTne2x6S6b/XMfWNcTZXbbYFt8b8DfNVSNMIMCeehrpii5r+5sn7ndv+z+ZENHjBTw+g/ghRNmUBBP3tJuCTMzloXKpypycnHDWD2+0m9qBlUizKAg1n3LtCz3rd2NmSsyquKarR0xHUBVCDMogI23mZbN+nTF7vna2azFjbtMzaAqhBkUwEYb/mfcMdQWGys0LZv1lR0uEoaqEGaQuOEd9ehfK8xyX6tooNw52BZbx31cQxV4p0PittwjynKt6Yibdlf3Rvwbdvo9gCoQZpC47aZlM+7d1xajFb464s4hj2mCKhBmkLBWM2LnOl/Iue/tqXag3pOF2Zg7zaD0hBkkbM8TomxWPjGrsma2lPvTYR/ZUHbe5ZCw3cLs5x4bMS16UJhB6XmXQ8LcXbbf1om6RxNl1o8KdSg7YQYJy6/KICp5d9mvsnUigJLzqQ8JG93tLZrb1fRzyK0fMzGDsvNpBwmbnDApym0wMZvR8tBMKD1hBglrjgaZ5lSQGWoFUHLCDBI2NWlSxC9MOAABpSfMIGFtDWtX/EJ7ze8DlJ0wg4S19wSZLp9UM3o9nQtKz8cdJKzmHTpjcYdJUa677ucAZedjHxLWu8Ru79ySDrv/c0sFKpSeMIOEdS3wRZw7sluY5Q7p8vsAZSfMIGEmZvv1NaZjroMQ2cRMoELZCTNI2PzDfBHPOqbHz+KUXqEOZSfMIGELDm9F3aRoxmkHiJJT/Qyg9IQZJKytPWLhUb6Mc2fNq/bPYfXcVjTcLwulJ8wgcQceLcxyq7JpUZX3mZ1b8TCFqhBmkLhDz5oMIjqyadGb+qobJxf2+T2AKhBmkLjuBVOmZk+7aGEzquiceZOxvNPhB6gCYQYFcNg51QySX7a6txVHVPBOszctFOZQFcIMCuCQ05vRNd/pzNwlB1ZrSW9Z53S89UBhDlUhzKAIahEr3zAeRLxryUTMq9AhgA8snQigOoQZFMSKcyaj92D7jPJDAFceWo1Yyadllx1kWgZVIsygINo6puPES03Ncm/JlvaOq8CTAH73EH/fUDXCDApk8asm48BjbATP71n9+GHljpaLF07GG12RAZUjzKBgTnvPWLT3OAjw6t5WvK+k+6/yJczLl9tbBlUkzKBguvum4vR/MxZEfPDgZpxawgd7f/zQ8SzO7CeEKhJmUECLTpiMY99s/1FP23T8tyPGYlF7eSaIH142Ea9fYAkTqkqYQUEde/FEHPIaJ/byZb/PHjMa80twhcalBzVnwgyoLmEGBXbKO8ZmDgRU3bE9U9nkbDy66sWNs/yxS/95hSiDqhNmUGD1RsTq943FkpPE2TnzJ+OqlWMxp614cZafvvzsMWPRqDnUAVUnzKDg2rKlvDM+OBqHn2dZ89x5rbgmC5xFHcUJnLcvbsafHTk+cwUIgDCDEqhl7+ST/9VYHHuxAwGnHtCKLxw3MrP3LHX/dtlE/P5h4yZlwM8JMyiRY988Eed+dCR6Flb7iz6Psr89aTh+a0maU8Ql2UTvuuNG4yM2+gO/RJhByfQd2YpzPjocR51f7S/9tmxt8D8eOh5XrxxLanr2zmzpcs2JIzMX5AL8strg4KAZOpTU3o31uOfz3TG4udj/BsvvbMuvB3mhBiZrcd329rh2W3vsa70yu7lOz0Isn5CtnivIgGcnzKACtv20EY+s6YyBDcUMtBcbZrO2jNfimq0dcdOeRuxpvjyBtjoLsg9lQXamIAOeB2EGFTLwVD2euLkj+h9uxNhAcc4BvlRhNiufmn07i7Mvbm/E/cNt8VKb25iOtyycjAv6JmcmZQDPlzCDCprO3vX9DzaySVpbNkVriz1PvvRx8lJ6qcPsmTZlU7Rb9jbiO1moPTxSj90vcJJ2XM9UnJZF2D9dMBmn9E5Fp5OWwAsgzICYzOIkn6bt216PTTvGo39rK7YPjse+qWZMxyv/EXH2Od1x9rnd8XJYl8XZtol6PDBSi53ZnxvG69F8xo+gkb362qdnDhQs6ZiKlV1TcUwWZb0leCQU8MoTZlBhg1PjcfPIU3Hv+La4f7w/1jcHYvvkcKTmigVnxBV9ZwZA2TUCqJRNk0PxzX0/izXDj8VdY1sCgHQIM6iIH41ujKsG7o5bsz9b0wblACkSZlByeYj9ye7b4u6xrQFA2oQZlNRjzd3xsR3fnwkzAIpBmEEJfXbvT7Ip2Y9iZGoyACgOYQYl0t8aiQ/3/038cGRDAFA8wgxKYuPkYFy65frY0BwMAIpJmEEJ3DO2Lf7l1q/N3EsGQHEJMyi4H2TLlu/ZviZG7ScDKDxhBgX2wHh//ObWGxJ4aBIAL4V6AIWU7yl72xZRBlAmwgwKKN9LdsmW62P31FgAUB7CDAroD3feGhudvgQoHWEGBfN/htbGdUMPBgDlI8ygQDZPDsWf77kzACgnYQYF8ondt1vCBCgxYQYFkQfZF4YeCgDKS5hBQfzZntsCgHITZlAA+Z1lXxl6JAAoN2EGBXDVnrsCgPITZpC4/Gb/748+FQCUnzCDxH17+InY2BwKAMpPmEHibti3LgCoBmEGibt7fGsAUA3CDBK2vjngQlmAChFmkLD7x/sDgOoQZpCwteM7AoDqEGaQsHwpE4DqEGaQsI0t+8sAqkSYQcJ2t0YDgOoQZpCwfa1mAFAdwgwSNjwtzACqRJhBwmoBQJUIM0hYb70jAKgOYQYJ6643goiDGnMCoAqEGSSsu2Zilju8fX4AVIEwg4St6lwURPTUTA6BahBmkLCFbd1BxGldSwKgCoQZJOzYjoVRdSs7FkTN+VSgIoQZJOzs7uVRdce2HxgAVSHMIGHLG3NjUaMnquzCOUcGQFUIM0jc+d0rospea2oIVIgwg8S99YBjoqpO7Voci91hBlSIMIPEre5eGn31rqiid/WeFABVIswgcd21Rrx97olRNfk5zLN7LGMC1SLMoADeO29VVM1lvcfHIY25AVAlwgwKYEljTlxUsdOJv9N3RgBUjTCDgriy76xor1XjLfu2bFp2mGkZUEHCDApiZUdfZfaaXWFaBlSUMIMC+Vg2NSv78zOvWHCmvWVAZQkzKJB59c74o4XnRVkdmgXZhxacGgBVJcygYC7pPSb++QFHR9nUoxZfWvovoqfWHgBVJcyggD5x0PlxaHu5lvv+YOFr4/D2+QFQZcIMCqi33hHXL700FpTkiQCXzz89PjDfEiaAMIOCyjfI//XBF8fcemcU2VuyZdnfW3hWACDMoNBWdy2Nryy9ZGZ/VhFd0HNEfHLRPwsA9hNmUHAndy6KNcsui+XtvVEkb80mZfnEL38WKAD7CTMogdO6lsRXl/5GHFKQAwG/s2B1fGbxRQHA3yXMoCTyO8C+lsXZGxN+pma+H+5Tiy6Ij/a9JgD4+2qDg4PTAZTKf999R3xu8P7Y1RqNVJzVtSw+ufgNbvUHeA7CDEpq4+RgfGL37fGloYfjlTS/3hUfW/iaePfckwKA5ybMoOS+O/JkfGbg3rhldGO8nPJly/fPWxXvm3/KzKOkAPiHCTOoiDvGNsc1Az+JG4cfj1+nxW1z4p1zT4z3Z0FW9DvWAF5uwgwqZsvkUHw7m6L9v6FH48dZrL0U8gB729zjZu4lO7tredRqxbxXDeCVJsygwrZPDscDE/1x++jmeHB8RzwxORAbm4PxXB8K+WOgFjV6YlXn4nh118Hxqs5FM/8NwIsnzAAAEuEeMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBECDMAgEQIMwCARAgzAIBE/H8etM8Bvd+38QAAAABJRU5ErkJggg==",
    },
    {
      id: "2",
      url:
        "https://media.vlpt.us/images/jacoblee19/post/75c33f16-6dc3-4653-b46e-b2ecad61e496/github.jpg",
    },
    {
      id: "3",
      url:
        "https://media.vlpt.us/images/namezin/profile/0f06c7d4-957c-4393-ae7f-52b7a60ce00c/programmer-seo.jpg?w=240",
    },
    {
      id: "4",
      url:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAFeCAYAAABn3sxXAAAXBUlEQVR4Xu3aMa/O+RYF4P+IikgEUehUouIriEJBlKhEofQNfAeJjqiVCqWKRDQKEbVEImiERknMZG5xcifu3LPOGa9593qf213Z7//s/axfsXJzfzt48ODvi/8QIECAAAECBAgQKBX4TeEtTdZZBAgQIECAAAEC/xFQeD0EAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIECBAgAABAgSqBRTe6ngdR4AAAQIECBAgoPB6AwQIECBAgAABAtUCCm91vI4jQIAAAQIECBBQeL0BAgQIECBAgACBagGFtzpexxEgQIAAAQIECCi83gABAgQIECBAgEC1gMJbHa/jCBAgQIAAAQIEFF5vgAABAgQIECBAoFpA4a2O13EECBAgQIAAAQIKrzdAgAABAgQIECBQLaDwVsfrOAIECBAgQIAAAYXXGyBAgAABAgQIEKgWUHir43UcAQIECBAgQICAwusNECBAgAABAgQIVAsovNXxOo4AAQIECBAgQEDh9QYIENhogWPHji3Hjx9fmcGzZ8/+8u0TJ04sR44c2fq3N2/eLB8+fPjh7x86dGg5efLk1r9/+fJlefXq1cr29GECBAg0Cyi8zem6jQCBbQUeP368nD59etu53Q5cuHBh+e/S++7du2X//v1bn3vx4sVy9uzZHz5/586d5fLly1v//v379+Xw4cO7XcPvCBAgsNECCu9Gx+94AgSePHmynDp1amUQFy9eXJ4+fbr1/ffv3y/79u3b+u8vX75czpw588Pfv3v37nLp0iWFd2XJ+DABApskoPBuUtpuJUDgBwGF16MgQIBAv4DC25+xCwkQ+D8Ct27dWs6fP7+t0dGjR/8y8+3bt+Xz58/b/u7Pb79+/dr/wrutlAECBAisTkDhXZ2tLxMgUCTw6dOnZc+ePVsXPXr0aLly5cqOL/R/adgxmR8QIEDgHwsovP+Y0AcIENgEAYV3E1J2IwECrQIKb2uy7iJA4KcKKLw/ldPHCBAg8EsFFN5fyu2PESAwVUDhnZqcvQkQILAsCq9XQIAAgUBA4Q2QjBAgQGBNBRTeNQ3GWgQIrJeAwrteediGAAECOxFQeHeiZZYAgY0VUHg3NnqHEyBQIKDwFoToBAIEVi+g8K7e2F8gQIDAqgQU3lXJ+i4BAlUCCm9VnI4hQGDDBBTeDQvcuQQI7E5A4d2dm18RIEBgHQQU3nVIwQ4ECKy9gMK79hFZkAABAn8roPB6HAQIEAgEFN4AyQgBAgTWVEDhXdNgrEWAwHoJKLzrlYdtCBAgsBMBhXcnWmYJENhYAYV3Y6N3OAECBQIKb0GITiBAYPUCCu/qjf0FAgQIrEpA4V2VrO8SIFAloPBWxekYAgQ2TEDh3bDAnUuAwO4EFN7dufkVAQIE1kFA4V2HFOxAgMDaCyi8ax+RBQkQIPC3Agqvx0GAAIFAQOENkIwQIEBgTQUU3jUNxloECKyXgMK7XnnYhgABAjsRUHh3omWWAIGNFVB4NzZ6hxMgUCCg8BaE6AQCBFYv8G8X3j8v/Pr1664OvXfv3nLz5s1d/daPCBAg0CCg8Dak6AYCBFYusA6Fd7dHPnjwYLl+/fpuf+53BAgQGC+g8I6P0AEECPwKAYX3Vyj7GwQIEFiNgMK7GldfJUCgTODjx4/L3r17t656+PDhcu3atR1f+fbt2+XAgQNbv3v+/Ply7ty5H75z+/bt5erVqzv+/v/6wf3795cbN278lG/5CAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYKKLwTU7MzAQIECBAgQIBALKDwxlQGCRAgQIAAAQIEJgoovBNTszMBAgQIECBAgEAsoPDGVAYJECBAgAABAgQmCii8E1OzMwECBAgQIECAQCyg8MZUBgkQIECAAAECBCYK/AEmS8HOgTxAOgAAAABJRU5ErkJggg==",
    },
  ]);

  const [failureItems, setFailureItems] = useState([
    {
      id: "1",
      content:
        "반송 에러, 정보 꼬임반송 에러, 정보 꼬임반송 에러, 정보 꼬임반송 에러, 정보 꼬임반송 에러, 정보 꼬임반송 에러",
      frequency: 60,
      leadTime: 30,
      woNo: 123675415,
      description: "장비 통신 에러",
      isSelected: false,
    },
    {
      id: "2",
      content: "압력 이상",
      frequency: 50,
      leadTime: 10,
      woNo: 123675415,
      description: "장비 통신 에러",
      isSelected: false,
    },
    {
      id: "3",
      content: "반송 에러, 정보 꼬임",
      frequency: 100,
      leadTime: 100,
      woNo: 123675415,
      description: "장비 통신 에러",
      isSelected: false,
    },
    {
      id: "4",
      content: "반송 에러, 정보 꼬임",
      frequency: 80,
      leadTime: 40,
      woNo: 123675415,
      description: "장비 통신 에러",
      isSelected: false,
    },
    {
      id: "5",
      content: "반송 에러, 정보 꼬임",
      frequency: 60,
      leadTime: 30,
      woNo: 123675415,
      description: "장비 통신 에러",
      isSelected: false,
    },
  ]);

  const [checkItems, setCheckItems] = useState([
    {
      id: "1",
      content:
        "PC 통신 케이블 연결상태 점검, PC 통신 케이블 연결상태 점검, PC 통신 케이블 연결상태 점검, PC 통신 케이블 연결상태 점검, PC 통신 케이블 연결상태 점검",
      measure: "육안",
      measureType: "GAUGE",
      upperBound: "5.00",
      controlUppderBound: "4.00",
      controlLowerBound: "3.00",
      lowerBound: "3.00",
      uom: "Hours",
      valueList: [],
      value: "",
      comment: "연결상태 점검 확인, 점검 확인",
      result: "", //OK , NG
      isSelected: false,
    },
    {
      id: "2",
      content: "반송 에러, 정보꼬임",
      measure: "육안",
      measureType: "GAUGE",
      upperBound: "5.00",
      controlUppderBound: "4.00",
      controlLowerBound: "3.00",
      lowerBound: "3.00",
      uom: "Hours",
      valueList: [],
      value: "",
      comment: "",
      result: "", //OK , NG
      isSelected: false,
    },
  ]);

  const [causeItems, setCauseItems] = useState([
    {
      id: "1",
      content: "통신 Not Connector 상태 이상",
      cause:
        "3줄일 경우입니다. 3줄일 경우입니다. 3줄일 경우입니다. 3줄일 경우입니다. 3줄일 경우입니다. ",
      resolution: "2줄일 경우입니다. 2줄일 경우입니다. 2줄일 경우입니다. ",
      successRate: 50,
      result: "", // NO, YES
    },
    {
      id: "2",
      content:
        "반송 에러, 정보꼬임, 반송 에러, 정보꼬임,반송 에러, 정보꼬임,반송 에러, 정보꼬임,반송 에러, 정보꼬임,반송 에러, 정보꼬임,반송 에러, 정보꼬임,반송 에러, 정보꼬임,반송 에러, 정보꼬임",
      cause:
        "3줄일 경우입니다. 3줄일 경우입니다. 3줄일 경우입니다. 3줄일 경우입니다. 3줄일 경우입니다. ",
      resolution: "2줄일 경우입니다. 2줄일 경우입니다. 2줄일 경우입니다. ",
      successRate: 50,
      result: "", // NO, YES
    },
  ]);

  const onPictureDelete = (id) => {
    const items = pictures.filter((item) => item.id !== id);

    setPictures(items);
  };

  const onCauseAndResolutionPicutureDelete = (id) => {
    const items = causeAndResolutionPictures.filter((item) => item.id !== id);

    setCauseAndResolutionPictures(items);
  };

  const onSelectFailureItem = (id) => {
    const items = failureItems
      .filter((item) => item.id !== id)
      .map((item) => ({ ...item, isSelected: false }));
    const newItem = {
      ...failureItems.filter((item) => item.id === id)[0],
      isSelected: true,
    };

    setFailureItems([...items, newItem]);
  };

  const onSelectCheckItem = (id) => {
    const items = checkItems
      .filter((item) => item.id !== id)
      .map((item) => ({ ...item, isSelected: false }));
    const newItem = {
      ...checkItems.filter((item) => item.id === id)[0],
      isSelected: true,
    };

    setCheckItems([...items, newItem]);
  };

  const onSelectCauseItem = (id, result) => {
    const items = causeItems
      .filter((item) => item.id !== id)
      .map((item) => ({ ...item, result: "" }));
    const newItem = {
      ...causeItems.filter((item) => item.id === id)[0],
      result,
    };

    setCauseItems([...items, newItem]);
  };

  const onSelectCauseItemUndefined = () => {
    // release items
    const items = causeItems.map((item) => ({ ...item, result: "" }));
    setCauseItems(items);

    // and auto save
    onSaveClick();
  };

  return (
    <div className="work-order-manage-diagnosis">
      <div className="tabs-in-tab">
        <ul>
          <li
            className={cx(activeTabinTab === "diagnosis/failure" && "active")}
            onClick={() => handleTabInTab("diagnosis/failure")}
          >
            <span>Failure</span>
            {activeTabinTab !== "diagnosis/failure" &&
              failureItems.filter((item) => item.isSelected === true).length >
                0 && (
                <IconBase className="check-icon">
                  <IconTabCheck />
                </IconBase>
              )}
          </li>
          <li
            className={cx(activeTabinTab === "diagnosis/check" && "active")}
            onClick={() => handleTabInTab("diagnosis/check")}
          >
            <span>Check</span>
            {activeTabinTab !== "diagnosis/check" &&
              checkItems.filter((item) => item.isSelected === true).length >
                0 && (
                <IconBase className="check-icon">
                  <IconTabCheck />
                </IconBase>
              )}
          </li>
          <li
            className={cx(
              activeTabinTab === "diagnosis/causeandresolution" && "active"
            )}
            onClick={() => handleTabInTab("diagnosis/causeandresolution")}
          >
            <span>Cause &amp; Resolution</span>
          </li>
        </ul>
      </div>

      {/* Tab 2-1: Failure */}
      {activeTabinTab === "diagnosis/failure" && (
        <div className="tab-in-tab-panel">
          <div className="tab-in-tab-title">
            <span className="title">List</span>
            <span className="sub-title">택1</span>
          </div>
          <ul className="tab-in-tab-items">
            {failureItems
              .sort((a, b) => (a.id > b.id ? 1 : -1))
              .map((item, idx) => (
                <li key={idx}>
                  <WorkOrderToDoItemTypeE
                    itemIdx={idx + 1}
                    itemName={item.content}
                    itemSelected={item.isSelected}
                    onSelect={() => onSelectFailureItem(item.id)}
                  >
                    {/* <WorkOrderBMDiagnosisFailureTypeA {...item} /> */}
                  </WorkOrderToDoItemTypeE>
                </li>
              ))}
            {/* {!showUndefined ? (
              <li className="flex-end">
                <div
                  className="undefined-btn"
                  onClick={() => setShowUndefined(true)}
                >
                  Undefined
                </div>
              </li>
            ) : (
              <li>
                <WorkOrderToDoItemTypeE
                  itemIdx={`${0}`}
                  itemName={"Undefined"}
                  itemSelected={false}
                  onSelect={() => console.log("check")}
                />
              </li>
            )} */}
          </ul>
        </div>
      )}
      {/* Tab 2-2: Check */}
      {activeTabinTab === "diagnosis/check" && (
        <div className="tab-in-tab-panel">
          <div className="tab-in-tab-title">
            <span className="title">List</span>
            <span className="sub-title">택1</span>
          </div>
          <ul className="tab-in-tab-items">
            {checkItems
              .sort((a, b) => (a.id > b.id ? 1 : -1))
              .map((item, idx) => (
                <li key={idx}>
                  <WorkOrderToDoItemTypeC2
                    itemId={item.id}
                    itemIdx={`${1}.${idx + 1}`}
                    itemName={item.content}
                    itemResult={item.result}
                    itemSelected={item.isSelected}
                    itemComment={item.comment}
                    onSelect={onSelectCheckItem}
                  >
                    <WorkOrderBMDiagnosisCheckTypeA {...item} />
                  </WorkOrderToDoItemTypeC2>
                </li>
              ))}
            {/* {showUndefined && (
              <li>
                <WorkOrderToDoItemTypeE
                  itemIdx={`${0}.${0}`}
                  itemName={"Undefined"}
                  itemSelected={false}
                  onSelect={() => console.log("check")}
                />
              </li>
            )} */}
          </ul>
        </div>
      )}
      {/* Tab 2-3: Cause & Resolution */}
      {activeTabinTab === "diagnosis/causeandresolution" && (
        <div className="tab-in-tab-panel">
          <div className="tab-in-tab-title">
            <span className="title">List</span>
            <span className="sub-title">택1</span>
          </div>
          <ul className="tab-in-tab-items">
            {causeItems
              .sort((a, b) => (a.id > b.id ? 1 : -1))
              .map((item, idx) => (
                <li key={idx}>
                  <WorkOrderToDoItemTypeD
                    itemId={item.id}
                    itemIdx={`${1}.${1}.${idx + 1}`}
                    itemName={item.content}
                    itemResult={item.result}
                    onSelect={onSelectCauseItem}
                  >
                    <WorkOrderBMDiagnosisCauseTypeB {...item} />
                  </WorkOrderToDoItemTypeD>
                </li>
              ))}
            {/* {showUndefined && (
              <li>
                <WorkOrderToDoItemTypeE
                  itemIdx={`${0}.${0}.${0}`}
                  itemName={"Undefined"}
                  itemSelected={false}
                  onSelect={onSelectCauseItemUndefined}
                />
              </li>
            )} */}
          </ul>
        </div>
      )}

      {(activeTabinTab === "diagnosis/failure" ||
        activeTabinTab === "diagnosis/check") && (
        <>
          <div className="pictures-header">
            <span className="title">Failure &amp; Check Picture</span>
            {pictures?.length > 0 && <CircleText text={pictures.length} />}
          </div>
          <div className="pictures-wrapper">
            <div className="picture-template">
              <PictureBox isTemplate={true} />
            </div>
            <ul className="todo-pictures">
              {pictures.map((item, idx) => (
                <li key={idx}>
                  <PictureBox
                    pictureInfo={item.url}
                    onDelete={() => onPictureDelete(item.id)}
                  />
                </li>
              ))}
            </ul>
          </div>
        </>
      )}
      {activeTabinTab === "diagnosis/causeandresolution" && (
        <>
          <div className="pictures-header">
            <span className="title">Cause &amp; Resolution Picture</span>
            {causeAndResolutionPictures?.length > 0 && (
              <CircleText text={causeAndResolutionPictures.length} />
            )}
          </div>
          <div className="pictures-wrapper">
            <div className="picture-template">
              <PictureBox isTemplate={true} />
            </div>
            <ul className="todo-pictures">
              {causeAndResolutionPictures.map((item, idx) => (
                <li key={idx}>
                  <PictureBox
                    pictureInfo={item.url}
                    onDelete={() => onCauseAndResolutionPicutureDelete(item.id)}
                  />
                </li>
              ))}
            </ul>
          </div>
        </>
      )}
      <FixedFooter>
        {activeTabinTab === "diagnosis/failure" && (
          <WorkOrderStepButtonTypeA
            content="Next Step"
            onClick={() => {
              if (
                failureItems.filter((item) => item.isSelected === true)
                  .length === 0
              )
                return;
              handleTabInTab("diagnosis/check");
            }}
            className={
              failureItems.filter((item) => item.isSelected === true).length >
                0 && "active"
            }
          />
        )}
        {activeTabinTab === "diagnosis/check" && (
          <WorkOrderStepButtonTypeA
            content="Next Step"
            onClick={() => {
              if (
                checkItems.filter((item) => item.isSelected === true).length ===
                0
              )
                return;
              handleTabInTab("diagnosis/causeandresolution");
            }}
            className={
              checkItems.filter((item) => item.isSelected === true).length >
                0 && "active"
            }
          />
        )}
        {activeTabinTab === "diagnosis/causeandresolution" && (
          <div className="work-order-btns">
            <WorkOrderStepButtonTypeB
              className={cx("work-order-btn", isSave && "active")}
              content="S/P"
              onClick={
                isSave ? () => setMode("SP_SELECT_BOX") : () => onShowMessage()
              }
            />
            <WorkOrderStepButtonTypeB
              className={cx(
                "work-order-btn",
                "gray",
                causeItems.filter((item) => item.result.length > 0).length >
                  0 && "active"
              )}
              content="Save"
              onClick={() =>
                causeItems.filter((item) => item.result.length > 0).length >
                  0 && onSaveClick()
              }
            />
          </div>
        )}
      </FixedFooter>
      {mode === "SP_SELECT_BOX" && (
        <DimedModal className="work-order-bm-sp-select-modal">
          <div className="sp-select-wrapper">
            <div className="sp-select-header">
              <h3 className="header-text">S/P</h3>
              <IconBase onClick={() => setMode("")}>
                <IconClosePopup />
              </IconBase>
            </div>
            <ul className="sp-select-items">
              <li className="sp-select-item">
                <div className="sp-select-item-content" onClick={onClickNextSP}>
                  S/P Requirements
                </div>
              </li>
              <li className="sp-select-item">
                <div
                  className="sp-select-item-content"
                  onClick={onClickNextSPIssue}
                >
                  S/P Issue
                </div>
              </li>
            </ul>
          </div>
        </DimedModal>
      )}
    </div>
  );
};

export default WorkOrderManageDiagnosis;
