import React, { useState } from "react";
import cx from "classnames";
import FixedFooter from "components/base/FixedFooter";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import SuccessModal from "components/modal/SuccessModal";

import WorkOrderOPCard from "components/workOrder/WorkOrderOPCard";

import { useSnackbar } from "notistack";

import { useHistory } from "react-router-dom";

import "./WorkOrderManageOP.scss";

const WorkOrderManageOP = ({ woNo, onDetailClick }) => {
  const [mode, setMode] = useState("");
  const [opData, setOpData] = useState([
    {
      opIndex: "10",
      opNo: "OP00001038",
      opDescription: "L Battery Module Assemply #1",
      opResult: "",
      isDone: false,
    },
    {
      opIndex: "11",
      opNo: "OP00001039",
      opDescription: "L Battery Module Assemply #2",
      opResult: "NG",
      isDone: false,
    },
    {
      opIndex: "12",
      opNo: "OP00001040",
      opDescription: "L Battery Module Assemply #3",
      opResult: "OK",
      isDone: false,
    },
  ]);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = () => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar("저장할 OP를 선택하세요.", {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 118 }}
          onClick={() => closeSnackbar(key)}
        >
          <span>{message}</span>
        </div>
      ),
    });
  };
  const onSelectToggle = (opNo) => {
    const newItem = opData
      .filter((data) => data.opNo === opNo)
      .map((data) => ({ ...data, isDone: !data.isDone }));
    const item = opData.filter((data) => data.opNo !== opNo);
    setOpData([...newItem, ...item]);
  };
  const onResultClick = (opNo, opResult) => {
    const newItem = opData
      .filter((data) => data.opNo === opNo)
      .map((data) => ({ ...data, opResult }));
    const item = opData.filter((data) => data.opNo !== opNo);
    setOpData([...newItem, ...item]);
  };
  const onAllSelectToggle = () => {
    // 선택 안된게 없다면 전체해제
    // 그렇지 않다면 전체선택
    const selectFlag = opData.filter((data) => !data.isDone).length === 0;
    if (selectFlag) {
      const newItems = opData.map((data) => ({ ...data, isDone: false }));
      setOpData(newItems);
    } else {
      const newItems = opData.map((data) => ({ ...data, isDone: true }));
      setOpData(newItems);
    }
  };
  const onAllOK = () => {
    const newItems = opData.map((data) => ({ ...data, opResult: "OK" }));
    setOpData(newItems);
  };

  const onSave = () => {
    if (opData.filter((data) => data.isDone).length === 0) {
      onShowMessage();
      return;
    }
    setMode("SUCCESS_MODAL");
  };

  //setMode("OP/DETAIL");

  const history = useHistory();
  const onComplete = () => {
    history.go(0);
  };

  return (
    <>
      <div className="work-order-manage-op">
        <div className="op-title">
          <div className="title">Operation</div>
          <div className="all-btns">
            <div className={cx("all-select", "active")} onClick={onAllOK}>
              ALL OK
            </div>
            <div className="splitter" />
            <div
              className={cx("all-select", "active")}
              onClick={onAllSelectToggle}
            >
              {opData.filter((data) => !data.isDone).length === 0
                ? "전체해제"
                : "전체선택"}
            </div>
          </div>
        </div>
        {opData && (
          <ul className="op-items">
            {opData
              .sort((a, b) => (a.opIndex > b.opIndex ? 1 : -1))
              .map((data) => (
                <li className="op-item" key={data.opIndex}>
                  <WorkOrderOPCard
                    {...data}
                    onResultClick={onResultClick}
                    onDetailClick={onDetailClick}
                    onToggle={() => onSelectToggle(data.opNo)}
                  />
                </li>
              ))}
          </ul>
        )}
        <FixedFooter className="footer-white">
          <div className="btns-wrapper">
            {opData.filter((data) => data.isDone).length > 0 && (
              <div className="counter">
                <span className="select-count">
                  {`${opData.filter((data) => data.isDone).length} `}
                </span>
                <span className="select-total">{`/ ${opData.length}`}</span>
              </div>
            )}
            <div className="sp-issue">SP Issue</div>
            <div className="result-save" onClick={onSave}>
              Save
            </div>
          </div>
        </FixedFooter>
      </div>

      <ModalFullPageAni
        modeState={mode}
        pageName="SUCCESS_MODAL"
        motion="fade"
        isDimmed={true}
      >
        <SuccessModal
          content={
            <span>
              WO-{woNo} OP의 작업을
              <br />
              완료하였습니다!
            </span>
          }
          onClick={() => onComplete()}
          onClose={() => setMode("")}
        />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrderManageOP;
