import React, { useState } from "react";
import cx from "classnames";

import InputSelector from "components/common/InputSelector";
import WorkOrderSPCircleText from "components/workOrder/WorkOrderSPCircleText";

import { IconPlus, IconMinus, IconComment } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import useDelayUnmount from "lib/hooks/useDelayUnmount";

import "./WorkOrderToDoItemTypeD.scss";

const WorkOrderToDoItemTypeD = ({
  itemId,
  itemIdx,
  itemName,
  itemResult,
  className,
  onSelect,
  children,
}) => {
  const [isExpand, setIsExpand] = useState(false);

  const onClick = () => {
    setIsExpand(!isExpand);
  };

  const onSelectClick = (itemId, result) => {
    onSelect(itemId, result);
    setIsExpand(false);
  };

  const shouldRender = useDelayUnmount(isExpand, 300);

  return (
    <div
      className={cx(
        "work-order-todo-item-typeD",
        isExpand && "expand",
        className
      )}
    >
      <div className="todo-item-typeD-header" onClick={onClick}>
        <div className="col">
          <span className="item-index">{itemIdx}</span>
          <span className={cx("item-name", !isExpand && "line-ellipse")}>
            {itemName}
          </span>
        </div>
        {itemResult ? (
          <div className="col">
            {isExpand ? (
              <IconMinus size="20" fill="#A5B3C2" />
            ) : (
              <div
                className={cx(
                  "selected-round-tag",
                  itemResult === "NO" && "alert"
                )}
              >
                {itemResult}
              </div>
            )}
          </div>
        ) : (
          <div className="col">
            {!isExpand && <span className="expand-text">더보기</span>}
            <IconBase>
              {isExpand ? (
                <IconMinus size="20" fill="#A5B3C2" />
              ) : (
                <IconPlus size="20" fill="#A5B3C2" />
              )}
            </IconBase>
          </div>
        )}
      </div>
      {shouldRender && (
        <div
          className={cx(
            "todo-item-typeD-content",
            isExpand ? "expand" : "collapse"
          )}
        >
          {children}
          {/* <div className="todo-item-labeled">
            <span className="label">Comments</span>
            <IconBase className={cx("comment", comments?.length > 0 && "more")}>
              <IconComment />
            </IconBase>
          </div> */}
          <div className="todo-item-btns">
            <button
              className={cx(
                "btn-check-select",
                "ng-btn",
                itemResult === "NO" && "selected"
              )}
              onClick={() => onSelectClick(itemId, "NO")}
            >
              <span>No</span>
            </button>
            <button
              className={cx(
                "btn-check-select",
                "ok-btn",
                itemResult === "Yes" && "selected"
              )}
              onClick={() => onSelectClick(itemId, "Yes")}
            >
              <span>Yes</span>
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default WorkOrderToDoItemTypeD;
