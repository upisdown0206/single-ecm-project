import React from "react";
import cx from "classnames";

import { IconCheckTodo, IconMainReleaseFull } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderToDoItemTypeA.scss";

const WorkOrderToDoItemTypeA = ({
  itemName,
  isCheck,
  onToDoCheckClick,
  className,
}) => {
  return (
    <div
      className={cx("work-order-todo-item-typeA", className)}
      onClick={onToDoCheckClick}
    >
      <span className="item-name">{itemName}</span>
      <IconBase>
        {isCheck ? (
          <IconMainReleaseFull fill="#26B9D1" width="21" height="21" />
        ) : (
          <IconCheckTodo fill="#BEC9D4" width="21" height="21" />
        )}
      </IconBase>
    </div>
  );
};

export default WorkOrderToDoItemTypeA;
