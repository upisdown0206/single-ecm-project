import React from "react";
import cx from "classnames";

import "./WorkOrderTabButtonTypeA.scss";

const WorkOrderTabButtonTypeA = ({ number, tabName, className, onClick }) => {
  return (
    <div
      className={cx("work-order-tab-button-typeA", className)}
      onClick={onClick}
    >
      <div className="circle-number">
        <span>{number}</span>
      </div>
      <div className="tab-name">
        <span>{tabName}</span>
      </div>
    </div>
  );
};

export default WorkOrderTabButtonTypeA;
