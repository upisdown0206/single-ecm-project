import React from "react";
import cx from "classnames";

import { IconDiagnosis, IconSafetyPlan } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderStepBox.scss";

const WorkOrderStepBox = ({ woProcessItems, onStepClick }) => {
  return (
    <div className="work-order-step-box">
      <ul className="step-items">
        {woProcessItems?.map((item, idx) => (
          <li
            key={idx}
            className={cx(
              "step-item",
              woProcessItems?.filter((item) => item.isDone === false).length ===
                0 && "all-done"
            )}
            onClick={() => onStepClick(item.type)}
          >
            <div className={cx("work-order-step", item.processStatus)}>
              <IconBase className={cx("circle", item.isDone && "done")}>
                {item.type === "safetyPlan" && <IconSafetyPlan />}
                {(item.type === "diagnosis" || item.type === "op") && (
                  <IconDiagnosis />
                )}
              </IconBase>
              <span className="title">{item.processName}</span>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default WorkOrderStepBox;
