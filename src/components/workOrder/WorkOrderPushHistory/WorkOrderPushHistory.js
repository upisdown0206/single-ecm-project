import React, { useState } from "react";
import BMWrap from "components/workOrder/BM/BMWrap";
import BMdarkDiv from "components/workOrder/BM/BMdarkDiv";
import { IconHistory } from "components/common/Icons";
import BmCardType from "components/workOrder/BM/BMCardType";

const WorkOrderPushHistory = ({ onClose }) => {
  const [isSelectNowDateTag, setIsSelectNowDateTag] = useState(false);
  const [isSelectDateTag, setIsSelectDateTag] = useState(false);
  const selectCalendar = (e) => {
    setIsSelectNowDateTag(!isSelectNowDateTag);
  };
  return (
    <>
      <BMWrap title={"Push History"} icon={<IconHistory />} onClose={onClose}>
        <BMdarkDiv nowsTitle={"Equipment ID"} nowTitle={"P7TPHT0104"} />
        <BmCardType />
      </BMWrap>
    </>
  );
};

export default WorkOrderPushHistory;
