import React, { useState } from "react";
import cx from "classnames";

import {
  IconPlus,
  IconMinus,
  IconComment,
  IconCheck,
  IconMainReleaseFull,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import useDelayUnmount from "lib/hooks/useDelayUnmount";

import "./WorkOrderToDoItemTypeC2.scss";

const WorkOrderToDoItemTypeC2 = ({
  itemId,
  itemIdx,
  itemName,
  itemResult,
  itemComment,
  itemSelected,
  className,
  onSelect,
  onComment,
  children,
}) => {
  const [isExpand, setIsExpand] = useState(false);
  const [result, setResult] = useState(itemResult);

  const onClick = () => {
    setIsExpand(!isExpand);
  };

  const onSelectClick = (id) => {
    onSelect(id);
    setIsExpand(false);
  };

  const [inputValue, setInputValue] = useState("");
  const [commentValue, setCommentValue] = useState("");

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    setInputValue(value);
  };
  const onCommentChange = (e) => {
    const {
      target: { value },
    } = e;
    setCommentValue(value);
  };

  const shouldRender = useDelayUnmount(isExpand, 300);

  return (
    <div
      className={cx(
        "work-order-todo-item-typeC2",
        isExpand && "expand",
        className
      )}
    >
      <div className="todo-item-typeC-header" onClick={onClick}>
        <div className="col">
          <span className="item-index">{itemIdx}</span>
          <span className={cx("item-name", !isExpand && "line-ellipse")}>
            {itemName}
          </span>
        </div>
        {itemSelected ? (
          <div className="col">
            {isExpand ? (
              <IconMinus size="20" fill="#A5B3C2" />
            ) : (
              <>
                <IconBase>
                  <IconMainReleaseFull fill="#26B9D1" width="21" height="21" />
                </IconBase>
                <span className="selected-text">선택</span>
              </>
            )}
          </div>
        ) : (
          <div className="col">
            {!isExpand && <span className="expand-text">더보기</span>}
            <IconBase>
              {isExpand ? (
                <IconMinus size="20" fill="#A5B3C2" />
              ) : (
                <IconPlus size="20" fill="#A5B3C2" />
              )}
            </IconBase>
          </div>
        )}
      </div>
      {shouldRender && (
        <div
          className={cx(
            "todo-item-typeC-content",
            isExpand ? "expand" : "collapse"
          )}
        >
          <div className="spliter mt-none" />
          {children}
          <div className="spliter" />
          <div className="todo-item-labeled">
            <span className="label">Value</span>
            <input
              className="input-content"
              type="number"
              placeholder="Value"
              value={inputValue}
              onChange={onChange}
            />
          </div>
          <div className="todo-item-labeled flex-top">
            <span className="label">Comments</span>
            <textarea
              className="input-content"
              placeholder="Placeholder"
              value={commentValue}
              onChange={onCommentChange}
            />
          </div>
          <div className="todo-item-labeled">
            <span className="label">Check</span>
            <ul className="input-checkbtnItems">
              <li className="input-checkbtnItem">
                <div
                  className={cx("check-button", result === "NG" && "selected")}
                  onClick={() => setResult("NG")}
                >
                  NG
                </div>
              </li>
              <li className="input-checkbtnItem">
                <div
                  className={cx("check-button", result === "OK" && "selected")}
                  onClick={() => setResult("OK")}
                >
                  OK
                </div>
              </li>
            </ul>
          </div>
          <div className="todo-item-btns">
            <button
              className={cx("btn-item-select", itemSelected && "selected")}
              onClick={() => onSelectClick(itemId)}
            >
              <IconBase>
                {itemSelected ? (
                  <IconCheck strokeFill="#ffffff" />
                ) : (
                  <IconCheck />
                )}
              </IconBase>
              {!itemSelected && <span>선택</span>}
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default WorkOrderToDoItemTypeC2;
