import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";

import PullToRefresh from "components/common/PullToRefresh";

import SearchConditionPanelTypeA from "components/search/SearchConditionPanelTypeA";
import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";
import FormDetail from "components/search/FormDetail";

import WorkOrderResultCardTypeA from "components/workOrder/WorkOrderResultCardTypeA";
import WorkOrderResultCardTypeB from "components/workOrder/WorkOrderResultCardTypeB";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./WorkOrderList.scss";

const WorkOrderList = ({ onSearchClick, onDetailClick }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "owningDept", text: "홍길동" },
    { id: "2", type: "owningDept", text: "P7 C/F 일상보전반" },
    { id: "3", type: "equipmentId", text: "2020-08-11 ~ 2020-08-11" },
    { id: "4", type: "equipmentId", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);
  const [results] = useState([
    {
      progressState: "In progress",
      woType: "BM",
      woNo: "123675415",
      equipmentId: "P8TCVD0103",
      description: "[MES-BM] 155695ABDT001",
      isActualStarted: true,
    },
    {
      progressState: "Release",
      woType: "PM",
      woNo: "123675416",
      equipmentId: "P7FCVD0101",
      description: "[PM] 155695ABDT001",
      isActualStarted: false,
    },
    {
      progressState: "In progress",
      woType: "BM",
      woNo: "123675417",
      equipmentId: "P8TCVD0103",
      description: "[MES-BM] 155695ABDT001",
      isActualStarted: true,
    },
    {
      progressState: "Release",
      woType: "PM",
      woNo: "123675418",
      equipmentId: "P7FCVD0101",
      description: "[PM] 155695ABDT001",
      isActualStarted: false,
    },
    {
      progressState: "In progress",
      woType: "BM",
      woNo: "123675419",
      equipmentId: "P8TCVD0103",
      description: "[MES-BM] 155695ABDT001",
      isActualStarted: false,
    },
    {
      progressState: "In progress",
      woType: "BM",
      woNo: "123675420",
      equipmentId: "P8TCVD0103",
      description: "[MES-BM] 155695ABDT001",
      isActualStarted: false,
    },
    {
      progressState: "In progress",
      woType: "BM",
      woNo: "123675421",
      equipmentId: "P8TCVD0103",
      description: "[MES-BM] 155695ABDT001",
      isActualStarted: false,
    },
  ]);

  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <>
      <div className="work-order-list">
        <FixedHeader
          left={
            <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
              {!isMenuOpen && <IconHamburger />}
            </IconBase>
          }
          center={
            <div className="equipment-title">
              <HeaderTitle text="Work Order List">
                {results && <CircleText text={results.length} />}
              </HeaderTitle>
            </div>
          }
        />

        <BaseMain
          isMenuOpen={isMenuOpen}
          onMenuClick={onMenuClick}
          style={{
            height: `calc(100vh - (56px + 60px + 56px))`,
            marginTop: `calc(56px + 56px)`,
          }}
        >
          <FixedWrapper style={{ top: "56px" }}>
            <SearchConditionPanelTypeB
              onSearchClick={onSearchClick}
              keywords={keywords}
              setKeywords={setKeywords}
              isBorderBot={false}
            />
          </FixedWrapper>
          {/* <PullToRefresh onRefresh={handleRefreshTest}> */}
          <div className="work-order-list-main">
            <ul className="wo-list-items">
              {results.map((result, idx) => (
                <li
                  key={idx}
                  onClick={() => onDetailClick(result.woNo, result.woType)}
                >
                  <>
                    {idx < results.length / 2 ? (
                      <WorkOrderResultCardTypeA
                        progressState={result.progressState}
                        woType={result.woType}
                        woNo={result.woNo}
                        equipmentId={result.equipmentId}
                        description={result.description}
                        isActualStarted={result.isActualStarted}
                      />
                    ) : (
                      <WorkOrderResultCardTypeB
                        progressState={result.progressState}
                        woType={result.woType}
                        woNo={result.woNo}
                        equipmentId={result.equipmentId}
                        description={result.description}
                        isActualStarted={result.isActualStarted}
                      />
                    )}
                  </>
                </li>
              ))}
            </ul>
          </div>
          {/* </PullToRefresh> */}
        </BaseMain>
        {!isMenuOpen && (
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        )}
      </div>
    </>
  );
};

export default WorkOrderList;
