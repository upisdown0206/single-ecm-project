import React from "react";
import cx from "classnames";

import "./WorkOrderStepButtonTypeB.scss";

const WorkOrderStepButtonTypeB = ({ content, className, isAlert, onClick }) => {
  return (
    <button
      className={cx(
        "work-order-step-button-typeB",
        className,
        isAlert && "alert"
      )}
      onClick={onClick}
    >
      <span>{content}</span>
    </button>
  );
};

export default WorkOrderStepButtonTypeB;
