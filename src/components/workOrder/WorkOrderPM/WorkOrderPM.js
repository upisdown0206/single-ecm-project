import React, { useState, useEffect } from "react";
import cx from "classnames";

import WorkOrderManageSafetyPlan from "components/workOrder/WorkOrderManageSafetyPlan";
import WorkOrderManageOP from "components/workOrder/WorkOrderManageOP";
import WorkOrderTabButtonTypeA from "components/workOrder/WorkOrderTabButtonTypeA";

import "./WorkOrderPM.scss";

const WorkOrderPM = ({ woNo, tabPage = "sp", onDetailClick }) => {
  const [activeTab, setActiveTab] = useState("safetyplan");
  const handleTab = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  useEffect(() => {
    if (tabPage === "sp") {
      handleTab("safetyplan");
    } else if (tabPage === "op") {
      handleTab("op");
    }
  }, []);

  return (
    <div className="work-order-pm">
      <div className="work-order-tabs">
        <div className="inner">
          <WorkOrderTabButtonTypeA
            number="1"
            tabName="Safety Plan"
            className={cx(
              "tab",
              "right-to-left",
              activeTab === "safetyplan" && "active"
            )}
            onClick={() => handleTab("safetyplan")}
          />
          <WorkOrderTabButtonTypeA
            number="2"
            tabName="OP"
            className={cx(
              "tab",
              "left-to-right",
              activeTab === "op" && "active"
            )}
            onClick={() => handleTab("op")}
          />
          <div className={cx("animation-bg", activeTab)} />
        </div>
      </div>

      {/* Tab 1: Safety Plan */}
      <div
        className={cx(
          "tab-panel",
          activeTab === "safetyplan" ? "visible" : "hidden"
        )}
      >
        <WorkOrderManageSafetyPlan onNextClick={() => handleTab("op")} />
      </div>
      {/* Tab 2: OP */}
      <div
        className={cx("tab-panel", activeTab === "op" ? "visible" : "hidden")}
      >
        <WorkOrderManageOP woNo={woNo} onDetailClick={onDetailClick} />
      </div>
    </div>
  );
};

export default WorkOrderPM;
