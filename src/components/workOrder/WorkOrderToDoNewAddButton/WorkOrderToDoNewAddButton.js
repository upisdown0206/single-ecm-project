import React from "react";

import { IconPlus } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderToDoNewAddButton.scss";

const WorkOrderToDoNewAddButton = ({ ...rest }) => {
  return (
    <button className="work-order-todo-new-add-button" {...rest}>
      <IconBase>
        <IconPlus size="24" />
      </IconBase>
      <span>새항목추가하기</span>
    </button>
  );
};

export default WorkOrderToDoNewAddButton;
