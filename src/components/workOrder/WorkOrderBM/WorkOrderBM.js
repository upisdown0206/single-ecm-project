import React, { useState, useEffect } from "react";
import cx from "classnames";

import DimedModal from "components/base/DimedModal";
import OKCancelModal from "components/modal/OKCancelModal";

import WorkOrderManageSafetyPlan from "components/workOrder/WorkOrderManageSafetyPlan";
import WorkOrderManageDiagnosis from "components/workOrder/WorkOrderManageDiagnosis";
import WorkOrderTabButtonTypeA from "components/workOrder/WorkOrderTabButtonTypeA";

import { useSnackbar } from "notistack";

import "./WorkOrderBM.scss";

const WorkOrderBM = ({ onClickNextSP, onClickNextSPIssue, tabPage = "sp" }) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = () => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar("결과값을 저장해야 자재요청을 할 수 있습니다.", {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 80 }}
          onClick={() => closeSnackbar(key)}
        >
          <span>{message}</span>
        </div>
      ),
    });
  };

  const [isSaveCheckModal, setIsSaveCheckModal] = useState(false);
  const onSaveClick = () => {
    setIsSaveCheckModal(true);
  };
  const onCancelModalSaveCheckClick = () => {
    setIsSaveCheckModal(false);
  };

  const [isSave, setIsSave] = useState(false);
  const onSaveCheckModalClick = () => {
    // Save Confirm BM
    // S/P 버튼 활성화
    setIsSave(true);
    setIsSaveCheckModal(false);
  };

  const [activeTab, setActiveTab] = useState("safetyplan");
  const handleTab = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  useEffect(() => {
    if (tabPage === "sp") {
      handleTab("safetyplan");
    } else if (tabPage === "diagnosis") {
      handleTab("diagnosis");
    }
  }, []);

  return (
    <div className="work-order-bm">
      <div className="work-order-tabs">
        <div className="inner">
          <WorkOrderTabButtonTypeA
            number="1"
            tabName="Safety Plan"
            className={cx(
              "tab",
              "right-to-left",
              activeTab === "safetyplan" && "active"
            )}
            onClick={() => handleTab("safetyplan")}
          />
          <WorkOrderTabButtonTypeA
            number="2"
            tabName="Diagnosis"
            className={cx(
              "tab",
              "left-to-right",
              activeTab === "diagnosis" && "active"
            )}
            onClick={() => handleTab("diagnosis")}
          />
          <div className={cx("animation-bg", activeTab)} />
        </div>
      </div>
      {/* Tab 1: Safety Plan */}
      <div
        className={cx(
          "tab-panel",
          activeTab === "safetyplan" ? "visible" : "hidden"
        )}
      >
        <WorkOrderManageSafetyPlan
          onNextClick={() => {
            handleTab("diagnosis");
          }}
        />
      </div>
      {/* Tab 2: Diagnosis */}
      <div
        className={cx(
          "tab-panel",
          activeTab === "diagnosis" ? "visible" : "hidden"
        )}
      >
        <WorkOrderManageDiagnosis
          isSave={isSave}
          onSaveClick={onSaveClick}
          onShowMessage={onShowMessage}
          onClickNextSP={onClickNextSP}
          onClickNextSPIssue={onClickNextSPIssue}
        />
      </div>
      {isSaveCheckModal && (
        <DimedModal className="modal-top-max work-order-bm-save-check-modal">
          <OKCancelModal
            title="저장하시겠습니까?"
            onConfirm={onSaveCheckModalClick}
            onClose={onCancelModalSaveCheckClick}
          >
            <p className="modal-content">
              자재요청 후 결과값 수정이 불가합니다.
            </p>
          </OKCancelModal>
        </DimedModal>
      )}
    </div>
  );
};

export default WorkOrderBM;
