import React from "react";
import cx from "classnames";

import "./WorkOrderSPCircleText.scss";

const WorkOrderSPCircleText = ({ children, className, ...rest }) => {
  return (
    <div className={cx("work-order-sp-circle-text", className)} {...rest}>
      {children}
    </div>
  );
};

export default WorkOrderSPCircleText;
