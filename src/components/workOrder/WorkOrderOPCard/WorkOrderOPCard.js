import React from "react";
import cx from "classnames";

import {
  IconMainReleaseFull,
  IconCheckTodo,
  IconEdit,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderOPCard.scss";

const WorkOrderOPCard = ({
  opIndex,
  opNo,
  opDescription,
  opResult,
  isDone,
  onResultClick,
  onDetailClick,
  onToggle,
}) => {
  return (
    <div
      className={cx("work-order-op-card", isDone && "active")}
      onClick={onToggle}
    >
      <IconBase className={cx("ab-right", "active")}>
        {isDone ? (
          <IconMainReleaseFull fill="#26B9D1" width="21" height="21" />
        ) : (
          <IconCheckTodo fill="#BEC9D4" width="21" height="21" />
        )}
      </IconBase>
      <div className="card-title">
        <span className="no">{opIndex}</span>
        <span className="name">{opNo}</span>
      </div>
      <div className="card-content">
        <span className="desc">{opDescription}</span>
      </div>
      <div className="card-buttons">
        <div
          className={cx("card-button", opResult === "OK" && "ok-active")}
          onClick={(e) => {
            e.stopPropagation();
            onResultClick(opNo, "OK");
          }}
        >
          OK
        </div>
        <div
          className={cx("card-button", opResult === "NG" && "ng-active")}
          onClick={(e) => {
            e.stopPropagation();
            onResultClick(opNo, "NG");
          }}
        >
          NG
        </div>
        <div
          className="detail-btn"
          onClick={(e) => {
            e.stopPropagation();
            onDetailClick(opIndex);
          }}
        >
          <IconEdit />
        </div>
      </div>
    </div>
  );
};

export default WorkOrderOPCard;
