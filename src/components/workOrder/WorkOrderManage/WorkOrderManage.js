import React, { useState } from "react";
import cx from "classnames";

import ModalFullPageAni from "components/common/ModalFullPageAni";

import FixedHeader from "components/base/FixedHeader";
import BaseMain from "components/base/BaseMain";

import HeaderTitle from "components/common/HeaderTitle";
import { IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DimedModal from "components/base/DimedModal";
import OKCancelModal from "components/modal/OKCancelModal";
import WorkOrderBM from "components/workOrder/WorkOrderBM";
import WorkOrderPM from "components/workOrder/WorkOrderPM";
import WorkOrderOPDetail from "components/workOrder/WorkOrderOPDetail";

import "./WorkOrderManage.scss";
import WorkOrderBMSPRequirements from "components/workOrder/WorkOrderBMSPRequirements";
import WorkOrderBMSPIssue from "components/workOrder/WorkOrderBMSPIssue";

const WorkOrderManage = ({ woNo, woType, tabPage, onClose }) => {
  const [mode, setMode] = useState("init");

  const [isCheckModal, setIsCheckModal] = useState(false);

  const onBackClick = () => {
    setIsCheckModal(true);
    //setMode("WORK_ORDER_MANAGE/CONFIRM_MODAL");
  };

  const onCancelModalBackClick = () => {
    setIsCheckModal(false);
    //setMode("");
  };

  const onConfirmModalBackClick = () => {
    // hide page
    onClose();
  };

  const onClickNextSP = () => {
    setMode("WORK_ORDER_BM/SP_MANAGE");
  };
  const onClickNextSPIssue = () => {
    setMode("WORK_ORDER_BM/SP_ISSUE");
  };

  const [opIndex, setOpIndex] = useState();
  const onDetailClick = (opIndex) => {
    setOpIndex(opIndex);
    setMode("WORK_ORDER_PM/OP/DETAIL");
  };

  return (
    <>
      <ModalFullPageAni
        className="work-order-manage"
        isBaseComponent={true}
        modeState={mode}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text={woNo} />}
        />
        <BaseMain
          style={{
            height: `calc(100vh - (56px + 79px))`,
            marginTop: `calc(56px)`,
            background: "white",
          }}
        >
          {woType === "BM" ? (
            <WorkOrderBM
              onClickNextSP={onClickNextSP}
              onClickNextSPIssue={onClickNextSPIssue}
              tabPage={tabPage}
            />
          ) : (
            <WorkOrderPM
              woNo={woNo}
              tabPage={tabPage}
              onDetailClick={onDetailClick}
            />
          )}
        </BaseMain>
        {isCheckModal && (
          <DimedModal className="modal-top-max work-order-save-check-modal">
            <OKCancelModal
              title="페이지 나가기"
              onConfirm={onConfirmModalBackClick}
              onClose={onCancelModalBackClick}
            >
              <p className="modal-content">
                페이지를 벗어나면 작업한 내용이 사라집니다. 그래도
                나가시겠습니까?
              </p>
            </OKCancelModal>
          </DimedModal>
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_BM/SP_MANAGE">
        <WorkOrderBMSPRequirements onClick={() => setMode("")} />
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_BM/SP_ISSUE">
        <WorkOrderBMSPIssue onClick={() => setMode("")} />
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_PM/OP/DETAIL">
        <WorkOrderOPDetail onClose={() => setMode("")} opIndex={opIndex} />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrderManage;
