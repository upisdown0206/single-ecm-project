import React, { useState } from "react";
import cx from "classnames";

import DimedModal from "components/base/DimedModal";
import FixedFooter from "components/base/FixedFooter";

import { IconListSelect } from "components/common/Icons";
import Button from "components/common/Button";

import "./WorkOrderSelectBoxModal.scss";

const WorkOrderSelectBoxModal = ({ onClick, onSelect }) => {
  const [options, setOptions] = useState([
    {
      id: 999,
      text: "전체",
    },
    {
      id: 1,
      text: "option 1",
    },
    {
      id: 2,
      text: "option 2",
    },
    {
      id: 3,
      text: "option 3",
    },
    {
      id: 4,
      text: "option 4",
    },
    {
      id: 5,
      text: "option 5",
    },
    {
      id: 6,
      text: "option 6",
    },
    {
      id: 7,
      text: "option 7",
    },
    {
      id: 8,
      text: "option 8",
    },
    {
      id: 9,
      text: "option 9",
    },
  ]);

  return (
    <DimedModal>
      <div className="work-order-select-wrapper">
        <div className="modal-header">
          <h1>WO Type</h1>
          <button>초기화</button>
        </div>
        <div className="modal-select-list">
          <ul className="select-items">
            {options.map((option, idx) => (
              <li key={idx}>
                <div
                  data-id={option.id}
                  className={cx("select-item", idx === 0 && "active")}
                >
                  {option.text}
                  {idx === 0 && <IconListSelect />}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>

      <FixedFooter className="select-box-button-wrapper">
        <Button fullWidth onClick={onClick}>
          확인
        </Button>
      </FixedFooter>
    </DimedModal>
  );
};

export default WorkOrderSelectBoxModal;
