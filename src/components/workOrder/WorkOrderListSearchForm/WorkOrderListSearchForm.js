import React, { useState } from "react";
import moment from "moment";
import { format } from "date-fns";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import DateCalendarPicker from "components/common/DateCalendarPicker";
import DateTimePicker from "components/common/DateTimePicker";
import ModalFullPage from "components/common/ModalFullPage";
import ModalFullPageAni from "components/common/ModalFullPageAni";

import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeD from "components/search/LabelInputTypeD";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";

import WorkOrderSelectBoxModal from "components/workOrder/WorkOrderSelectBoxModal";
import LabelSelectBox from "components/workOrder/LabelSelectBox";
import FormDetail from "components/search/FormDetail";

import { IconCalendar, IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import DimedModal from "components/base/DimedModal";
import SingleSelect from "components/common/select/SingleSelect";

import "./WorkOrderListSearchForm.scss";

const WorkOrderListSearchForm = ({ onClose }) => {
  const [mode, setMode] = useState("init");
  const onInnerClose = () => {
    setMode("");
  };
  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");

  const [keywords, setKeywords] = useState([
    { id: "1", type: "woNo", text: "wo1111" },
    { id: "2", type: "woNo", text: "wo2222" },
    { id: "3", type: "equipmentId", text: "123444" },
    { id: "4", type: "equipmentId", text: "456555" },
    { id: "5", type: "owningDept", text: "조립1팀" },
    { id: "6", type: "owningDept", text: "조립2팀" },
    { id: "7", type: "woType", text: "BM" },
    { id: "8", type: "woType", text: "PM" },
  ]);

  const optionSelectItems = [
    {
      id: "all",
      value: "전체",
    },
    {
      id: "o1",
      value: "option 1",
    },
    {
      id: "o2",
      value: "option 2",
    },
    {
      id: "o3",
      value: "option 3",
    },
    {
      id: "o4",
      value: "option 4",
    },
    {
      id: "o5",
      value: "option 5",
    },
    {
      id: "o6",
      value: "option 6",
    },
    {
      id: "o7",
      value: "option 7",
    },
    {
      id: "o8",
      value: "option 8",
    },
    {
      id: "o9",
      value: "option 9",
    },
  ];
  const [optionSelectedId, setOptionSelectedId] = useState("");

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const [isShowSelectBox, setIsShowSelectBox] = useState(false);
  const onSelectModal = () => {
    setIsShowSelectBox(!isShowSelectBox);
  };

  const [isShowFirstCalenderBox, setIsShowFirstCalendarBox] = useState(false);
  const [isShowSecondCalenderBox, setIsShowSecondCalendarBox] = useState(false);
  const [fromToType, setFromToType] = useState("FROM");
  const [isShowFirstTimeBox, setIsShowFirstTimeBox] = useState(false);
  const [isShowSecondTimeBox, setIsShowSecondTimeBox] = useState(false);

  const onFirstCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
  };
  const onSecondCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowSecondCalendarBox(!isShowSecondCalenderBox);
  };

  const onFirstTimeModal = (e) => {
    setIsShowFirstTimeBox(!isShowFirstTimeBox);
  };
  const onSecondTimeModal = (e) => {
    setIsShowSecondTimeBox(!isShowSecondTimeBox);
  };

  const onBackClick = () => {
    // close page
    onClose();
  };

  const onInputClick = (e, type, initValue = "") => {
    // render form-detail (type)
    setMode("FORM_DETAIL");
    setFormType(type);
    setSearchInitValue(initValue);
  };

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const onInputAllClearClick = () => {
    setKeywords([]);
  };

  const [scheduledFromDt, setScheduledFromDt] = useState("");
  const [scheduledFromTime, setScheduledFromTime] = useState(
    moment("00:00", "HH:mm")
  );
  const [scheduledToDt, setScheduledToDt] = useState("");
  const [scheduledToTime, setScheduledToTime] = useState(
    moment("00:00", "HH:mm")
  );
  const [mesDownFromDt, setMesDownFromDt] = useState("");
  const [mesDownFromTime, setMesDownFromTime] = useState(
    moment("00:00", "HH:mm")
  );
  const [mesDownToDt, setMesDownToDt] = useState("");
  const [mesDownToTime, setMesDownToTime] = useState(moment("00:00", "HH:mm"));

  return (
    <>
      <ModalFullPageAni
        className="work-order-list-search-form"
        modeState={mode}
        isBaseComponent={true}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Search" />}
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + ${isKeyboardFocus ? `0px` : `79px`})`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeD
              labelText="WO No"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="woNo"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB
              labelText="Equipment ID"
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeD
              labelText="Owning Department"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="owningDept"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>

          <div className="label-input">
            <LabelSelectBox
              labelText="WO Type"
              defaultText={
                optionSelectItems?.find((item) => item.id === optionSelectedId)
                  ?.value || "Placeholder"
              }
              onClick={onSelectModal}
            />
          </div>

          <div className="label-input">
            <label>Scheduled Start Date</label>
            <div className="date-input-row">
              <span className="date-label">From</span>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="FROM"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={
                      scheduledFromDt && format(scheduledFromDt, "yyyy.MM.dd")
                    }
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
                <div
                  className="input-col"
                  onClick={onFirstTimeModal}
                  data-fromtotype="FROM"
                >
                  <DateTimePicker
                    timeValue={scheduledFromTime}
                    setTimeValue={setScheduledFromTime}
                    type="From"
                  />
                  {/* <IconInputClock size="24px" />
                <input type="text" value={scheduledFromTime} readOnly /> */}
                </div>
              </div>
            </div>
            <div className="date-input-row">
              <span className="date-label">To</span>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="TO"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={scheduledToDt && format(scheduledToDt, "yyyy.MM.dd")}
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
                <div
                  className="input-col"
                  onClick={onFirstTimeModal}
                  data-fromtotype="TO"
                >
                  <DateTimePicker
                    timeValue={scheduledToTime}
                    setTimeValue={setScheduledToTime}
                    type="To"
                  />
                  {/* <IconInputClock size="24px" />
                <input type="text" value={scheduledToTime} readOnly /> */}
                </div>
              </div>
            </div>
          </div>
          <div className="label-input">
            <label>MES Down Start Time</label>
            <div className="date-input-row">
              <span className="date-label">From</span>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onSecondCalendarModal}
                  data-fromtotype="FROM"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={mesDownFromDt && format(mesDownFromDt, "yyyy.MM.dd")}
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
                <div
                  className="input-col"
                  onClick={onSecondTimeModal}
                  data-fromtotype="FROM"
                >
                  <DateTimePicker
                    timeValue={mesDownFromTime}
                    setTimeValue={setMesDownFromTime}
                    type="From"
                  />
                  {/* <IconInputClock size="24px" />
                <input type="text" value={mesDownFromTime} readOnly /> */}
                </div>
              </div>
            </div>
            <div className="date-input-row">
              <span className="date-label">To</span>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onSecondCalendarModal}
                  data-fromtotype="TO"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={mesDownToDt && format(mesDownToDt, "yyyy.MM.dd")}
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
                <div
                  className="input-col"
                  onClick={onSecondTimeModal}
                  data-fromtotype="TO"
                >
                  <DateTimePicker
                    timeValue={mesDownToTime}
                    setTimeValue={setMesDownToTime}
                    type="To"
                  />
                  {/* <IconInputClock size="24px" />
                <input type="text" value={mesDownToTime} readOnly /> */}
                </div>
              </div>
            </div>
          </div>

          <div className="label-input">
            <LabelInputTypeB
              labelText="Description"
              onToggleFocus={onToggleFocus}
            />
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
        {isShowSelectBox && (
          // <WorkOrderSelectBoxModal
          //   onClick={onSelectModal}
          //   style={{ top: "143px" }}
          // />
          <DimedModal className="work-order-list-search-form-select-modal">
            <SingleSelect
              title="Option"
              items={optionSelectItems}
              onClose={onSelectModal}
              onSelect={(e) => {
                if (!e?.currentTarget?.id) return;
                // 같을 시엔 취소
                if (e.currentTarget.id === optionSelectedId) {
                  setOptionSelectedId(null);
                } else {
                  setOptionSelectedId(e.currentTarget.id);
                }
                onSelectModal();
              }}
              selectedId={optionSelectedId} //test
            />
          </DimedModal>
        )}
        {isShowFirstCalenderBox && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={scheduledFromDt}
            setFromDate={setScheduledFromDt}
            toDate={scheduledToDt}
            setToDate={setScheduledToDt}
            onClose={() => setIsShowFirstCalendarBox(false)}
          />
        )}
        {isShowSecondCalenderBox && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={mesDownFromDt}
            setFromDate={setMesDownFromDt}
            toDate={mesDownToDt}
            setToDate={setMesDownToDt}
            onClose={() => setIsShowSecondCalendarBox(false)}
          />
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetail
          type={formType}
          onClose={onInnerClose}
          initValue={searchInitValue}
        />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrderListSearchForm;
