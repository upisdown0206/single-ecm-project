import React from "react";

import { IconClosePopup, IconComment } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderCommentModal.scss";

const WorkOrderCommentModal = ({ onClose }) => {
  return (
    <div className="work-order-comment-modal">
      <div className="comment-row">
        <div className="col">
          <IconBase className="comment-icon">
            <IconComment />
          </IconBase>
          <span className="comment-modal-title">Comments</span>
        </div>
        <div className="col">
          <IconBase className="comment-icon" onClick={onClose}>
            <IconClosePopup />
          </IconBase>
        </div>
      </div>
      <div className="comment-row">
        <textarea className="comment-textarea"></textarea>
      </div>
    </div>
  );
};

export default WorkOrderCommentModal;
