import React, { useEffect, useState } from "react";
import BMWrap from "components/workOrder/BM/BMWrap";
import BMdarkDiv from "components/workOrder/BM/BMdarkDiv";
import BMIssueDetailDiv from "../BMIssueDetailDiv";

const BMSpIssueModalDetail = () => {

  return (
    <>
      <BMWrap title={"S/P Requirements"} >
        <BMdarkDiv
          nowTitle={"No. 10"}
          spNDesc={true}
        />
        <BMIssueDetailDiv />
      </BMWrap>
    </>
  );
};

export default BMSpIssueModalDetail;
