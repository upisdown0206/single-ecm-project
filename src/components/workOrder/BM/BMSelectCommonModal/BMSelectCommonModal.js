import React, {useState} from 'react';
import "./BMSelectCommonModal.scss";
import DimedModal from "../../../base/DimedModal";
import cx from "classnames";
import {IconListSelect} from "../../../common/Icons";
const BMSelectCommonModal = ({ onClick, style }) => {
  const [options, setOptions] = useState([
    {
      id: 1,
      text: "option1",
    },
    {
      id: 2,
      text: "option2",
    },
    {
      id: 3,
      text: "option3 ",
    },
  ]);
  return (
    <DimedModal style={style}>
      <div className="work-order-select-wrapper" style={{ top: "auto" }}>
        <div className="modal-select-list" style={{ marginBottom: "0" }}>
          <ul className="select-items">
            {options.map((option, idx) => (
              <li key={idx} onClick={onClick}>
                <div
                  data-id={option.id}
                  className={cx("select-item", idx === 0 && "active")}
                >
                  {option.text}
                  {idx === 0 && <IconListSelect />}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </DimedModal>
  );
};

export default BMSelectCommonModal;