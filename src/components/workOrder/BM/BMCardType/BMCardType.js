import React, { useState } from "react";

import { IconCalendarFull } from "components/common/Icons";

const BmCardType = () => {
  return (
    <div className="bm-card-list-box">
      <div className="bm-date-title">
        <span className="icon-calendar">
          <IconCalendarFull />
        </span>
        2020.08.07 (금)
      </div>

      <div className="bm-history-list">
        <dl>
          <dt>
            <span>BM</span>
            <span>10:04</span>
          </dt>
          <dd>WO-123675415:PSTART0205:7999</dd>
        </dl>
        <dl>
          <dt>
            <span>BM</span>
            <span>10:04</span>
          </dt>
          <dd>WO-123675415:PSTART0205:7999</dd>
        </dl>
      </div>

      <hr className={"line20"} />

      <div className="bm-date-title">
        <span className="icon-calendar">
          <IconCalendarFull />
        </span>
        2020.08.06 (목)
      </div>

      <div className="bm-history-list">
        <dl>
          <dt>
            <span>BM</span>
            <span>10:04</span>
          </dt>
          <dd>WO-123675415:PSTART0205:7999</dd>
        </dl>
        <dl>
          <dt>
            <span>BM</span>
            <span>10:04</span>
          </dt>
          <dd>WO-123675415:PSTART0205:7999</dd>
        </dl>
      </div>
    </div>
  );
};

export default BmCardType;
