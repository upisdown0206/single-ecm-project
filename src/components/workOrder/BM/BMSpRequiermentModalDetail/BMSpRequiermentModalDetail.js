import React, { useEffect, useState } from "react";
import BMWrap from "components/workOrder/BM/BMWrap";
import BMdarkDiv from "components/workOrder/BM/BMdarkDiv";
import BMSPDetailDiv from "../BMSPDetailDiv";

const BMSpRequiermentModalDetail = ({ onClose }) => {
  return (
    <>
      <BMWrap title={"Detail"} onClose={onClose}>
        <BMdarkDiv nowTitle={"1-1-1"} />
        <BMSPDetailDiv />
      </BMWrap>
    </>
  );
};

export default BMSpRequiermentModalDetail;
