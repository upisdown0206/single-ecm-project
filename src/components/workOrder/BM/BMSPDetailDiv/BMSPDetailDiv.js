import React, { useState } from "react";
import { format } from "date-fns";
import moment from "moment";
import cx from "classnames";

import "./BMSPDetailDiv.scss";
import FmTab from "../../../common/framerMotion/FmTab";
import LabelInputTypeC from "../../../search/LabelInputTypeC";
import LabelInputTypeE from "../../../search/LabelInputTypeE";
import Counter from "../../../common/Counter";
import LabelSelectBox from "../../LabelSelectBox";
import {
  IconSearchMedium,
  IconCalendar,
  IconClock,
  IconComment,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import RoundTag from "components/common/RoundTag";
import FixedFooter from "../../../base/FixedFooter";
import Button from "../../../common/Button";
import DateCalendarPicker from "../../../common/DateCalendarPicker";
import ScrollableModal from "../../../base/ScrollableModal";
import ModalFullPage from "../../../common/ModalFullPage";
import BMSPSearch from "../BMSPSearch";

import DimedModal from "components/base/DimedModal";
import SingleSelect from "components/common/select/SingleSelect";

const BMSPDetailDiv = ({}) => {
  const locationSelectItems = [
    {
      id: "l1",
      value: "Electorde BD 1",
    },
    {
      id: "l2",
      value: "Electorde BD 2",
    },
    {
      id: "l3",
      value: "Electorde BD 3",
    },
    {
      id: "l4",
      value: "Electorde BD 4",
    },
    {
      id: "l5",
      value: "Electorde BD 5",
    },
  ];
  const [locationSelectedId, setLocationSelectedId] = useState("l1");

  const storeRoomSelectItems = [
    {
      id: "s1",
      value: "CENTRAL",
    },
    {
      id: "s2",
      value: "CENTRAL2",
    },
    {
      id: "s3",
      value: "CENTRAL3",
    },
  ];
  const [storeRoomSelectedId, setStoreRoomSelectedId] = useState("s1");

  const bufferZoneSelectItems = [
    {
      id: "b1",
      value: "BF1",
    },
    {
      id: "b2",
      value: "BF2",
    },
    {
      id: "b3",
      value: "BF3",
    },
  ];
  const [bufferZoneSelectedId, setBufferZoneSelectedId] = useState("b1");

  const deliveryTimeSelectItems = [
    {
      id: "d1",
      value: "9 AM",
    },
    {
      id: "d2",
      value: "10 AM",
    },
    {
      id: "d3",
      value: "11 AM",
    },
    {
      id: "d4",
      value: "12 AM",
    },
    {
      id: "d5",
      value: "1 PM",
    },
  ];
  const [deliveryTimeSelectedId, setDeliveryTimeSelectedId] = useState("d1");

  const [keywords, setKeywords] = useState([
    {
      id: "1",
      type: "spType",
      text: "BMBOLT0003 Bolt",
      subText: "Bolt, Mobile,A109K-4000HBolt,Mo bile,A109K-4000H",
    },
  ]);
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const onInputClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { type },
    } = currentTarget;
  };

  const [isSPSearch, setIsSPSearch] = useState(false);

  const [isShowCalenderBox, setisShowCalendarBox] = useState(false);
  const onCalendarModal = () => {
    setisShowCalendarBox(!isShowCalenderBox);
  };

  const [scheduledFromDt, setScheduledFromDt] = useState("");
  const [scheduledToDt, setScheduledToDt] = useState("");
  const [isShowFirstCalenderBox, setIsShowFirstCalendarBox] = useState(false);
  const [fromToType, setFromToType] = useState("FROM");
  const onFirstCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
  };

  // const [isRoundTagToggle, setRoundTagToggle] = useState(false);
  // const onRoundTagToggle = (e) => {
  //   setRoundTagToggle(!isRoundTagToggle);
  // };

  const [isShowSelectBox, setIsShowSelectBox] = useState(false);
  const onSelectModal = () => {
    setIsShowSelectBox(!isShowSelectBox);
  };

  const [mode, setMode] = useState("");

  const [isHistoryView, setIsHistoryView] = useState(false);
  const onHistoryView = () => {
    setIsHistoryView(!isHistoryView);
  };

  const onCancelSelect = (e) => {
    setIsHistoryView(false);
  };

  const [isDeliveryType, setDeliveryType] = useState("deliveryChoice");

  return (
    <div className="bmsp-wrap">
      <FmTab />

      <div className="bmsp-contents">
        <div className="label-input">
          <LabelInputTypeE
            labelText="SP"
            keywords={keywords}
            setKeywords={setKeywords}
            searchType="spType"
            onInputClick={onInputClick}
            necessary={<span className={"necessary-dot"}></span>}
          />

          <div className="label-input-else">
            <label>
              Issue Qty<span className={"necessary-dot"}></span>
            </label>
            <Counter type="typeWide" size={"12px"} />
          </div>

          <div className="flex-div2">
            <div className="label-input">
              <LabelSelectBox
                labelText="Central Storeroom"
                onClick={() => setMode("SELECT/STOREROOM")}
                defaultText={
                  storeRoomSelectItems.find(
                    (item) => item.id === storeRoomSelectedId
                  )?.value || ""
                }
              />
            </div>

            <div className="label-input-wrap">
              <label>Available Qty</label>
              <input type="text" value="10" disabled="disabled" />
            </div>
          </div>

          <div className="flex-div2">
            <div className="label-input">
              <LabelSelectBox
                labelText="Buffer Zone"
                onClick={() => setMode("SELECT/BUFFERZONE")}
                defaultText={
                  bufferZoneSelectItems.find(
                    (item) => item.id === bufferZoneSelectedId
                  )?.value || ""
                }
              />
            </div>

            <div className="label-input-wrap">
              <label>Available Qty</label>
              <input type="text" value="10" disabled="disabled" />
            </div>
          </div>

          <div className="flex-div2">
            <div className="label-input" onClick={onCalendarModal}>
              <label>Required Date</label>
              <div className="calendar-fromto-type">
                <div
                  className={cx("date-calendar-input", "wid100")}
                  onClick={onFirstCalendarModal}
                  data-fromtotype="FROM"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={
                      scheduledFromDt && format(scheduledFromDt, "yyyy.MM.dd")
                    }
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
              </div>
            </div>

            <div className="label-input">
              <LabelSelectBox
                labelText="Delivery Time"
                LeftIconType={
                  <IconBase>
                    <IconClock />
                  </IconBase>
                }
                onClick={() => setMode("SELECT/DELIVERYTIME")}
                defaultText={
                  deliveryTimeSelectItems.find(
                    (item) => item.id === deliveryTimeSelectedId
                  )?.value || ""
                }
              />
            </div>
          </div>

          <div className="label-input">
            <LabelSelectBox
              labelText="Delivery Location"
              onClick={() => setMode("SELECT/STATUS")}
              defaultText={
                locationSelectItems.find(
                  (item) => item.id === locationSelectedId
                )?.value || ""
              }
            />
          </div>

          <div className="label-input">
            <label>Delivery Type</label>
            <div className="filter-round div3">
              <RoundTag
                tagName={"Deliver"}
                onClick={() => setDeliveryType("delivery")}
                active={isDeliveryType === "delivery" && "active"}
              />
              <RoundTag
                tagName={"Direct"}
                onClick={() => setDeliveryType("direct")}
                active={`${isDeliveryType === "direct" && "active"}`}
              />
            </div>
          </div>
        </div>

        <hr className="border3" />

        <LabelInputTypeC
          labelText="Comment"
          keywords={keywords}
          setKeywords={setKeywords}
          keywordType="equipmentId"
          onInputClick={onInputClick}
          setKeyboardFocus={setIsKeyboardFocus}
          iconType={<IconComment />}
        />
      </div>

      <FixedFooter>
        <div className="bottom-btn-wrap div2">
          <Button color="gray" outline onClick={onHistoryView}>
            Select EBOM
          </Button>
          <Button color="gray">Save</Button>
        </div>
      </FixedFooter>

      {isShowFirstCalenderBox && (
        <DateCalendarPicker
          fromToType={fromToType}
          fromDate={scheduledFromDt}
          setFromDate={setScheduledFromDt}
          toDate={scheduledToDt}
          setToDate={setScheduledToDt}
          onClose={() => setIsShowFirstCalendarBox(false)}
        />
      )}

      {mode === "SELECT/STOREROOM" && (
        <DimedModal>
          <SingleSelect
            title="Store Room"
            items={storeRoomSelectItems}
            onClose={() => setMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === storeRoomSelectedId) {
                setStoreRoomSelectedId(null);
              } else {
                setStoreRoomSelectedId(e.currentTarget.id);
              }
              setMode("");
            }}
            selectedId={storeRoomSelectedId} //test
          />
        </DimedModal>
      )}
      {mode === "SELECT/BUFFERZONE" && (
        <DimedModal>
          <SingleSelect
            title="Buffer Zone"
            items={bufferZoneSelectItems}
            onClose={() => setMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === bufferZoneSelectedId) {
                setBufferZoneSelectedId(null);
              } else {
                setBufferZoneSelectedId(e.currentTarget.id);
              }
              setMode("");
            }}
            selectedId={bufferZoneSelectedId} //test
          />
        </DimedModal>
      )}
      {mode === "SELECT/DELIVERYTIME" && (
        <DimedModal>
          <SingleSelect
            title="Delivery Time"
            items={deliveryTimeSelectItems}
            onClose={() => setMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === deliveryTimeSelectedId) {
                setDeliveryTimeSelectedId(null);
              } else {
                setDeliveryTimeSelectedId(e.currentTarget.id);
              }
              setMode("");
            }}
            selectedId={deliveryTimeSelectedId} //test
          />
        </DimedModal>
      )}
      {mode === "SELECT/STATUS" && (
        <DimedModal>
          <SingleSelect
            title="Status"
            items={locationSelectItems}
            onClose={() => setMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === locationSelectedId) {
                setLocationSelectedId(null);
              } else {
                setLocationSelectedId(e.currentTarget.id);
              }
              setMode("");
            }}
            selectedId={locationSelectedId} //test
          />
        </DimedModal>
      )}

      {isHistoryView && (
        <ScrollableModal onTouchHandler={onCancelSelect} isDimmed={true}>
          <div className="activity-list">
            <div className="al_head--alter">
              <div className="filter-input-wrapper">
                <IconBase className="icon-left">
                  <IconSearchMedium />
                </IconBase>
                <input
                  type="text"
                  className="filter-input"
                  placeholder="Search"
                />
              </div>
            </div>

            <div className="sp-history-wrap">
              <ul className="sp-history-items">
                <li className="sp-history-item" onClick={onHistoryView}>
                  <div className="sphi-title">
                    <div className="tit-txt">
                      BMBOLT0003
                      <div className="count-circle-alter">
                        <div className="circle-text-alter">
                          Qty.<span className="detail-circle-value">3</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="sphi-content">
                    <div className="content__description">
                      Bolt, Mobile, A109K-4000H
                    </div>
                    <div className="eqpt-desc-box">
                      <div className="eqpt-box__row">
                        <div className="label">Equipment Id</div>
                        <div className="content">P8TCVD9097856654</div>
                      </div>
                      <div className="eqpt-box__row">
                        <div className="label">Description</div>
                        <div className="content">
                          Nozzle ** 9344-100-162, UNISEM
                        </div>
                      </div>
                    </div>
                    <div className="issue__description">
                      <div className="label">Last Issue</div>
                      <div className="content">
                        <span className="date">2020.05.01</span>
                        <span className="time">12:30</span>
                      </div>
                    </div>
                  </div>
                </li>

                <li className="sp-history-item" onClick={onHistoryView}>
                  <div className="sphi-title">
                    <div className="tit-txt">
                      BMBOLT0003
                      <div className="count-circle-alter">
                        <div className="circle-text-alter">
                          Qty.<span className="detail-circle-value">3</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="sphi-content">
                    <div className="content__description">
                      Bolt, Mobile, A109K-4000H
                    </div>
                    <div className="eqpt-desc-box">
                      <div className="eqpt-box__row">
                        <div className="label">Equipment Id</div>
                        <div className="content">P8TCVD9097856654</div>
                      </div>
                      <div className="eqpt-box__row">
                        <div className="label">Description</div>
                        <div className="content">
                          Nozzle ** 9344-100-162, UNISEM
                        </div>
                      </div>
                    </div>
                    <div className="issue__description">
                      <div className="label">Last Issue</div>
                      <div className="content">
                        <span className="date">2020.05.01</span>
                        <span className="time">12:30</span>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="sp-history-item" onClick={onHistoryView}>
                  <div className="sphi-title">
                    <div className="tit-txt">
                      BMBOLT0003
                      <div className="count-circle-alter">
                        <div className="circle-text-alter">
                          Qty.<span className="detail-circle-value">3</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="sphi-content">
                    <div className="content__description">
                      Bolt, Mobile, A109K-4000H
                    </div>
                    <div className="eqpt-desc-box">
                      <div className="eqpt-box__row">
                        <div className="label">Equipment Id</div>
                        <div className="content">P8TCVD9097856654</div>
                      </div>
                      <div className="eqpt-box__row">
                        <div className="label">Description</div>
                        <div className="content">
                          Nozzle ** 9344-100-162, UNISEM
                        </div>
                      </div>
                    </div>
                    <div className="issue__description">
                      <div className="label">Last Issue</div>
                      <div className="content">
                        <span className="date">2020.05.01</span>
                        <span className="time">12:30</span>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="sp-history-item" onClick={onHistoryView}>
                  <div className="sphi-title">
                    <div className="tit-txt">
                      BMBOLT0003
                      <div className="count-circle-alter">
                        <div className="circle-text-alter">
                          Qty.<span className="detail-circle-value">3</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="sphi-content">
                    <div className="content__description">
                      Bolt, Mobile, A109K-4000H
                    </div>
                    <div className="eqpt-desc-box">
                      <div className="eqpt-box__row">
                        <div className="label">Equipment Id</div>
                        <div className="content">P8TCVD9097856654</div>
                      </div>
                      <div className="eqpt-box__row">
                        <div className="label">Description</div>
                        <div className="content">
                          Nozzle ** 9344-100-162, UNISEM
                        </div>
                      </div>
                    </div>
                    <div className="issue__description">
                      <div className="label">Last Issue</div>
                      <div className="content">
                        <span className="date">2020.05.01</span>
                        <span className="time">12:30</span>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="sp-history-item" onClick={onHistoryView}>
                  <div className="sphi-title">
                    <div className="tit-txt">
                      BMBOLT0003
                      <div className="count-circle-alter">
                        <div className="circle-text-alter">
                          Qty.<span className="detail-circle-value">3</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="sphi-content">
                    <div className="content__description">
                      Bolt, Mobile, A109K-4000H
                    </div>
                    <div className="eqpt-desc-box">
                      <div className="eqpt-box__row">
                        <div className="label">Equipment Id</div>
                        <div className="content">P8TCVD9097856654</div>
                      </div>
                      <div className="eqpt-box__row">
                        <div className="label">Description</div>
                        <div className="content">
                          Nozzle ** 9344-100-162, UNISEM
                        </div>
                      </div>
                    </div>
                    <div className="issue__description">
                      <div className="label">Last Issue</div>
                      <div className="content">
                        <span className="date">2020.05.01</span>
                        <span className="time">12:30</span>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </ScrollableModal>
      )}

      {isSPSearch && (
        <ModalFullPage motion="vertical">
          <BMSPSearch />
        </ModalFullPage>
      )}
    </div>
  );
};

export default BMSPDetailDiv;
