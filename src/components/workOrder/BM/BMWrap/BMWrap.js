import React, {useState} from "react";
import "../WorkOrderBM.scss";

import { IconClosePopup } from "components/common/Icons";

const BMWrap = ({ title, icon, children, onClose }) => {
  const [mode, setMode] = useState("init");
  const [popupMode, setPopupMode] = useState("");
  const onInnerClose = () => {
    setMode("");
  };
  return (
    <div className="bm-operation-wrap">
      <div className="bm-operation-header">
        <button className="btn-bm-close" onClick={onClose}>
          <IconClosePopup strokeFill={"#ffffff"} />
        </button>
        <h1>
          {icon}
          {title}
        </h1>
      </div>
      {children}
    </div>
  );
};

export default BMWrap;
