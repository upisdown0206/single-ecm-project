import React, { useState } from "react";
import cx from "classnames";
import DimedModal from "components/base/DimedModal";
import {IconClosePopup, IconListSelect} from "components/common/Icons";
import "./BMSpRequiermentModalCheckType.scss";

const BMSpRequiermentModalCheckType = ({ onClick, style }) => {
  const [options, setOptions] = useState([
    {
      id: 1,
      text: "직접 추가하기",
    },
    {
      id: 2,
      text: "SP History 복사하기",
    },
  ]);

  return (
    <DimedModal style={style}>
      <div className="work-order-select-wrapper" style={{ top: "auto" }}>
        <div className="modal-header">
          <h1>새항목추가</h1>
          <button onClick={onClick}><IconClosePopup /></button>
        </div>
        <div className="modal-select-list" style={{ marginBottom: "0" }}>
          <ul className="select-items">
            {options.map((option, idx) => (
              <li key={idx} onClick={onClick}>
                <div
                  data-id={option.id}
                  className={cx("select-item", idx === 0 && "active")}
                >
                  {option.text}
                  {idx === 0 && <IconListSelect />}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </DimedModal>
  );
};

export default BMSpRequiermentModalCheckType;
