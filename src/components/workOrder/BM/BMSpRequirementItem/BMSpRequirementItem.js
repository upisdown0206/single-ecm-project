import React, {useState} from "react";
import "./BMSpRequirementItem.scss";
import classnames from 'classnames'
import WorkOrderToDoItemTypeA from "components/workOrder/WorkOrderToDoItemTypeA";
import {IconInventoryMovement, IconLocation} from "../../../common/Icons";

const BMSpRequirementItem = ({isDeleteAdd,onSelectClick,onClick, }) => {
  const [todoItems, setTodoItems] = useState([
    {
      id: 1,
      title: "1-1-1",
      text: "BMM12345",
      location:"central central centralcentralcentral",
      value:"Bolt, Mobile, A109K-4000H",
      ItemQty: "000",
      ReservedQty: 992,
      delivery: false,
      deliveryNumber: false,
      Qty:true,
      isChecked: false,
    },
    {
      id: 2,
      title: "10",
      text: "BMM12345",
      location:"central ",
      value:"Bolt, Mobile, A109K-4000H",
      ItemQty: 1,
      ReservedQty: 1,
      delivery: false,
      deliveryNumber: false,
      Qty:true,
      isChecked: false,
    },
    {
      id: 3,
      title: "1-1-1",
      text: "BMM12345",
      location:"central ",
      value:"Bolt, Mobile, A109K-4000H",
      ItemQty: 1,
      ReservedQty: 1,
      delivery: "Entered",
      deliveryNumber: "No.1234",
      isChecked: false,
      Qty:false,
    },
  ]);

  const [isCheckClick, setCheckClick] = useState(false)
  const onToDoCheckClick = (id) => {

    const newItems = todoItems.map((item) =>
      item.id === id ? { ...item, isChecked: !item.isChecked } : item
    );

    setTodoItems(newItems);
  };

  const [isSelectChekDetailType, setSelectChekDetailType] = useState(false);
  const onSelectChekDetailType = () => {
    setSelectChekDetailType(!isSelectChekDetailType);
  }

  return <div className="list-group"  >
    {
      todoItems.map((item) => (
        <div key={item.id}
         className={classnames("sp-requirements-card",(item.isChecked && "active"),  isDeleteAdd && "on")}
             onClick={onSelectChekDetailType}
        >
          <div className="list-group-item" onClick={onClick}>
            <div className="list-group-header">
              <div className="left-col">
                <span className="title">{item.title}</span>
                <span className="text">{item.text}</span>
              </div>
              <div className="location"> <IconLocation width="16px" height="16px" fill="#E0205C"/>{item.location}</div>
            </div>
            <div className="value">
              {item.value}
            </div>

            <div className="list-number-wrap">
              <span className="number-title">Item Qty</span>
              <span className="number-text">{item.ItemQty}</span>
              <span className="line">|</span>
              <span className="number-title">Reserved Qty</span>
              <span className="number-text">{item.ReservedQty}</span>
            </div>

            {item.delivery && (
              <div className="delivery-box">
                <IconInventoryMovement fill1="#ffffff" fill2="#ffffff" width="19px" height="18px" />
                {item.deliveryNumber}
                <span className="line">|</span>
                {item.delivery}
              </div>
            )}

          </div>

          { isDeleteAdd &&
            <WorkOrderToDoItemTypeA
              isCheck={item.isChecked}
              onToDoCheckClick={() => onToDoCheckClick(item.id)}
              onSelectClick={onSelectClick}
            />
          }

        </div>
      ))
    }
  </div>;
};

export default BMSpRequirementItem;
