import React, { useState } from "react";
import { format } from "date-fns";
import moment from "moment";

import cx from "classnames";

import DateTimePicker from "components/common/DateTimePicker";
import {
  IconPlus,
  IconMinus,
  IconMainReleaseFull,
  IconCheck,
  IconSearchInput,
  IconCalInput,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import useDelayUnmount from "lib/hooks/useDelayUnmount";
import LabelInputTypeE from "components/search/LabelInputTypeE";
import DateTargetPicker from "components/common/DateTargetPicker";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";

import LabelInputCircleRadio from "components/workOrder/LabelInputCircleRadio";

import "./WorkOrderCheckResultInsert.scss";

const WorkOrderCheckResultInsert = ({
  className,
  itemIdx,
  itemName,
  itemSelected,
  itemPoint,
  children,
}) => {
  const [mode, setMode] = useState("init");
  const [isExpand, setIsExpand] = useState(false);

  const [keywords, setKeywords] = useState([
    {
      id: "1",
      type: "activity",
      text: "BMBOLT0003 Bolt",
      subText: "Bolt, Mobile,A109K-4000HBolt,Mo bile,A109K-4000H",
    },
  ]);

  const result = [
    {
      id: "ok",
      value: "OK",
    },
    {
      id: "ngAdjust",
      value: "NG-ADJUST",
    },
    {
      id: "ngWo",
      value: "NG-WO",
    },
  ];

  const onClick = () => {
    setIsExpand(!isExpand);
  };

  const [isShowCalBox, setIsShowCalBox] = useState(false);
  const [targetDt, setTargetDt] = useState("");

  const onCalClick = () => {
    setIsShowCalBox(!isShowCalBox);
  };

  const [targetTime, setTargetTime] = useState(moment("00:00", "HH:mm"));

  const [searchInitValue, setSearchInitValue] = useState("");
  const [formType, setFormType] = useState("");
  const onInputClick = (e, type, initValue = "") => {
    // render form-detail (type)
    setMode("FORM_DETAIL");
    setFormType(type);
    setSearchInitValue(initValue);
  };

  const [inputValue, setInputValue] = useState("");
  const [commentValue, setCommentValue] = useState("");

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    setInputValue(value);
  };
  const onCommentChange = (e) => {
    const {
      target: { value },
    } = e;
    setCommentValue(value);
  };
  const shouldRender = useDelayUnmount(isExpand, 300);

  const [selectedId, setSelectedId] = useState("ok");
  return (
    <>
      <div
        className={cx(
          "work-order-check-result-insert",
          isExpand && "expand",
          className
        )}
      >
        <div className="check-result-item-header" onClick={onClick}>
          <div className="col">
            <span className="item-index">{itemIdx}</span>
            <span className={cx("item-name", !isExpand && "line-ellipse")}>
              {itemName}
            </span>
          </div>

          {itemSelected ? (
            <div className="col">
              {isExpand ? (
                <IconMinus size="20" fill="#A5B3C2" />
              ) : (
                <>
                  <IconBase>
                    <IconMainReleaseFull
                      fill="#26B9D1"
                      width="21"
                      height="21"
                    />
                  </IconBase>
                  <span className="selected-text">선택</span>
                </>
              )}
            </div>
          ) : (
            <div className="col">
              {!isExpand && <span className="expand-text">더보기</span>}
              <IconBase>
                {isExpand ? (
                  <div className="flex-box">
                    <div className="circle-point">
                      <span className="point">{itemPoint}</span>
                      <span className="unit">P</span>
                    </div>
                    <IconMinus size="20" fill="#A5B3C2" />
                  </div>
                ) : (
                  <IconPlus size="20" fill="#A5B3C2" />
                )}
              </IconBase>
            </div>
          )}
        </div>
        {shouldRender && (
          <div
            className={cx(
              "check-result-item-content",
              isExpand ? "expand" : "collapse"
            )}
          >
            <div className="spliter mt-none" />
            {children}
            <div className="spliter" />
            <div className="check-result-labeled flex-col">
              <span className="label">Value</span>
              <input
                className="input-content"
                type="text"
                placeholder="Value"
                value={inputValue}
                onChange={onChange}
                style={{ marginTop: "6px" }}
              />
            </div>
            <div className="check-result-labeled flex-col">
              <span className="label">Measure Result</span>
              <LabelInputCircleRadio
                labelText=""
                items={result}
                radioGroupName={`selected${itemIdx}`}
                selectedId={selectedId}
                setSelectedId={setSelectedId}
              />
            </div>
            {selectedId === "ngWo" && (
              <div className="check-result-labeled flex-col bg-dark">
                <span className="label">Activity</span>
                <LabelInputTypeE
                  labelText=""
                  keywords={keywords}
                  setKeywords={setKeywords}
                  searchType="activity"
                  onInputClick={onInputClick}
                />
              </div>
            )}
            <div className="check-result-labeled flex-col">
              <span className="label">Measured Date</span>
              <div className="flex-wrapper">
                <div className="input-wrapper" onClick={onCalClick}>
                  <input
                    className="input-content"
                    type="text"
                    placeholder="YYYY.MM.DD"
                    value={targetDt && format(targetDt, "yyyy.MM.dd")}
                    readOnly
                  />
                  <IconBase className="ab-left">
                    <IconCalInput />
                  </IconBase>
                </div>
                <DateTimePicker
                  timeValue={targetTime}
                  setTimeValue={setTargetTime}
                  style={{ background: "white", marginLeft: "8px" }}
                  type="Target"
                />
              </div>
            </div>
            <div className="check-result-labeled flex-col">
              <span className="label">Comments</span>
              <textarea
                className="input-content"
                placeholder="Placeholder"
                value={commentValue}
                onChange={onCommentChange}
                style={{ marginTop: "6px" }}
              />
            </div>
          </div>
        )}
      </div>
      {isShowCalBox && (
        <DateTargetPicker
          inDt={targetDt}
          setToDate={setTargetDt}
          onClose={() => setIsShowCalBox(false)}
        />
      )}
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={() => setMode("")}
          initValue={searchInitValue}
        />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrderCheckResultInsert;
