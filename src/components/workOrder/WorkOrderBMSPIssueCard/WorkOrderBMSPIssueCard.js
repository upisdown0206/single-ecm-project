import React, { useState } from "react";
import cx from "classnames";

import IconBase from "components/common/IconBase";
import { IconLocation } from "components/common/Icons";

import "./WorkOrderBMSPIssueCard.scss";

const data = [{}];

const WorkOrderBMSPIssueCard = ({
  idx,
  id,
  location,
  status,
  description,
  reservedQty,
  remainQty,
  issueQty,
  toBFQty,
  toStoreroomQty,
  toSupplierQty,
  onFocus,
  onBlur,
}) => {
  const [qty, setQty] = useState({
    issueQty: 3,
    toBFQty: 3,
    toStoreRoomQty: 3,
    toSupplierQty: 3,
  });

  const onChange = (e) => {};
  return (
    <div className="work-order-bm-issue-card">
      <div className="row spb">
        <div className="col max-width spb">
          <div className="col">
            <div className="no-text">{idx}</div>
            <div className="no-id">{id}</div>
          </div>
          <div className="location">
            <IconBase className="location-icon">
              <IconLocation width="16" height="16" />
            </IconBase>
            <div className="location-text">{location}</div>
          </div>
        </div>
        <div className="col">
          <div className="rounded-text">{status}</div>
        </div>
      </div>
      <div className="row">
        <div className="desc">{description}</div>
      </div>
      <div className="row">
        <div className="sub-label">Reserved Qty</div>
        <div className="primary-text">992</div>
        <div className="divider" />
        <div className="sub-label">Remain Qty</div>
        <div className="primary-text">000</div>
      </div>
      <div className="row">
        <div className="col max-width">
          <div className="sub-label max-width">Issue QTY</div>
          <input
            className={cx("number-input")}
            type="number"
            value={Number(qty.issueQty).toString()}
            onChange={(e) => {
              const {
                target: { value },
              } = e;
              if (!value) {
                setQty({
                  ...qty,
                  issueQty: "",
                });
                return;
              }
              setQty({
                ...qty,
                issueQty: parseInt(value, 10),
              });
            }}
            placeholder="0"
            onFocus={onFocus}
            onBlur={onBlur}
            onClick={(e) => e.stopPropagation()}
          />
        </div>
        <div className="col max-width">
          <div style={{ marginLeft: "9px" }} className="sub-label max-width">
            To BF
          </div>
          <input
            className={cx("number-input")}
            type="number"
            value={Number(qty.toBFQty).toString()}
            onChange={(e) => {
              const {
                target: { value },
              } = e;
              if (!value) {
                setQty({
                  ...qty,
                  toBFQty: "",
                });
                return;
              }
              setQty({
                ...qty,
                toBFQty: parseInt(value, 10),
              });
            }}
            placeholder="0"
            onFocus={onFocus}
            onBlur={onBlur}
            onClick={(e) => e.stopPropagation()}
          />
        </div>
      </div>
      <div className="row">
        <div className="col max-width">
          <div className="sub-label max-width">To Storeroom</div>
          <input
            className={cx("number-input")}
            type="number"
            value={Number(qty.toStoreRoomQty).toString()}
            onChange={(e) => {
              const {
                target: { value },
              } = e;
              if (!value) {
                setQty({
                  ...qty,
                  toStoreRoomQty: "",
                });
                return;
              }
              setQty({
                ...qty,
                toStoreRoomQty: parseInt(value, 10),
              });
            }}
            placeholder="0"
            onFocus={onFocus}
            onBlur={onBlur}
            onClick={(e) => e.stopPropagation()}
          />
        </div>
        <div className="col max-width">
          <div style={{ marginLeft: "9px" }} className="sub-label max-width">
            To Supplier
          </div>
          <input
            className={cx("number-input")}
            type="number"
            value={Number(qty.toSupplierQty).toString()}
            onChange={(e) => {
              const {
                target: { value },
              } = e;
              if (!value) {
                setQty({
                  ...qty,
                  toSupplierQty: "",
                });
                return;
              }
              setQty({
                ...qty,
                toSupplierQty: parseInt(value, 10),
              });
            }}
            placeholder="0"
            onFocus={onFocus}
            onBlur={onBlur}
            onClick={(e) => e.stopPropagation()}
          />
        </div>
      </div>
    </div>
  );
};

export default WorkOrderBMSPIssueCard;
