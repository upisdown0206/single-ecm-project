import React from "react";
import cx from "classnames";

import ActualStateDisplay from "components/common/ActualStateDisplay";

import { IconLight } from "components/common/Icons";

import "./WorkOrderResultCardTypeA.scss";

const WorkOrderResultCardTypeA = ({
  progressState,
  woType,
  woNo,
  equipmentId,
  description,
  isActualStarted,
  ...rest
}) => {
  return (
    <div
      className={cx(
        "work-order-result-card-typeA",
        woType === "BM" ? "bm-type" : "pm-type"
      )}
      {...rest}
    >
      <div className="item-state-type">
        <span className="state">{progressState}</span>
        <div className="type">
          {woType === "BM" && (
            <IconLight width="12" height="12" fill="#A5B3C2" />
          )}
          <span>{woType}</span>
        </div>
      </div>
      <dl className="item-title">
        <dt>WO No</dt>
        <dd>{woNo}</dd>
      </dl>
      <div className="item-text">
        <dl>
          <dt>Equipment ID</dt>
          <dd>{equipmentId}</dd>
        </dl>
        <dl>
          <dt>Description</dt>
          <dd>{description}</dd>
        </dl>
      </div>
      <ActualStateDisplay
        className="ab-right-bottom"
        isStarted={isActualStarted}
      />
    </div>
  );
};

export default WorkOrderResultCardTypeA;
