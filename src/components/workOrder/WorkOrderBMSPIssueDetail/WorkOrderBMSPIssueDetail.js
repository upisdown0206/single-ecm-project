import React, { useState } from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import HeaderTitle from "components/common/HeaderTitle";
import CircleText from "components/common/CircleText";

import WorkOrderBMSPIssueItemPanel from "components/workOrder/WorkOrderBMSPIssueItemPanel";
import SwitchButtonAlter from "components/common/SwitchButtonAlter";

import {
  IconClosePopup,
  IconLocation,
  IconTrashCan,
  IconRoundPlus,
  IconRoundClose,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import { useSnackbar } from "notistack";

import "./WorkOrderBMSPIssueDetail.scss";

const data = [
  {
    id: "BMM12345",
    no: 10,
    spId: "BMBOLT0003",
    location: "BF 1",
    status: "NEW",
    description: "Bolt, Mobile, A109K-4000H",
    issueList: [
      {
        qty: {
          repairQty: 0,
          discardQty: 0,
          notDeassignQty: 0,
          unlQty: 0,
          additionalQty: 0,
        },
        equipmentId: "P7TCVD01-0101",
        serialNoIn: null,
        serialNoOut: null,
        replacementCategory: null,
        reasonForReplacement: null,
        comment: null,
      },
    ],
  },
];

const WorkOrderBMSPIssueDetail = ({ selectedId, onBackClick }) => {
  const [deleteToggle, setDeleteToggle] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = (equipmentId = "Equipment Id") => {
    enqueueSnackbar(`'${equipmentId}'가 삭제되었습니다.`, {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 80 }}
          onClick={() => closeSnackbar(key)}
        >
          <span>{message}</span>
        </div>
      ),
    });
  };

  return (
    <div className="work-order-bm-sp-issue-detail">
      <FixedHeader
        left={
          <IconBase onClick={onBackClick}>
            <IconClosePopup fill="#ffffff" bgFill="#424952" />
          </IconBase>
        }
        center={<HeaderTitle text={`Detail`} blackMode={true} />}
        blackMode={true}
      />

      <main
        className="detail-main"
        style={{
          height: `calc(100vh - 56px - 88px)`,
          marginTop: `56px`,
          overflowY: `auto`,
        }}
      >
        <div className="sticky-header">
          <div className="title">
            <div className="text">No. 10</div>
          </div>
          <div className="row flex-spb">
            <div className="col">
              <div className="label">S/P</div>
              <div className="value">BMBOLT0003</div>
            </div>
            <div className="col">
              <div className="location">
                <IconBase className="location-icon">
                  <IconLocation width="18" height="18" />
                </IconBase>
                <div className="location-text">BF1</div>
                <div className="rounded-text">NEW</div>
              </div>
            </div>
          </div>
          <div className="row flex-spb">
            <div className="col">
              <div className="label">Desc</div>
              <div className="value">Bolt, Mobile, A109K-4000H</div>
            </div>
          </div>
        </div>
        <div className="content-wrapper">
          <div className="row flex-spb">
            <div className="col">
              <div className="label">Issue</div>
              <CircleText text="1" />
            </div>
            <div className="col">
              <div
                className="toggle-delete"
                onClick={() => setDeleteToggle(!deleteToggle)}
              >
                <IconBase className="delete-icon">
                  <IconTrashCan />
                </IconBase>
                <SwitchButtonAlter isOn={deleteToggle} />
              </div>
            </div>
          </div>
          <div className="row" style={{ marginTop: "10px" }}>
            <div className="item-box add-item">
              <IconBase className="add-icon">
                <IconRoundPlus />
              </IconBase>
              <div className="item-text">Add</div>
            </div>
            <ul className="issue-items">
              <li className="issue-item">
                <div className={cx("item-box", "active")}>
                  <div className={cx("circle-title", "none-sn")}>Qty 0</div>
                  <div className="item-text">Equipment Id</div>
                  {deleteToggle && (
                    <div className="ab-right">
                      <IconBase
                        className="delete-icon"
                        onClick={() => onShowMessage()}
                      >
                        <IconRoundClose />
                      </IconBase>
                    </div>
                  )}
                </div>
              </li>
              <li className="issue-item">
                <div className="item-box">
                  <div className={cx("circle-title")}>Qty 10</div>
                  <div className="item-text">P7TCVD01-0101</div>
                  {deleteToggle && (
                    <div className="ab-right">
                      <IconBase
                        className="delete-icon"
                        onClick={() => onShowMessage("P7TCVD01-0101")}
                      >
                        <IconRoundClose />
                      </IconBase>
                    </div>
                  )}
                </div>
              </li>
              <li className="issue-item">
                <div className={cx("item-box", "saved")}>
                  <div className={cx("circle-title")}>Qty 10</div>
                  <div className="item-text">P7TCVD01-0101</div>
                </div>
              </li>
              <li className="issue-item">
                <div className={cx("item-box", "saved")}>
                  <div className={cx("circle-title")}>Qty 10</div>
                  <div className="item-text">P7TCVD01-0101</div>
                </div>
              </li>
            </ul>
          </div>
          <WorkOrderBMSPIssueItemPanel style={{ marginTop: "28px" }} />
        </div>
      </main>
      <FixedFooter className="footer-white">
        <div className={cx("primary-button", "active")}>Save</div>
      </FixedFooter>
    </div>
  );
};

export default WorkOrderBMSPIssueDetail;
