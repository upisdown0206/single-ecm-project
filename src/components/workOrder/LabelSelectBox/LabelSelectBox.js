import React from "react";

import { IconArrowDownInput, IconClock } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./LabelSelectBox.scss";

const LabelSelectBox = ({
  labelText,
  onClick,
  LeftIconType,
  defaultText = "",
  placeholder = "Text",
}) => {
  return (
    <div className="label-select-box">
      <label>{labelText}</label>
      <div className="select-dropdown-popup" onClick={onClick}>
        <div>
          {LeftIconType}
          <button className="select-text">
            {defaultText || <span className="placeholder">{placeholder}</span>}
          </button>
        </div>
        <IconBase>
          <IconArrowDownInput />
        </IconBase>
      </div>
    </div>
  );
};

export default LabelSelectBox;
