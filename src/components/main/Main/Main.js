import React, { useState } from "react";
import cx from "classnames";

import MenuNav from "components/base/MenuNav";
import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";

import {
  IconInventory,
  IconSearchEqpt,
  IconDailyCheck,
  IconDailyInspection,
  IconWorkOrder,
  IconRelease,
  IconInventoryCount,
  IconReturn,
} from "components/common/Icons";
import AppNavigation from "components/common/AppNavigation";
import HeaderTitle from "components/common/HeaderTitle";
import MainMenuButton from "components/main/MainMenuButton";
import MainUserCard from "components/main/MainUserCard";
import "./Main.scss";

const Main = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const onMenuClick = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  return (
    <>
      <div className="main-wrapper">
        <FixedHeader center={<HeaderTitle text="Home" />} />

        <main className={cx("main", isMenuOpen && "modal-open")}>
          {isMenuOpen && <MenuNav onMenuClick={onMenuClick} />}
          <div className="main-user">
            <MainUserCard firstName="홍" lastName="길동" staffRank="사원" />
          </div>
          <div className="main-menu-buttons">
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconSearchEqpt width="64" height="64" />}
                text="WO"
                linkUrl="/workorder"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconDailyInspection width="64" height="64" />}
                text="Daily Check"
                linkUrl="/daily-check"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconDailyCheck width="64" height="64" />}
                text="Spare Parts"
                linkUrl="/spareparts"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconInventory width="64" height="64" />}
                text="Inventory"
                linkUrl="/inventory"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconWorkOrder width="64" height="64" />}
                text="Equipment"
                linkUrl="/equipment"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconRelease width="64" height="64" />}
                text="Delivery "
                linkUrl="/delivery"
                className="menu-button small"
              />
              {/*/!* else : IconBigNone1~IconBigNone5 *!/*/}
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconInventoryCount width="64" height="64" />}
                text="Stocktaking"
                linkUrl="/stock-taking"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconReturn width="64" height="64" />}
                text="Return"
                linkUrl="/return"
                className="menu-button small"
              />
              {/*/!* else : IconBigNone1~IconBigNone5 *!/*/}
            </div>
          </div>
        </main>
        {!isMenuOpen && (
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        )}
      </div>
    </>
  );
};

export default Main;
