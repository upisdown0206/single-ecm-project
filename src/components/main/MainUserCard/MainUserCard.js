import React from "react";

import "./MainUserCard.scss";

const MainUserCard = ({ firstName, lastName, staffRank }) => {
  return (
    <div className="main-user-card">
      <div className="card-col">
        <div className="firstName-circle">
          <span className="firtName">{firstName}</span>
        </div>
      </div>
      <div className="card-col flex-col">
        <span className="main-user-hi">안녕하세요,</span>
        <span className="fullName">
          {`${firstName}${lastName} ${staffRank}님`}
        </span>
      </div>
    </div>
  );
};

export default MainUserCard;
