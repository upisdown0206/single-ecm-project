import React from "react";

import { IconBox } from "components/common/Icons";

import "./ReturnCardPanel.scss";

const ReturnCardPanel = ({
  onClick,
  id,
  itemCode,
  status,
  delivery,
  description,
  dttm,
}) => {
  return (
    <div className="return-card-panel" onClick={onClick}>
      <div className="row spb" style={{ marginBottom: "3px" }}>
        <div className="col">
          <span className="title">{itemCode}</span>
          <span className="date">{dttm}</span>
          <span className="date time">12AM</span>
        </div>
        <div className="col">
          <div className="box-text">{`${id}`}</div>
        </div>
      </div>
      <div className="row" style={{ marginBottom: "8px" }}>
        <div className="col">
          <span className="desc">{description}</span>
        </div>
      </div>
      <div className="row">
        <div className="value-with-icon no-icon">
          <span className="icon-value">Approval</span>
        </div>
        <div className="value-with-icon">
          <IconBox width="18" height="18" />
          <span className="icon-value">{delivery}</span>
        </div>
      </div>
    </div>
  );
};

export default ReturnCardPanel;
