import React, { useState } from "react";
import { format } from "date-fns";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import DateCalendarPicker from "components/common/DateCalendarPicker";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import CircleWithIcon from "components/common/CircleWithIcon";

import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeE from "components/search/LabelInputTypeE";

import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";
import SelectInputDiv from "components/search/SelectInputDiv";
import DimedModal from "components/base/DimedModal";

import SingleSelect from "components/common/select/SingleSelect";
import MultiSelect from "components/common/select/MultiSelect";

import LabelInputCircleRadio from "components/workOrder/LabelInputCircleRadio";

import {
  IconCommBack,
  IconInputClock,
  IconCalendar,
  IconDelete12,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./ReturnSearchForm.scss";

const ReturnSearchForm = ({ onBackClick }) => {
  const [mode, setMode] = useState("init");
  const [subMode, setSubMode] = useState("");
  const onInnerClose = () => {
    setMode("");
  };

  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");
  const [searchExpandMode, setSearchExpandMode] = useState(false);

  const [keywords, setKeywords] = useState([
    {
      id: "1",
      type: "itemCode",
      text: "Item1",
      subText: "Item1 Item1",
    },
    {
      id: "s1",
      type: "status",
      text: "Accept",
    },
  ]);
  const statusSelectItems = [
    {
      id: "s1",
      value: "Accept",
    },
    {
      id: "s2",
      value: "Reject",
    },
    {
      id: "s3",
      value: "Status 3",
    },
    {
      id: "s4",
      value: "Status 4",
    },
    {
      id: "s5",
      value: "Status 5",
    },
    {
      id: "s6",
      value: "Status 6",
    },
  ];

  const timeSelectItems = [
    {
      id: "t1",
      value: "09AM",
    },
    {
      id: "t2",
      value: "10AM",
    },
    {
      id: "t3",
      value: "11AM",
    },
    {
      id: "t4",
      value: "12AM",
    },
    {
      id: "t5",
      value: "1PM",
    },
  ];

  const onInputAllClearClick = () => {
    setKeywords([]);
    setStatusSelectedId(null);
    setTimeSelectedIds([]);
  };

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const [scheduledFromDt, setScheduledFromDt] = useState("");
  const [scheduledToDt, setScheduledToDt] = useState("");

  const [isShowFirstCalenderBox, setIsShowFirstCalendarBox] = useState(false);
  const [fromToType, setFromToType] = useState("FROM");

  const onFirstCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
  };

  const onItemClick = () => {
    setFormType("itemCode");
    setMode("FORM_DETAIL");
    setSearchInitValue("");
    setSearchExpandMode(false);
  };

  const result = [
    {
      id: "delivery",
      value: "Delivery",
    },
    {
      id: "direct",
      value: "Direct",
    },
  ];

  const [selectedId, setSelectedId] = useState("delivery");
  const [statusSelectedId, setStatusSelectedId] = useState("s1");
  const [timeSelectedIds, setTimeSelectedIds] = useState(["t1", "t2"]);

  return (
    <>
      <ModalFullPageAni
        className="return-search-form"
        modeState={mode}
        isBaseComponent={true}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Search" />}
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + ${isKeyboardFocus ? `0px` : `79px`})`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeB labelText="ID" onToggleFocus={onToggleFocus} />
          </div>
          <div className="label-input">
            <SelectInputDiv
              labelText="Status"
              placeholder="Status"
              value={
                statusSelectItems.find((item) => item.id === statusSelectedId)
                  ?.value
              }
              onClick={() => setSubMode("RETURN_SEARCH/STATUS")}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeE
              labelText="Item"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="itemCode"
              onInputClick={onItemClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <div className="label">Delivery Type</div>
            <LabelInputCircleRadio
              labelText=""
              items={result}
              radioGroupName="deliveryType"
              selectedId={selectedId}
              setSelectedId={setSelectedId}
            />
          </div>
          <div className="label-input">
            <div className="label">Delivery Date</div>
            <div className="date-input-row">
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="FROM"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={
                      scheduledFromDt && format(scheduledFromDt, "yyyy.MM.dd")
                    }
                    placeholder="From"
                    readOnly
                  />
                </div>
              </div>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="TO"
                  style={{ marginLeft: "8px" }}
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={scheduledToDt && format(scheduledToDt, "yyyy.MM.dd")}
                    placeholder="To"
                    readOnly
                  />
                </div>
              </div>
            </div>
            <div className="date-input-row">
              <SelectInputDiv
                className={timeSelectedIds.length > 0 && "re-padding"}
                beforeIcon={
                  <IconBase className="time-icon">
                    <IconInputClock />
                  </IconBase>
                }
                placeholder="Time"
                value={
                  timeSelectItems.filter((item) =>
                    timeSelectedIds.includes(item.id)
                  ).length > 0 ? (
                    <div className="circles">
                      {timeSelectItems
                        .filter((item) => timeSelectedIds.includes(item.id))
                        .map((item) => (
                          <CircleWithIcon
                            key={item.id}
                            className="circle-item"
                            text={item.value}
                            icon={
                              <IconBase
                                className="delete-icon"
                                onClick={(e) => {
                                  e.stopPropagation();
                                  setTimeSelectedIds(
                                    timeSelectedIds.filter(
                                      (id) => id !== item.id
                                    )
                                  );
                                }}
                              >
                                <IconDelete12 />
                              </IconBase>
                            }
                          />
                        ))}
                    </div>
                  ) : null
                }
                onClick={() => setSubMode("RETURN_SEARCH/TIME")}
              />
            </div>
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
        {isShowFirstCalenderBox && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={scheduledFromDt}
            setFromDate={setScheduledFromDt}
            toDate={scheduledToDt}
            setToDate={setScheduledToDt}
            onClose={() => setIsShowFirstCalendarBox(false)}
          />
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={onInnerClose}
          initValue={searchInitValue}
          isExpandMode={searchExpandMode}
        />
      </ModalFullPageAni>
      {subMode === "RETURN_SEARCH/STATUS" && (
        <DimedModal className="return-search-select-modal">
          <SingleSelect
            title="Status"
            items={statusSelectItems}
            onClose={() => setSubMode("")}
            onSelect={(e) => {
              if (!e?.target?.id) return;
              setStatusSelectedId(e.target.id);
              setSubMode("");
              console.log(`Status Select Id : ${e.target.id}`);
            }}
            selectedId={statusSelectedId} //test
          />
        </DimedModal>
      )}
      {subMode === "RETURN_SEARCH/TIME" && (
        <DimedModal className="return-search-select-modal">
          <MultiSelect
            title="Time"
            onClose={() => setSubMode("")}
            items={timeSelectItems}
            selectedIds={timeSelectedIds}
            setSelectedIds={setTimeSelectedIds}
          />
        </DimedModal>
      )}
    </>
  );
};

export default ReturnSearchForm;
