import React, { useState } from "react";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";

import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";
import ReturnCardPanel from "components/return/ReturnCardPanel";
import ReturnDetail from "components/return/ReturnDetail";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./ReturnLanding.scss";

const ReturnLanding = ({ onSearchClick }) => {
  const [mode, setMode] = useState("init");
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [results] = useState([
    {
      id: "I-P-1336",
      itemCode: "QCAD0001",
      delivery: "Direct",
      description:
        "Description Description Description Description Description",
      dttm: "2020.12.10",
      owningDepartment: "CWA-전극 보수 1반 1-PG",
      deliveryFrom: "BF 1",
      deliveryTo: "Central",
      requestDate: "2020-08-11",
      requestTime: "14:33:00",
      requestBy: "박선임",
      woNo: "123456",
      opSeq: "20",
      woDescription: "P1L01-010301-MS TEst 12 - 1",
      quantity: "2",
      condition: "NEW",
      status: "Accept",
      comment:
        "청춘 청춘의 날카로우나 우리 인생을 것이다. 커다란 무엇을 이상을 보배를 가슴에 뜨거운지라, 하는 철환하였는가? 새 같은 살 인도하겠다는 그들의 간에 그들의 봄바람이다. 발휘하기 우리 길지 천고에 눈에 부패뿐이다. ",
    },
    {
      id: "I-P-1337",
      itemCode: "QCAD0002",
      delivery: "Direct",
      description:
        "Description Description Description Description Description",
      dttm: "2020.12.10",
      owningDepartment: "CWA-전극 보수 1반 1-PG",
      deliveryFrom: "BF 1",
      deliveryTo: "Central",
      requestDate: "2020-08-11",
      requestTime: "14:33:00",
      requestBy: "박선임",
      woNo: "123456",
      opSeq: "20",
      woDescription: "P1L01-010301-MS TEst 12 - 1",
      quantity: "2",
      condition: "NEW",
      status: "Reject",
      comment:
        "청춘 청춘의 날카로우나 우리 인생을 것이다. 커다란 무엇을 이상을 보배를 가슴에 뜨거운지라, 하는 철환하였는가? 새 같은 살 인도하겠다는 그들의 간에 그들의 봄바람이다. 발휘하기 우리 길지 천고에 눈에 부패뿐이다. ",
    },
    {
      id: "I-P-1338",
      itemCode: "QCAD0003",
      delivery: "Direct",
      description:
        "Description Description Description Description Description",
      dttm: "2020.12.10",
      owningDepartment: "CWA-전극 보수 1반 1-PG",
      deliveryFrom: "BF 1",
      deliveryTo: "Central",
      requestDate: "2020-08-11",
      requestTime: "14:33:00",
      requestBy: "박선임",
      woNo: "123456",
      opSeq: "20",
      woDescription: "P1L01-010301-MS TEst 12 - 1",
      quantity: "2",
      condition: "NEW",
      status: "not",
      comment:
        "청춘 청춘의 날카로우나 우리 인생을 것이다. 커다란 무엇을 이상을 보배를 가슴에 뜨거운지라, 하는 철환하였는가? 새 같은 살 인도하겠다는 그들의 간에 그들의 봄바람이다. 발휘하기 우리 길지 천고에 눈에 부패뿐이다. ",
    },
    {
      id: "I-P-1339",
      itemCode: "QCAD0004",
      delivery: "Direct",
      description:
        "Description Description Description Description Description",
      dttm: "2020.12.10",
      owningDepartment: "CWA-전극 보수 1반 1-PG",
      deliveryFrom: "BF 1",
      deliveryTo: "Central",
      requestDate: "2020-08-11",
      requestTime: "14:33:00",
      requestBy: "박선임",
      woNo: "123456",
      opSeq: "20",
      woDescription: "P1L01-010301-MS TEst 12 - 1",
      quantity: "2",
      condition: "NEW",
      status: "not",
      comment:
        "청춘 청춘의 날카로우나 우리 인생을 것이다. 커다란 무엇을 이상을 보배를 가슴에 뜨거운지라, 하는 철환하였는가? 새 같은 살 인도하겠다는 그들의 간에 그들의 봄바람이다. 발휘하기 우리 길지 천고에 눈에 부패뿐이다. ",
    },
    {
      id: "I-P-1340",
      itemCode: "QCAD0005",
      delivery: "Direct",
      description:
        "Description Description Description Description Description",
      dttm: "2020.12.10",
      owningDepartment: "CWA-전극 보수 1반 1-PG",
      deliveryFrom: "BF 1",
      deliveryTo: "Central",
      requestDate: "2020-08-11",
      requestTime: "14:33:00",
      requestBy: "박선임",
      woNo: "123456",
      opSeq: "20",
      woDescription: "P1L01-010301-MS TEst 12 - 1",
      quantity: "2",
      condition: "NEW",
      status: "not",
      comment:
        "청춘 청춘의 날카로우나 우리 인생을 것이다. 커다란 무엇을 이상을 보배를 가슴에 뜨거운지라, 하는 철환하였는가? 새 같은 살 인도하겠다는 그들의 간에 그들의 봄바람이다. 발휘하기 우리 길지 천고에 눈에 부패뿐이다. ",
    },
    {
      id: "I-P-1341",
      itemCode: "QCAD0006",
      delivery: "Direct",
      description:
        "Description Description Description Description Description",
      dttm: "2020.12.10",
      owningDepartment: "CWA-전극 보수 1반 1-PG",
      deliveryFrom: "BF 1",
      deliveryTo: "Central",
      requestDate: "2020-08-11",
      requestTime: "14:33:00",
      requestBy: "박선임",
      woNo: "123456",
      opSeq: "20",
      woDescription: "P1L01-010301-MS TEst 12 - 1",
      quantity: "2",
      condition: "NEW",
      status: "not",
      comment:
        "청춘 청춘의 날카로우나 우리 인생을 것이다. 커다란 무엇을 이상을 보배를 가슴에 뜨거운지라, 하는 철환하였는가? 새 같은 살 인도하겠다는 그들의 간에 그들의 봄바람이다. 발휘하기 우리 길지 천고에 눈에 부패뿐이다. ",
    },
  ]);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "itemCode", text: "QCA" },
    { id: "2", type: "fromto", text: "2020-08-11 ~ 2020-08-11" },
  ]);

  const [selectInfo, setSelectInfo] = useState(null);

  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <div className="return-landing">
          <FixedHeader
            left={
              <IconBase
                onClick={
                  isMenuOpen ? undefined : () => setIsMenuOpen(!isMenuOpen)
                }
              >
                {!isMenuOpen && <IconHamburger />}
              </IconBase>
            }
            center={
              <div className="equipment-title">
                <HeaderTitle text="Return">
                  {results && <CircleText text={results.length} />}
                </HeaderTitle>
              </div>
            }
          />
          <BaseMain
            className="delivery-main"
            isMenuOpen={isMenuOpen}
            onMenuClick={() => setIsMenuOpen(!isMenuOpen)}
            style={{
              height: `calc(100vh - (56px + 60px + 55px))`,
              marginTop: `calc(56px + 55px)`,
            }}
          >
            <FixedWrapper style={{ top: "56px" }}>
              <SearchConditionPanelTypeB
                onSearchClick={onSearchClick}
                keywords={keywords}
                setKeywords={setKeywords}
                isBorderBot={false}
              />
            </FixedWrapper>
            {results && (
              <ul className="return-items">
                {results.map((item) => (
                  <li key={item.id} className="return-item">
                    <ReturnCardPanel
                      {...item}
                      onClick={() => {
                        setMode("RETURN_DETAIL");
                        setSelectInfo(item);
                      }}
                    />
                  </li>
                ))}
              </ul>
            )}
          </BaseMain>
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        </div>
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="RETURN_DETAIL">
        <ReturnDetail onBackClick={() => setMode("")} {...selectInfo} />
      </ModalFullPageAni>
    </>
  );
};

export default ReturnLanding;
