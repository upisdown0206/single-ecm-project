import React, { useState } from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";

import {
  IconCommBack,
  IconComment,
  IconMainReleaseFull,
  IconMainAlertFull,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import { useSnackbar } from "notistack";

import HeaderTitle from "components/common/HeaderTitle";

import OKCancelModal from "components/modal/OKCancelModal";
import DimedModal from "components/base/DimedModal";

import FixedFooter from "components/base/FixedFooter";

import "./ReturnDetail.scss";

const ReturnDetail = ({
  id,
  itemCode,
  status,
  delivery,
  description,
  dttm,
  owningDepartment,
  deliveryFrom,
  deliveryTo,
  requestDate,
  requestTime,
  requestBy,
  woNo,
  opSeq,
  woDescription,
  quantity,
  condition,
  comment,
  onBackClick,
}) => {
  const [confirmModal, setConfirmModal] = useState(false);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = (inMessage, isOK = true) => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar(inMessage, {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 80 }}
          onClick={() => closeSnackbar(key)}
        >
          <div className="ok-wrapper">
            {isOK ? (
              <IconMainReleaseFull fill="#04B395" width="22" height="22" />
            ) : (
              <IconMainAlertFull width="22" height="22" />
            )}
            <span className="text">{message}</span>
          </div>
        </div>
      ),
    });
  };

  const isFixedFooter = status === "not";
  const [isAccept, setIsAccept] = useState(true);

  return (
    <>
      <div className="return-detail">
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack fill="#ffffff" bgFill="#424952" />
            </IconBase>
          }
          center={<HeaderTitle text="Return" blackMode={true} />}
          blackMode={true}
        />

        <main
          className="return-detail-main"
          style={{
            height: isFixedFooter
              ? `calc(100vh - (56px + 90px))`
              : `calc(100vh - 56px)`,
            marginTop: `56px`,
            overflowY: `auto`,
          }}
        >
          <div className="sticky-header flex-spb">
            <div className="col">
              <div className="text">{itemCode}</div>
              <div className="sub-text">{dttm}</div>
            </div>
          </div>
          <div className="content-panel">
            <div className="content-row">
              <div className="col">
                <div className="label">Item Description</div>
                <div className="value">{description}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">ID</div>
                <div className="value">{id}</div>
              </div>
              <div className="col">
                <div className="label">Owning Department</div>
                <div className="value">{owningDepartment}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">From</div>
                <div className="value">{deliveryFrom}</div>
              </div>
              <div className="col">
                <div className="label">To</div>
                <div className="value">{deliveryTo}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Request Date</div>
                <div className="value">
                  {requestDate}
                  <span className="sub">{requestTime}</span>
                </div>
              </div>
              <div className="col">
                <div className="label">Request By</div>
                <div className="value">{requestBy}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">WO No</div>
                <div className="value">{woNo}</div>
              </div>
              <div className="col">
                <div className="label">Op Seq</div>
                <div className="value">{opSeq}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">WO Description</div>
                <div className="value">{woDescription}</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Quantity</div>
                <div className="value">{quantity}</div>
              </div>
              <div className="col">
                <div className="label">Delivery Type</div>
                <div className="value">
                  <div className="circle">{delivery}</div>
                </div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Status</div>
                <div className="value">
                  Approval {status !== "not" && status}
                </div>
              </div>
              <div className="col">
                <div className="label">Condition</div>
                <div className="value">
                  <div className="circle">{condition}</div>
                </div>
              </div>
            </div>
          </div>
          <div className="comment-panel">
            <div className="inner">
              <div className="comment-header">
                <IconBase className="comment-icon">
                  <IconComment width="24" height="24" />
                </IconBase>
                <div className="comment-title">Comment</div>
              </div>
              <textarea className="comment-ta" placeholder="Placeholder" />
              {!isFixedFooter && (
                <div className="return-buttons" style={{ marginTop: "30px" }}>
                  <div
                    className={cx(
                      "return-button",
                      status === "Reject" ? "active" : "disabled"
                    )}
                  >
                    <span className="button-text">Reject</span>
                  </div>
                  <div
                    className={cx(
                      "return-button",
                      status === "Accept" ? "active" : "disabled"
                    )}
                  >
                    <span className="button-text">Accept</span>
                  </div>
                </div>
              )}
            </div>
          </div>
        </main>
        {isFixedFooter && (
          <FixedFooter className="footer-white">
            <div className="return-buttons">
              <div
                className="return-button"
                onClick={() => {
                  setConfirmModal(true);
                  setIsAccept(false);
                }}
              >
                <span className="button-text">Reject</span>
              </div>
              <div
                className="return-button"
                onClick={() => {
                  setConfirmModal(true);
                  setIsAccept(true);
                }}
              >
                <span className="button-text">Accept</span>
              </div>
            </div>
          </FixedFooter>
        )}
      </div>
      {confirmModal && (
        <DimedModal className="modal-top-max return-confirm-modal">
          <OKCancelModal
            title="결과를 저장하시겠습니까?"
            onConfirm={() => {
              onBackClick();
              onShowMessage(
                `${itemCode}의 반환요청을 ${
                  isAccept ? "승인" : "거절"
                }하였습니다.`,
                isAccept
              );
            }}
            onClose={() => setConfirmModal(false)}
          >
            <p className="modal-content">
              결과를 저장하면 수정이 불가능합니다.
            </p>
          </OKCancelModal>
        </DimedModal>
      )}
    </>
  );
};

export default ReturnDetail;
