import React, { useState } from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseMain from "components/base/BaseMain";

import StockTakingDetailCardPanel from "components/stocktaking/StockTakingDetailCardPanel";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";
import { IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./StockTakingDetail.scss";

// type Item = {
//   stockId: string,
//   stockDesc: string,
//   location: string,
//   quantity: string,
//   quantityAvailable: string,
//   gapType: string,
//   comments: string,
// }

// type Items = Item[];

const StockTakingDetail = ({ items, status, onBackClick }) => {
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  return (
    <div className="stock-taking-detail">
      <FixedHeader
        left={
          <IconBase onClick={onBackClick}>
            <IconCommBack />
          </IconBase>
        }
        center={
          <div className="equipment-title">
            <HeaderTitle text="Detail" />
          </div>
        }
      />
      <BaseMain
        className="stock-taking-detail-main"
        style={{
          height: isKeyboardFocus
            ? `calc(100vh - 56px)`
            : `calc(100vh - (56px + 87px))`,
          marginTop: `56px`,
        }}
      >
        <div className="sticky-header">
          <h1 className="detail-wo">
            <span className="wo-text">123675415</span>
            {items && <CircleText text={items.length} />}
          </h1>
          <p className="detail-desc">
            재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc
          </p>
        </div>
        {items && (
          <ul className="detail-items">
            {items.map((item) => (
              <li key={item.stockId} className="detail-item">
                <StockTakingDetailCardPanel
                  {...item}
                  disabled={status === "Draft"} //test
                  onFocus={() => {
                    setIsKeyboardFocus(true);
                  }}
                  onBlur={() => setIsKeyboardFocus(false)}
                />
              </li>
            ))}
          </ul>
        )}
      </BaseMain>
      {!isKeyboardFocus && (
        <FixedFooter className="footer-white">
          <div className={cx("save-button", "active")}>Save</div>
        </FixedFooter>
      )}
    </div>
  );
};

export default StockTakingDetail;
