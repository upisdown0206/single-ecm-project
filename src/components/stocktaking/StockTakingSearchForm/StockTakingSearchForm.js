import React, { useState } from "react";
import { format } from "date-fns";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import DateCalendarPicker from "components/common/DateCalendarPicker";
import ModalFullPageAni from "components/common/ModalFullPageAni";

import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeD from "components/search/LabelInputTypeD";

import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";
import SelectInputDiv from "components/search/SelectInputDiv";
import DimedModal from "components/base/DimedModal";
import SingleSelect from "components/common/select/SingleSelect";

import { IconCommBack, IconCalendar } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./StockTakingSearchForm.scss";

const StockTakingSearchForm = ({ onBackClick }) => {
  const [mode, setMode] = useState("init");
  const [subMode, setSubMode] = useState("");
  const onInnerClose = () => {
    setMode("");
  };

  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");
  const [keywords, setKeywords] = useState([]);

  const statusSelectItems = [
    {
      id: "s1",
      value: "Status 1",
    },
    {
      id: "s2",
      value: "Status 2",
    },
    {
      id: "s3",
      value: "Status 3",
    },
    {
      id: "s4",
      value: "Status 4",
    },
    {
      id: "s5",
      value: "Status 5",
    },
    {
      id: "s6",
      value: "Status 6",
    },
  ];

  const onInputAllClearClick = () => {
    setKeywords([]);
    setSelectedId(null);
  };

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const [scheduledFromDt, setScheduledFromDt] = useState("");
  const [scheduledToDt, setScheduledToDt] = useState("");

  const [isShowFirstCalenderBox, setIsShowFirstCalendarBox] = useState(false);
  const [fromToType, setFromToType] = useState("FROM");

  const onFirstCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
  };

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const onItemCodeClick = () => {
    setFormType("department");
    setMode("FORM_DETAIL");
    setSearchInitValue("");
  };

  const [selectedId, setSelectedId] = useState("s1");
  return (
    <>
      <ModalFullPageAni
        className="stock-taking-search-form"
        modeState={mode}
        isBaseComponent={true}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Search" />}
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + ${isKeyboardFocus ? `0px` : `79px`})`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeB
              labelText="Stocktaking No"
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB
              labelText="Description"
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB
              labelText="Create By"
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeD
              labelText="Owning Department"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="department"
              onInputClick={onItemCodeClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <SelectInputDiv
              labelText="Status"
              placeholder="Status"
              value={
                statusSelectItems.find((item) => item.id === selectedId)?.value
              } //test
              onClick={() => setSubMode("STOCKTAKING_SEARCH/SELECT-STATUS")}
            />
          </div>
          <div className="label-input">
            <div className="label">Request Date</div>
            <div className="date-input-row">
              <span className="date-label">From</span>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="FROM"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={
                      scheduledFromDt && format(scheduledFromDt, "yyyy.MM.dd")
                    }
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
              </div>
            </div>
            <div className="date-input-row">
              <span className="date-label">To</span>
              <div className="calendar-fromto-type">
                <div
                  className="date-calendar-input input-col"
                  onClick={onFirstCalendarModal}
                  data-fromtotype="TO"
                >
                  <IconBase>
                    <IconCalendar width="17" height="18" />
                  </IconBase>
                  <input
                    type="text"
                    value={scheduledToDt && format(scheduledToDt, "yyyy.MM.dd")}
                    placeholder="YYYY.MM.DD"
                    readOnly
                  />
                </div>
              </div>
            </div>
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={
                (keywords?.length + selectedId ? 1 : 0) > 0 &&
                (keywords?.length + selectedId ? 1 : 0)
              }
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
        {isShowFirstCalenderBox && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={scheduledFromDt}
            setFromDate={setScheduledFromDt}
            toDate={scheduledToDt}
            setToDate={setScheduledToDt}
            onClose={() => setIsShowFirstCalendarBox(false)}
          />
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={onInnerClose}
          initValue={searchInitValue}
        />
      </ModalFullPageAni>
      {subMode === "STOCKTAKING_SEARCH/SELECT-STATUS" && (
        <DimedModal className="stocktaking-search-select-modal">
          <SingleSelect
            title="Status"
            items={statusSelectItems}
            onClose={() => setSubMode("")}
            onSelect={(e) => {
              if (!e?.currentTarget?.id) return;

              if (e.currentTarget.id === selectedId) {
                setSelectedId(null);
              } else {
                setSelectedId(e.currentTarget.id);
              }
              setSubMode("");
            }}
            selectedId={selectedId} //test
          />
        </DimedModal>
      )}
    </>
  );
};

export default StockTakingSearchForm;
