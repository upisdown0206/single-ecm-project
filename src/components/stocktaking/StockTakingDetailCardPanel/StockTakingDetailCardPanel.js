import React, { useState } from "react";
import cx from "classnames";

import DimedModal from "components/base/DimedModal";
import SingleSelect from "components/common/select/SingleSelect";

import IconBase from "components/common/IconBase";
import { IconLocation, IconSelectArrow2 } from "components/common/Icons";

import sampleImg from "asset/images/inventory-default-thumbnail.png";

import "./StockTakingDetailCardPanel.scss";

const StockTakingDetailCardPanel = ({
  stockId,
  stockDesc,
  location,
  quantity,
  quantityAvailable,
  gapType,
  comments,
  disabled = false,
  onFocus,
  onBlur,
}) => {
  const [subMode, setSubMode] = useState("");
  const [qaValue, setqaValue] = useState(quantityAvailable);
  const [commentValue, setcommentValue] = useState(comments);
  const onChange = (e) => {
    const {
      target: { value, name },
    } = e;
    if (name === "qa") {
      setqaValue(value);
    } else if (name === "comment") {
      setcommentValue(value);
    }
  };

  const gapSelectItems = [
    {
      id: "g1",
      value: "G01",
    },
    {
      id: "g2",
      value: "G02",
    },
    {
      id: "g3",
      value: "G03",
    },
  ];

  const onGapTypeClick = () => setSubMode("STOCKTAKING_DETAIL/GAP_TYPE");

  const [selectedId, setSelectedId] = useState(gapType);

  return (
    <div className="stock-taking-detail-card-panel">
      <div className="ab-location">
        <IconBase className="location-icon">
          <IconLocation width="16" height="16" />
        </IconBase>
        <span className="location-text">{location}</span>
      </div>
      <div className="row">
        <div className="col">
          <div className="detail-img-box">
            <img className="detail-img" src={sampleImg} alt="detail material" />
          </div>
        </div>
        <div className="col flex-col">
          <span className="detail-title">{stockId}</span>
          <span className="detail-desc">{stockDesc}</span>
        </div>
      </div>
      <div className="row spb" style={{ marginTop: "10px" }}>
        <div className="col">
          <span className="label">Quantity</span>
        </div>
        <div className="col">
          <span className="value primary">{quantity}</span>
        </div>
      </div>
      <div className="row spb" style={{ marginTop: "10px" }}>
        <div className="col">
          <span className="label">Quantity Available</span>
        </div>
        <div className="col">
          <input
            className={cx("number-input", disabled && "disabled")}
            name="qa"
            type="number"
            value={qaValue}
            onChange={onChange}
            readOnly={disabled}
            onFocus={() => {
              if (disabled) return;
              onFocus();
            }}
            onBlur={() => {
              if (disabled) return;
              onBlur();
            }}
          />
        </div>
      </div>
      <div className="row spb" style={{ marginTop: "10px" }}>
        <div className="col">
          <span className="label">Gap Type</span>
        </div>
        <div
          className="col flex-row"
          onClick={disabled ? null : onGapTypeClick}
        >
          <span className="value">
            {gapSelectItems.find((item) => item.id === selectedId)?.value}
          </span>
          <IconBase className={cx("arrow-icon", disabled && "disabled")}>
            <IconSelectArrow2 width="24" height="24" />
          </IconBase>
        </div>
      </div>
      <div className="row" style={{ marginTop: "10px" }}>
        <div className="col flex-start" style={{ marginRight: "14px" }}>
          <span className="label">Comments</span>
        </div>
        <div className="col flex-fill">
          <textarea
            name="comment"
            className={cx("comment-ta", disabled && "disabled")}
            placeholder="Placeholder"
            value={commentValue}
            readOnly={true}
            onChange={onChange}
            onFocus={() => {
              if (disabled) return;
              onFocus();
            }}
            onBlur={() => {
              if (disabled) return;
              onBlur();
            }}
          />
        </div>
      </div>
      {subMode === "STOCKTAKING_DETAIL/GAP_TYPE" && (
        <DimedModal className="stocktaking-detail-select-modal">
          <SingleSelect
            title="Gap Type"
            items={gapSelectItems}
            onClose={() => setSubMode("")}
            onSelect={(e) => {
              if (!e?.target?.id) return;
              // 같을 시엔 취소
              if (e.target.id === selectedId) {
                setSelectedId(null);
              } else {
                setSelectedId(e.target.id);
              }
              setSubMode("");
            }}
            selectedId={selectedId} //test
          />
        </DimedModal>
      )}
    </div>
  );
};

export default StockTakingDetailCardPanel;
