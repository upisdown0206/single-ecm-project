import React, { useState } from "react";
import { format } from "date-fns";

import "./DailyCheckFilterSearchForm.scss";

import { IconCalendar, IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DateCalendarPicker from "components/common/DateCalendarPicker";
import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";
import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeD from "components/search/LabelInputTypeD";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";
import RoundTag from "components/common/RoundTag";
import ModalFullPage from "../../common/ModalFullPage";

const DailyCheckFilterSearchForm = ({onClick}) => {
  const [keywords, setKeywords] = useState([
    { id: "1", type: "woNo", text: "wo1111" },
    { id: "2", type: "woNo", text: "wo2222" },
    { id: "3", type: "equipmentId", text: "123444" },
    { id: "4", type: "equipmentId", text: "456555" },
    { id: "5", type: "owningDept", text: "조립1팀" },
    { id: "6", type: "owningDept", text: "조립2팀" },
    { id: "7", type: "woType", text: "BM" },
    { id: "8", type: "woType", text: "PM" },
  ]);

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const [scheduledFromDt, setScheduledFromDt] = useState("");
  const [scheduledToDt, setScheduledToDt] = useState("");

  const [isShowFirstCalenderBox, setIsShowFirstCalendarBox] = useState(false);
  const [fromToType, setFromToType] = useState("FROM");

  const onFirstCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
  };

  const [isShowCalenderBox, setisShowCalendarBox] = useState(false);
  const onCalendarModal = () => {
    setisShowCalendarBox(!isShowCalenderBox);
  };
  const [isRoundTagToggle, setRoundTagToggle] = useState(false);
  const onRoundTagToggle = (e) => {
    setRoundTagToggle(!isRoundTagToggle);
  };

  const onInputClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { type },
    } = currentTarget;

  };

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const onInputAllClearClick = () => {
    setKeywords([]);
  };

  const [isMeasured, setMeasured] = useState("MeasuredTypeChoice")
  return (
    <ModalFullPage>
      <div className="work-order-list-search-form">
        <FixedHeader
          left={
            <IconBase onClick={onClick}>
              <IconCommBack />
            </IconBase>
          }
          center={
            <div className="header-icon">
              <div className="daily-check-title">
                <span>Search1</span>
              </div>
            </div>
          }
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + 79px)`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeD
              labelText="Equipment ID"
              keywords={keywords}
              setKeywords={setKeywords}
              keywordType="equipmentId"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB
              labelText="Point"
              keywords={keywords}
              keywordType="owningDept"
              onInputClick={onInputClick}
              onToggleFocus={onToggleFocus}
            />
          </div>

          <div className="label-input">
            <label>Measured Result</label>
            <div className="filter-round div3">
              <RoundTag
                tagName={"OK"}
                onClick={()=>setMeasured("ok")}
                active={`${isMeasured === "ok" && "active"}`}
              />
              <RoundTag
                tagName={"NG"}
                onClick={()=>setMeasured("NG")}
                active={`${isMeasured === "NG" && "active"}`}
              />
              <RoundTag
                tagName={"OK Uncheck"}
                onClick={()=>setMeasured("Uncheck")}
                active={`${isMeasured === "Uncheck" && "active"}`}
              />
              <RoundTag
                tagName={"NG WO"}
                onClick={()=>setMeasured("WO")}
                active={`${isMeasured === "WO" && "active"}`}
              />
            </div>
          </div>

          <div className="label-input" onClick={onCalendarModal}>
            <label>Scheduled Start Date</label>
            <div className="calendar-fromto-type">
              <div
                className="date-calendar-input"
                onClick={onFirstCalendarModal}
                data-fromtotype="FROM"
              >
                <IconBase>
                  <IconCalendar width="17" height="18" />
                </IconBase>
                <input
                  type="text"
                  value={scheduledFromDt && format(scheduledFromDt, "yyyy.MM.dd")}
                  placeholder="YYYY.MM.DD"
                  readOnly
                />
              </div>
              <div className={"bar"}>-</div>
              <div
                className="date-calendar-input"
                onClick={onFirstCalendarModal}
                data-fromtotype="TO"
              >
                <IconBase>
                  <IconCalendar width="17" height="18" />
                </IconBase>
                <input
                  type="text"
                  value={scheduledToDt && format(scheduledToDt, "yyyy.MM.dd")}
                  placeholder="YYYY.MM.DD"
                  readOnly
                />
              </div>
            </div>
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
              onClick={onClick}
            />
          </FixedFooter>
        )}
        {isShowFirstCalenderBox && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={scheduledFromDt}
            setFromDate={setScheduledFromDt}
            toDate={scheduledToDt}
            setToDate={setScheduledToDt}
            onClose={() => setIsShowFirstCalendarBox(false)}
          />
        )}
      </div>
    </ModalFullPage>
  );
};

export default DailyCheckFilterSearchForm;
