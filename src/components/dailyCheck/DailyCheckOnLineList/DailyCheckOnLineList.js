import React, {useEffect , useState} from "react";
import classNames from 'classnames'
import "../dc-common/DailyCheckCommon.scss";
import "./DailyCheckOnLineList.scss";
import FixedFooter from "components/base/FixedFooter";
import {
  IconSearchMedium,
} from "components/common/Icons";
import CheckBox from "components/common/CheckBox";
import DailyCheckOnlineItem from "components/dailyCheck/DailyCheckOnlineItem";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";

const DailyCheckOnLineList = ({onClick,onLineDownloadPage}) => {

  const DCOnlineList = ({ children }) => {
    return <div className="dc-online-list">{children}</div>;
  };

  const [downloadList, setDownloadList] = useState([
    { id: "1",
      checked : false,
      title: "P8TCVD0103",
      date:"2020-11-09",
      point:"110",
      roundTag:true,
      active:"active",
      tagName:"OK",
      value1: "01"
    },
    { id: "2",
      checked : false,
      title: "P8TCVD0103",
      date:"2020-11-09",
      point:"110",
      roundTag:true,
      active:"active",
      tagName:"NG",
      value1: "02"
    },
    { id: "3",
      checked : false,
      title: "P8TCVD0103",
      date:"2020-11-09",
      point:"110",
      roundTag:true,
      active:"",
      tagName:"OK",
      value1: "03"
    },
    { id: "4",
      checked : false,
      title: "P8TCVD0103",
      date:"2020-11-09",
      point:"110",
      roundTag:true,
      active:"",
      tagName:"NG",
      value1: "04"
    },
    { id: "5",
      checked : false,
      title: "P8TCVD0103",
      date:"2020-11-09",
      point:"110",
      roundTag:"",
      active:"",
      tagName:"",
      value1: "05"
    },
  ]);

  const [checked, setCheck] = useState(false);
  const onChange = (e) => {
    setCheck(e.target.checked);
  };

  return (
    <>
      {/*
        하단 초기화 검색 버튼 없을 시 삭제
        style={{marginBottom:"79px", height:"100%"}}
      */}

      <DCOnlineList >
        <div className={classNames("dc-online-list-header")}>
          <div>
            <CheckBox value1={"dc-01"} onChange={onChange}
              checked = {downloadList.filter((item) => item.checked === false).length > 0
                ? checked
                : !checked}
            />
            <label htmlFor={"cbdc-01"}>
              전체선택
            </label>

          </div>
          <div className={"new-notice"} onClick={onClick}>
            <button>
              <IconSearchMedium width="16px" height="19px" />
            </button>
          </div>
        </div>

        <div className="dc-online-list-wrap">
            {downloadList.map((item,idx)=>(
              <DailyCheckOnlineItem
                key={idx}
                title={item.title}
                date={item.date}
                point={item.point}
                roundTag={item.roundTag}
                tagName={item.tagName}
                active={item.active}
                onChange={onChange}
                checked={item.checked}
                value1={item.value1}
                index={idx}
              />
            ))}
          </div>

        {/*<div className="comm-fix-bottom div2 border1">*/}
        {/*  <div className="state-txt">초기화 3</div>*/}
        {/*  <Button onClick={onLineDownloadPage}>다운로드</Button>*/}
        {/*</div>*/}

        <FixedFooter>
          <SearchButtonPanelTypeA
            btnColor="gray"
            btnText="다운로드"
            onClick={onLineDownloadPage}
          />
        </FixedFooter>
      </DCOnlineList>
    </>
  );
};

export default DailyCheckOnLineList;
