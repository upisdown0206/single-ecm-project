import React from "react";

import "./DailyCheckModalReDownload.scss";
import "../dc-common/DailyCheckCommon.scss";

import Button from "components/common/Button";
import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";

const DailyCheckModalReDownload = ({onClick,onCancel}) => {
  return (
    <DailyCheckModal
      background="bg-type"
      position="modal-box-center"
    >
      <div className="modal-header">
        <h1>이미 업로드된 항목이 있습니다</h1>
        <button className="btn-popup-close" onClick={onCancel}>
          <IconBase>
            <IconClosePopup />
          </IconBase>
        </button>
      </div>
      <div className="modal-contents">
        <p className="guide-text">
          2020.10.03 00시에 50건을 업로드하였습니다.<br/>
          다시 다운로드 받으면 현재버전으로 업데이트됩니다.<br/>
          그래도 다운로드 하시겠습니까?
        </p>
      </div>

      <div className="btn-wrapper align-right">
        <Button size="small" color="text-only-gray" onClick={onCancel}>
          취소
        </Button>
        <Button size="small" color="text-only-primary" onClick={onClick}>
          확인
        </Button>
      </div>
    </DailyCheckModal>
  );
};

export default DailyCheckModalReDownload;
