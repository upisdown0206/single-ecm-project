import React from "react";
import "./DailyCheckModalConnect.scss";
import "../dc-common/DailyCheckCommon.scss";

import { IconOffline } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";

const DailyCheckModalConnect = () => {
  return (
    <DailyCheckModal
      title="인터넷연결안됨"
      background="bg-type"
      position="modal-box-center"
    >
      <div className="modal-header">
        <h1>인터넷연결안됨</h1>
      </div>
      <div className="modal-contents">
        <div className="connect-img">
          <IconBase>
            <IconOffline width="51px" height="51px" fill="#42C9DE" />
          </IconBase>
        </div>
        <div className="connect-text">
          인터넷이 연결되어있지않습니다.
          <br />
          인터넷연결시 저장된 항목들이 자동으로 업데이트됩니다.
        </div>
      </div>
      <span className="connect-tootip">3초뒤 사라짐</span>
    </DailyCheckModal>
  );
};

export default DailyCheckModalConnect;
