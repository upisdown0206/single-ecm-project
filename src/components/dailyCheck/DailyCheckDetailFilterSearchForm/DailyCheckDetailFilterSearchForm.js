import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import LabelInputTypeD from "components/search/LabelInputTypeD";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";

import { IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import RoundTag from "components/common/RoundTag";

import "./DailyCheckDetailFilterSearchForm.scss";
import ModalFullPage from "../../common/ModalFullPage";

const DailyCheckDetailFilterSearchForm = ({onBackClick,onFilterOpenClose}) => {
  const [keywords, setKeywords] = useState([
    { id: "1", type: "woNo", text: "wo1111" },
    { id: "2", type: "woNo", text: "wo2222" },
    { id: "3", type: "equipmentId", text: "123444" },
    { id: "4", type: "equipmentId", text: "456555" },
    { id: "5", type: "owningDept", text: "조립1팀" },
    { id: "6", type: "owningDept", text: "조립2팀" },
    { id: "7", type: "woType", text: "BM" },
    { id: "8", type: "woType", text: "PM" },
  ]);
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const [isShowSelectBox, setIsShowSelectBox] = useState(false);
  const onSelectModal = () => {
    setIsShowSelectBox(!isShowSelectBox);
  };

  const [isShowCalenderBox, setisShowCalendarBox] = useState(false);
  const onCalendarModal = () => {
    setisShowCalendarBox(!isShowCalenderBox);
  };
  const [isRoundTagToggle, setRoundTagToggle] = useState(false);
  const onRoundTagToggle = () => {
    setRoundTagToggle(!isRoundTagToggle);
  };

  const onInputClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { type },
    } = currentTarget;

    // history.push(`/form-detail/${type}`);
  };

  const onInputAllClearClick = () => {
    setKeywords([]);
  };

  return (
    <ModalFullPage>
      <div className="work-order-list-search-form">
        <FixedHeader
          left={
            <IconBase onClick={onFilterOpenClose}>
              <IconCommBack />
            </IconBase>
          }
          center={
            <div className="header-icon">
              <div className="daily-check-title">
                <span>Filter</span>
              </div>
            </div>
          }
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + 79px)`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeD
              labelText="키워드"
              keywords={keywords}
              setKeywords={setKeywords}
              keywordType="equipmentId"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>

          <div className="label-input">
            <label>담당자</label>
            <div className="filter-round div2">
              <RoundTag
                tagName={"My check"}
                onClick={onRoundTagToggle}
                active={`${isRoundTagToggle ? "active" : ""}`}
              />
              <RoundTag
                tagName={"Dept check"}
                onClick={onRoundTagToggle}
                active={`${isRoundTagToggle ? "active" : ""}`}
              />
            </div>
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
      </div>
    </ModalFullPage>
  );
};

export default DailyCheckDetailFilterSearchForm;
