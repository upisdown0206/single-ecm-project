import React from 'react';
import "./DailyCheckModal.scss";
import classNames from "classnames";

const DailyCheckModal = ({background,color,position,children,style}) => {
  return (
    <div className={classNames("modal-wrapper", background, color)} style={style}>
      <div className={classNames(position)}>
        {children}
      </div>
    </div>

  );
};

export default DailyCheckModal;