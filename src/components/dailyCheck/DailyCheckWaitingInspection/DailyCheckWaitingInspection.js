import React, { useState } from "react";

import "../dc-common/DailyCheckCommon.scss";
import "./DailyCheckWaitingInspection.scss";

import Button from "components/common/Button";
import DailyCommonRoundBox from "components/dailyCheck/DailyCommonRoundBox/DailyCommonRoundBox";
import PieChartCommon from "../../common/PieChartCommon";
import {IconDownload, IconDownload2} from "../../common/Icons";

const DailyCheckWaitingInspection = ({onCheckStart}) => {
  const [isSuccessFail, setIssuccessFail] = useState(false);
  const onSuccessFail = () => {
    setIssuccessFail(!isSuccessFail);
  };
  return (
    <>
      <DailyCommonRoundBox title="Check" inspectionStart>
        <PieChartCommon total={"50"} />

        <div className="legend-type">
          <span className="type01">
            <span className="bu"></span>My check<em className="number">23</em>
          </span>
          <span className="type02">
            <span className="bu"></span>Dept check
            <em className="number">15</em>
          </span>
        </div>
      </DailyCommonRoundBox>

      <div className="comm-fix-bottom div3-else">
        <button className="btn-download"><IconDownload fill={"#6B7682"} /></button>
        <Button outline>업로드</Button>
        <Button onClick={onCheckStart}>점검시작</Button>
      </div>
        {/*메시지 알림 업로드 성공 시 <DailyCheckModalSuccess /> */}
        {/*메시지 알림 업로드 실패 시  <DailyCheckModalFail /> */}

        {/*메시지 알림 타입 수정 => 확정 후 교체  */}
        {/*<MessageBar*/}
        {/*  style={{ bottom: "108px" }}*/}
        {/*  iconType={*/}
        {/*    <span className="icon-notice-success">*/}
        {/*      <IconListSelectThin*/}
        {/*        strokeColor={"#ffffff"}*/}
        {/*        size={"15px"}*/}
        {/*      />*/}
        {/*    </span>*/}
        {/*  }*/}
        {/*  messageText={"5건중 3건 업로드 성공!"}*/}
        {/*/>*/}
    </>
  );
};

export default DailyCheckWaitingInspection;
