import React, {useState} from "react";
import classNames from 'classnames';
import "../dc-common/DailyCheckCommon.scss";
import "./DailyCheckWaitingInspectionList.scss";

import FixedHeader from "components/base/FixedHeader";
import ContentsBox from "components/common/ContentsBox";
import Button from "components/common/Button";
import {
  IconCommBack,
  IconPlus,
  IconComment,
  IconSelectArrow2, IconNFCSmall, IconInputClock,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DailyCheckModalVoiceRecording from "../DailyCheckModalVoiceRecording";
import DailyCheckSelectBoxModal from "components/dailyCheck/DailyCheckSelectBoxModal";
import DailyCheckModalComments from "../DailyCheckModalComments";
import Counter from "../../common/Counter";
import ModalFullPage from "../../common/ModalFullPage";
import {IconClosePopup, IconDelete12, IconNFCTag, IconSearchSmall} from "../../common/Icons-bak";
import DailyCheckModalCheckType from "../DailyCheckModalCheckType";
import GuideTooltip from "../../common/GuideTooltip";

const DailyCheckWaitingInspectionList = ({onBackClick,onFilterOpenClose,onLineDownloadPage}) => {
  const [isRecodeModal, setIsRecodeModal] = useState(false);
  const [isWOTypeModal, setWOTypeModal] = useState(false);
  const [isCommentModal, setCommentModal] = useState(false);
  const [isCheckSearch, setIsCheckSearch] = useState(false);
  const [isSelectChekDetailType, setSelectChekDetailType] = useState(false);

  const onRecodeModal = () => {
    setIsRecodeModal(!isRecodeModal);
  };
  const onWOTypeModal = () => {
    setWOTypeModal(!isWOTypeModal);
  };
  const onCommentModal = () => {
    setCommentModal(!isCommentModal);
  };

  const onCheckSearch = (e) => {
    setIsCheckSearch(!isCheckSearch);
  }

  const [text, setText] = useState('');

  const onChange = (e) => {
    setText(e.target.value);
  };

  const onCheckSearchDelete = () => {
    setText('');
    setIsCheckSearch(!isCheckSearch)
  };

  const onSelectChekDetailType = () => {
    setSelectChekDetailType(!isSelectChekDetailType);
  }

  const [isTypeText, setTypeText] =useState("All")
  const [isNFCDisabled, setIsNFCDisabled] = useState(true);


  return (
    <ModalFullPage>
      <FixedHeader
        left={
          <IconBase onClick={onBackClick}>
            <IconCommBack />
          </IconBase>
        }
        checkDetail={true}
        right={
          <div className="check-detail-search-wrap">
            <div className={classNames("check-detail-search-item", isCheckSearch && "on")} onClick={onCheckSearch}>
              <span className="search-icon">
                  <IconSearchSmall />
              </span>
              <input type="text" placeholder="Check Detail" onChange={onChange} value={text} />
              <button onClick={onCheckSearchDelete}>
                <IconDelete12 />
              </button>
            </div>
            <div className="select-checkDetail-type"
              onClick={onSelectChekDetailType}
            >
              <div>{isTypeText}</div>
            </div>
          </div>
        }
      />
      {isSelectChekDetailType &&
        <DailyCheckModalCheckType
          onClick={onSelectChekDetailType}
        />
      }
      {
        text.length > 0 && (
          <div className="search-fix-notice">
            총 <span>3건</span>의 항목이 검색되었습니다.
          </div>
        )
      }

      <ContentsBox>
        {
          text.length > 0 && (
            <div style={{height:"28px"}}></div>
          )
        }
        <div className="daily-list">
          <div className="daily-list-item">
            <div className="dl-header">
              <div className="title">
                <span>1.</span>D1CPKG011210
              </div>

              <div className={"btn-r-wrap"}>
                <button className="btn-NFC-tag">
                  <IconNFCSmall />
                </button>
                <div className="tag-round">
                  110<span>&nbsp;P</span>
                </div>
              </div>
            </div>

            <hr className="border1" />

            <div className="dl-contents">
              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="20%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                    <tr>
                      <th>Description</th>
                      <td colSpan="3">
                        Collar 체결 점검
                      </td>
                    </tr>
                    <tr>
                      <th>Detail</th>
                      <td colSpan="3">전구간 체결상태확인 채결 전구간 체결상태확인 채결 전구간 체결상태확인 채결</td>
                    </tr>
                    <tr>
                      <th>Date</th>
                      <td colSpan="3">2020.10.05 - 2020.10.05</td>
                    </tr>
                    <tr>
                      <th>Interval Type</th>
                      <td>Day Day Day Day</td>
                      <th>Interval</th>
                      <td>8</td>
                    </tr>
                    <tr>
                      <th>Measurement<br />Element</th>
                      <td colSpan="3">길이</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-gray-round-wrap">
                <dl className="dl-gray-round-item">
                  <dt>Upper<br />Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br/>upper bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br />Lower Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Lower<br />Bound</dt>
                  <dd>10</dd>
                </dl>
              </div>

              <div className="dl-unit">
                <span>UOM</span><span>MM</span>
              </div>

              <hr className="border2" />

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                    <tr>
                      <th>Measured <br />Value</th>
                      <td colSpan="3">
                        <Counter type="typeWide" size={"12px"} />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Comments</th>
                    <td colSpan="3">
                      <div className="comment-textarea">
                        <textarea placeholder="placeholder"></textarea>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-btn-wraps">
                <button className="btn-add" onClick={onWOTypeModal}>
                  <IconPlus fill="#A5B3C2" size="24px" />
                </button>
                <button className="btn-outline-gray">NG-Adjust</button>
                <button className="btn-outline-gray">OK</button>
              </div>
            </div>

            <div className="dl-footer-btn" style={{ display: "none" }}>
              <Button fullWidth>OK</Button>
            </div>
          </div>

          <div className="daily-list-item">
            <div className="dl-header">
              <div className="title">
                <span>1.</span>D1CPKG011210
              </div>
              <div className="tag-round" style={{ display: "none" }}>
                110<span>&nbsp;P</span>
              </div>
              <div className={"btn-r-wrap"} >
                <button className="btn-NFC-tag">
                  <IconNFCSmall />
                </button>
                <button className="btn-more">
                  <IconSelectArrow2 />
                </button>
              </div>
            </div>

            <hr className="border1" />

            <div className="dl-contents" style={{display:"none"}}>
              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Description</th>
                    <td colSpan="3">
                      Collar 체결 점검
                    </td>
                  </tr>
                  <tr>
                    <th>Detail</th>
                    <td colSpan="3">전구간 체결상태확인 채결...</td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td colSpan="3">2020.10.05 - 2020.10.05</td>
                  </tr>
                  <tr>
                    <th>Interval Type</th>
                    <td>Day</td>
                    <th>Interval</th>
                    <td>8</td>
                  </tr>
                  <tr>
                    <th>Measurement<br />Element</th>
                    <td colSpan="3">길이</td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-gray-round-wrap">
                <dl className="dl-gray-round-item">
                  <dt>Upper<br />Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br/>upper bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br />Lower Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Lower<br />Bound</dt>
                  <dd>10</dd>
                </dl>
              </div>

              <div className="dl-unit">
                <span>UOM</span><span>MM</span>
              </div>

              <hr className="border2" />

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Measured <br />Value</th>
                    <td colSpan="3">
                      <Counter type="typeWide" size={"12px"} />
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Comments</th>
                    <td colSpan="3">
                      <div className="comment-textarea">
                        <textarea placeholder="placeholder"></textarea>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-btn-wraps">
                <button className="btn-add" onClick={onWOTypeModal}>
                  <IconPlus fill="#A5B3C2" size="24px" />
                </button>
                <button className="btn-outline-gray" disabled="disabled">NG-Adjust</button>
                <button className="btn-outline-gray" disabled="disabled">OK</button>
              </div>
            </div>

            <div className="dl-footer-btn">
              <Button fullWidth>OK</Button>
            </div>
          </div>
          <div className="daily-list-item disabled">
            <div className="dl-header">
              <div className="title">
                <span>1.</span>D1CPKG011210
              </div>
              <div className="tag-round" style={{ display: "none" }}>
                110<span>&nbsp;P</span>
              </div>
              <div className={"btn-r-wrap"} >
                <button className="btn-NFC-tag" disabled={"disabled"}>
                  {
                    isNFCDisabled &&
                    <>
                      <IconNFCSmall fill={"#fff"} />
                      <IconClosePopup size={"16px"} fill={"#ffffff"} />
                    </>
                  }
                </button>
                <button className="btn-more">
                  <IconSelectArrow2 />
                </button>
              </div>
            </div>

            <hr className="border1" />

            <div className="dl-contents" style={{display:"none"}}>
              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Description</th>
                    <td colSpan="3">
                      Collar 체결 점검
                    </td>
                  </tr>
                  <tr>
                    <th>Detail</th>
                    <td colSpan="3">전구간 체결상태확인 채결...</td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td colSpan="3">2020.10.05 - 2020.10.05</td>
                  </tr>
                  <tr>
                    <th>Interval Type</th>
                    <td>Day</td>
                    <th>Interval</th>
                    <td>8</td>
                  </tr>
                  <tr>
                    <th>Measurement<br />Element</th>
                    <td colSpan="3">길이</td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-gray-round-wrap">
                <dl className="dl-gray-round-item">
                  <dt>Upper<br />Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br/>upper bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br />Lower Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Lower<br />Bound</dt>
                  <dd>10</dd>
                </dl>
              </div>

              <div className="dl-unit">
                <span>UOM</span><span>MM</span>
              </div>

              <hr className="border2" />

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Measured <br />Value</th>
                    <td colSpan="3">
                      <Counter type="typeWide" size={"12px"} />
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Comments</th>
                    <td colSpan="3">
                      <div className="comment-textarea">
                        <textarea placeholder="placeholder"></textarea>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-btn-wraps">
                <button className="btn-add" onClick={onWOTypeModal}>
                  <IconPlus fill="#A5B3C2" size="24px" />
                </button>
                <button className="btn-outline-gray">NG-Adjust</button>
                <button className="btn-outline-gray">OK</button>
              </div>
            </div>

            <div className="dl-footer-btn">
              <Button fullWidth disabled>
                OK
              </Button>
            </div>
          </div>

          <div className="daily-list-item ">
            <div className="dl-header">
              <div className="title">
                <span>1.</span>D1CPKG011210
              </div>
              <div className="tag-round" style={{ display: "none" }}>
                110<span>&nbsp;P</span>
              </div>
              <div className={"btn-r-wrap"}>
                <button className="btn-NFC-tag">
                  <IconNFCSmall />
                </button>
                <button className="btn-more">
                  <IconSelectArrow2 />
                </button>
              </div>
            </div>

            <hr className="border1" />

            <div className="dl-contents" style={{display:"none"}}>
              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Description</th>
                    <td colSpan="3">
                      Collar 체결 점검
                    </td>
                  </tr>
                  <tr>
                    <th>Detail</th>
                    <td colSpan="3">전구간 체결상태확인 채결...</td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td colSpan="3">2020.10.05 - 2020.10.05</td>
                  </tr>
                  <tr>
                    <th>Interval Type</th>
                    <td>Day</td>
                    <th>Interval</th>
                    <td>8</td>
                  </tr>
                  <tr>
                    <th>Measurement<br />Element</th>
                    <td colSpan="3">길이</td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-gray-round-wrap">
                <dl className="dl-gray-round-item">
                  <dt>Upper<br />Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br/>upper bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br />Lower Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Lower<br />Bound</dt>
                  <dd>10</dd>
                </dl>
              </div>

              <div className="dl-unit">
                <span>UOM</span><span>MM</span>
              </div>

              <hr className="border2" />

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Measured <br />Value</th>
                    <td colSpan="3">
                      <Counter type="typeWide" size={"12px"} />
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Comments</th>
                    <td colSpan="3">
                      <div className="comment-textarea">
                        <textarea placeholder="placeholder"></textarea>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-btn-wraps">
                <button className="btn-add" onClick={onWOTypeModal}>
                  <IconPlus fill="#A5B3C2" size="24px" />
                </button>
                <button className="btn-outline-gray">NG-Adjust</button>
                <button className="btn-outline-gray">OK</button>
              </div>
            </div>

            <div className="dl-footer-btn">
              <Button color={"primary"} fullWidth>
                NG-Adjust
              </Button>
            </div>
          </div>
          <div className="daily-list-item disabled">
            <div className="dl-header">
              <div className="title">
                <span>1.</span>D1CPKG011210
              </div>
              <div className="tag-round" style={{ display: "none" }}>
                110<span>&nbsp;P</span>
              </div>
              <div className={"btn-r-wrap"}>
                <button className="btn-NFC-tag" disabled={"disabled"}>
                  {
                    isNFCDisabled &&
                    <>
                      <IconNFCSmall fill={"#fff"} />
                      <IconClosePopup size={"16px"} fill={"#ffffff"} />
                    </>
                  }
                </button>
                <button className="btn-more">
                  <IconSelectArrow2 />
                </button>
              </div>
            </div>

            <hr className="border1" />

            <div className="dl-contents" style={{display:"none"}}>
              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Description</th>
                    <td colSpan="3">
                      Collar 체결 점검
                    </td>
                  </tr>
                  <tr>
                    <th>Detail</th>
                    <td colSpan="3">전구간 체결상태확인 채결...</td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td colSpan="3">2020.10.05 - 2020.10.05</td>
                  </tr>
                  <tr>
                    <th>Interval Type</th>
                    <td>Day</td>
                    <th>Interval</th>
                    <td>8</td>
                  </tr>
                  <tr>
                    <th>Measurement<br />Element</th>
                    <td colSpan="3">길이</td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-gray-round-wrap">
                <dl className="dl-gray-round-item">
                  <dt>Upper<br />Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br/>upper bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br />Lower Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Lower<br />Bound</dt>
                  <dd>10</dd>
                </dl>
              </div>

              <div className="dl-unit">
                <span>UOM</span><span>MM</span>
              </div>

              <hr className="border2" />

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Measured <br />Value</th>
                    <td colSpan="3">
                      <Counter type="typeWide" size={"12px"} />
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Comments</th>
                    <td colSpan="3">
                      <div className="comment-textarea">
                        <textarea placeholder="placeholder"></textarea>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-btn-wraps">
                <button className="btn-add" onClick={onWOTypeModal}>
                  <IconPlus fill="#A5B3C2" size="24px" />
                </button>
                <button className="btn-outline-gray">NG-Adjust</button>
                <button className="btn-outline-gray">OK</button>
              </div>
            </div>

            <div className="dl-footer-btn">
              <Button color={"primary"} fullWidth disabled>
                NG-Adjust
              </Button>
            </div>
          </div>

          <div className="daily-list-item">
            <div className="dl-header">
              <div className="title">
                <span>2.</span>D1CPKG011210
              </div>
              <div className={"btn-r-wrap"}>
                <button className="btn-NFC-tag" disabled={"disabled"}>
                  {
                    isNFCDisabled &&
                    <>
                      <IconNFCSmall fill={"#fff"} />
                      <IconClosePopup size={"16px"} fill={"#ffffff"} />
                    </>
                  }
                </button>
                <div className="tag-round">
                  110<span>&nbsp;P</span>
                </div>
              </div>
            </div>

            <GuideTooltip
              text={"해당설비의 NFC가 태깅되지 않았습니다."}
              position={"daily-check-detail-NFC"}
            />

            <hr className="border1" />

            <div className="dl-contents">
              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Description</th>
                    <td colSpan="3">
                      Collar 체결 점검
                    </td>
                  </tr>
                  <tr>
                    <th>Detail</th>
                    <td colSpan="3">전구간 체결상태확인 채결...</td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td colSpan="3">2020.10.05 - 2020.10.05</td>
                  </tr>
                  <tr>
                    <th>Interval Type</th>
                    <td>Day</td>
                    <th>Interval</th>
                    <td>8</td>
                  </tr>
                  <tr>
                    <th>Measurement<br />Element</th>
                    <td colSpan="3">길이</td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-gray-round-wrap">
                <dl className="dl-gray-round-item">
                  <dt>Upper<br />Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br/>upper bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Control<br />Lower Bound</dt>
                  <dd>10</dd>
                </dl>
                <dl className="dl-gray-round-item">
                  <dt>Lower<br />Bound</dt>
                  <dd>10</dd>
                </dl>
              </div>

              <div className="dl-unit">
                <span>UOM</span><span>MM</span>
              </div>

              <hr className="border2" />

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Measured <br />Value</th>
                    <td colSpan="3">
                      <Counter type="typeWide" size={"12px"} disabled={"disabled"}/>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="tb-type-white">
                <table>
                  <colgroup>
                    <col width="90px" />
                    <col width="*" />
                    <col width="30%" />
                    <col width="20%" />
                  </colgroup>
                  
                  <tbody>
                  <tr>
                    <th>Comments</th>
                    <td colSpan="3">
                      <div className="comment-textarea">
                        <textarea placeholder="placeholder" disabled></textarea>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>

              <div className="dl-btn-wraps">
                <button className="btn-add" onClick={onWOTypeModal}>
                  <IconPlus fill="#A5B3C2" size="24px" />
                </button>
                <button className="btn-outline-gray" disabled={"disabled"}>NG-Adjust</button>
                <button className="btn-outline-gray" disabled={"disabled"}>OK</button>
              </div>
            </div>

            <div className="dl-footer-btn" style={{ display: "none" }}>
              <Button fullWidth>OK</Button>
            </div>
          </div>
        </div>

        <div className="comm-fix-bottom div2">
          <div className="total-type">
            <span>23</span>/ 50
          </div>
          <Button onClick={onBackClick}>저장</Button>
        </div>
      </ContentsBox>
      {isRecodeModal &&
        (<DailyCheckModalVoiceRecording onClick={onRecodeModal}/>)
      }
      {isWOTypeModal &&
        (<DailyCheckSelectBoxModal onClick={onWOTypeModal}/>)
      }
      {isCommentModal &&
        (<DailyCheckModalComments onClick={onCommentModal}/>)
      }
    </ModalFullPage>
  );
};

export default DailyCheckWaitingInspectionList;
