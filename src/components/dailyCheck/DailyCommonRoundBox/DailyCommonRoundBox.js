import React from 'react';

const DailyCommonRoundBox = ({title,children,state,stateType}) => {
  return (
    <div className="common_round-box">
      <h2>
        {title}
        {state && <span className="status">{stateType}</span>}
      </h2>
      {children}
    </div>
  );
};

export default DailyCommonRoundBox;