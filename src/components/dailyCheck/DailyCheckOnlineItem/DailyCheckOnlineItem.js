import React from 'react';
import CheckBox from "../../common/CheckBox";
import RoundTag from "../../common/RoundTag";

const DailyCheckOnlineItem = ({value1,title,date,onChange,checked,point,tagName,roundTag,active,...rest}) => {
  return (
    <div className="dc-online-list-item">
      <div className="items-row">
        <CheckBox value1={value1} onChange={onChange} checked={checked} />
        <span className="title">{title}</span>
        <span className="date">{date}</span>
      </div>

      <div className="items-row">
        <span className="point">{point} P</span>
        {roundTag && <RoundTag tagName={tagName} active={active} />}
      </div>
    </div>
  );
};

export default DailyCheckOnlineItem;