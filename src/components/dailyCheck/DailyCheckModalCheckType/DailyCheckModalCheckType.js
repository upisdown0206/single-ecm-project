import React, { useState } from "react";
import cx from "classnames";
import DimedModal from "components/base/DimedModal";
import { IconListSelect } from "components/common/Icons";
import "./DailyCheckModalCheckType.scss";

const DailyCheckModalCheckType = ({ onClick, style }) => {
  const [options, setOptions] = useState([
    {
      id: 1,
      text: "All",
    },
    {
      id: 2,
      text: "My Check",
    },
    {
      id: 3,
      text: "Dept Check ",
    },
  ]);

  return (
    <DimedModal style={style}>
      <div className="work-order-select-wrapper" style={{ top: "auto" }}>
        <div className="modal-select-list" style={{ marginBottom: "0" }}>
          <ul className="select-items">
            {options.map((option, idx) => (
              <li key={idx} onClick={onClick}>
                <div
                  data-id={option.id}
                  className={cx("select-item", idx === 0 && "active")}
                >
                  {option.text}
                  {idx === 0 && <IconListSelect />}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </DimedModal>
  );
};

export default DailyCheckModalCheckType;
