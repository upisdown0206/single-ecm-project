import React from "react";

import "./FixedWrapper.scss";

const FixedWrapper = ({ children, ...rest }) => {
  return (
    <div className="fixed-wrapper" {...rest}>
      {children}
    </div>
  );
};

export default FixedWrapper;
