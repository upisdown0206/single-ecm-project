import React from "react";
import cx from "classnames";
import "./FixedFooter.scss";

const FixedFooter = ({ children, className }) => {
  return <footer className={cx("fixed-bottom", className)}>{children}</footer>;
};

export default FixedFooter;
