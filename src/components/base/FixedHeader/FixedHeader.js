import React from "react";
import cx from "classnames";
import "./FixedHeader.scss";

const FixedHeader = ({
  left,
  right,
  center,
  centerScrollX = false,
  checkDetail = false,
  blackMode = false,
}) => {
  return (
    <header className={cx("header", blackMode && "black-mode")}>
      <div className="left">{left}</div>
      <div className={cx("center", centerScrollX && "overflow")}>{center}</div>
      <div className={cx("right", checkDetail && "checkDetail")}>{right}</div>
    </header>
  );
};

export default FixedHeader;
