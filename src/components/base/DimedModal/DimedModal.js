import React from "react";
import cx from "classnames";

import "./DimedModal.scss";

const DimedModal = ({ children, className, ...rest }) => {
  return (
    <div className={cx("dimed-modal-wrapper", className)} {...rest}>
      {children}
    </div>
  );
};

export default DimedModal;
