import React from "react";
import { Link, withRouter } from "react-router-dom";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";

import {
  IconClosePopup,
  IconSearchEqpt,
  IconDailyCheck,
  IconListSelect,
  IconWorkOrder,
  IconHomeRegular,
  IconInventory,
  IconDailyInspection,
  IconRelease,
  IconInventoryCount,
  IconReturn,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import "./MenuNav.scss";

const MenuNav = withRouter(({ onMenuClick, location, className }) => {
  const { pathname } = location;
  return (
    <div className={cx("menu-panel", className)}>
      <FixedHeader center={<div className="menu-header-title">Menu</div>} />
      <div className="menu-inner">
        <div className="menu-navi">
          <ul>
            <li>
              <Link
                to="/spareparts"
                className={cx(
                  "menu-link",
                  pathname === "/spareparts" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconDailyCheck width="34" height="34" />
                  </IconBase>
                  <span>Spare Parts</span>
                </div>
                {pathname === "/spareparts" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/inventory"
                className={cx(
                  "menu-link",
                  pathname === "/inventory" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconInventory width="34" height="34" />
                  </IconBase>
                  <span>Inventory</span>
                </div>
                {pathname === "/inventory" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/equipment"
                className={cx(
                  "menu-link",
                  pathname === "/equipment" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconWorkOrder width="34" height="34" />
                  </IconBase>
                  <span>Equipment</span>
                </div>
                {pathname === "/equipment" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/workorder"
                className={cx(
                  "menu-link",
                  pathname === "/workorder" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconSearchEqpt width="34" height="34" />
                  </IconBase>
                  <span>WO</span>
                </div>
                {pathname === "/workorder" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/daily-check"
                className={cx(
                  "menu-link",
                  pathname === "/daily-check" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconDailyInspection width="34" height="34" />
                  </IconBase>
                  <span>Daily Check</span>
                </div>
                {pathname === "/daily-check" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/delivery"
                className={cx(
                  "menu-link",
                  pathname === "/delivery" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconRelease width="34" height="34" />
                  </IconBase>
                  <span>Delivery</span>
                </div>
                {pathname === "/delivery" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/stock-taking"
                className={cx(
                  "menu-link",
                  pathname === "/stock-taking" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconInventoryCount width="34" height="34" />
                  </IconBase>
                  <span>Stocktaking</span>
                </div>
                {pathname === "/stock-taking" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
            <li>
              <Link
                to="/return"
                className={cx(
                  "menu-link",
                  pathname === "/return" && "selected"
                )}
              >
                <div className="menu-content">
                  <IconBase>
                    <IconReturn width="34" height="34" />
                  </IconBase>
                  <span>Return</span>
                </div>
                {pathname === "/return" && (
                  <div className="select-icon-box">
                    <IconListSelect />
                  </div>
                )}
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className="circle-buttons">
        <Link to="/" className="circle bg-white" onClick={onMenuClick}>
          <IconHomeRegular width="40" height="40" />
        </Link>
        <div className="circle bg-gray" onClick={onMenuClick}>
          <IconClosePopup />
        </div>
      </div>
    </div>
  );
});

export default MenuNav;
