import React, { useState, useEffect } from "react";
import cx from "classnames";

import "./ScrollableModal.scss";

const CLOSE_MIN_DISTANCE = 50;

const ScrollableModal = ({ children, onTouchHandler, isDimmed = false }) => {
  const [isMove, setIsMove] = useState(false);
  const [swipe, setSwipe] = useState({ y: null });
  const [currentTop, setCurrentTop] = useState();

  const onTouchStart = (e) => {
    setIsMove(true);

    setSwipe({
      y: currentTop,
      height: e.touches[0],
    });
  };
  const onTouchMove = (e) => {
    if (e.changedTouches && e.changedTouches.length) {
      // limited top position: 56px;
      if (e.changedTouches[0].clientY >= 56) {
        setCurrentTop(e.changedTouches[0].clientY);
      }
    }
  };
  const onTouchEnd = (e) => {
    setIsMove(false);

    if (e.changedTouches && e.changedTouches.length) {
      // recognize close action (diff CLOSE_MIN_DISTANCE pixel)
      if (e.changedTouches[0].clientY - swipe.y > CLOSE_MIN_DISTANCE) {
        onTouchHandler();
      }
    }
    setCurrentTop(swipe.y);
  };

  useEffect(() => {
    const topValue = document
      .querySelector(".scrollable-modal-wrapper")
      .getBoundingClientRect().top;
    if (topValue < 56) {
      setCurrentTop(56);
    } else {
      setCurrentTop(topValue);
    }
  }, []);

  return (
    <div
      className={cx(
        "scrollable-modal-wrapper",
        !isMove && "anima-top",
        isDimmed && "before-dimmed"
      )}
      style={{ top: currentTop && `${currentTop}px` }}
    >
      <div
        className="modal-header"
        onTouchStart={onTouchStart}
        onTouchMove={onTouchMove}
        onTouchEnd={onTouchEnd}
      >
        <div className="button-wrapper">
          <div className="scrollable-button" />
        </div>
      </div>
      <div className="modal-inner">{children}</div>
    </div>
  );
};

export default ScrollableModal;
