import React from "react";
import cx from "classnames";

import MenuNav from "components/base/MenuNav";
import useDelayUnmount from "lib/hooks/useDelayUnmount";

import "./BaseMain.scss";

const BaseMain = ({
  isMenuOpen,
  isModalOpen,
  onMenuClick,
  children,
  className,
  ...rest
}) => {
  const shouldRender = useDelayUnmount(isMenuOpen, 300);

  return (
    <main
      className={cx(
        "search-main",
        className,
        (isMenuOpen || isModalOpen) && "modal-open"
      )}
      {...rest}
    >
      {shouldRender && (
        <MenuNav
          onMenuClick={onMenuClick}
          className={isMenuOpen ? "mounted" : "unmounted"}
        />
      )}
      {children}
    </main>
  );
};

export default BaseMain;
