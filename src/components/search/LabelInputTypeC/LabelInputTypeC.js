import React from "react";
import "./LabelInputTypeC.scss";

const LabelInputTypeC = ({ labelText, onToggleFocus,iconType }) => {
  return (
    <div className="label-input-typeC">
      <label>{iconType}{labelText}</label>
      <div
        className="input-box input-plain"
        onFocus={onToggleFocus}
        onBlur={onToggleFocus}
      >
        <div className="input-box-row">
          <textarea placeholder="Placeholder" />
        </div>
      </div>
    </div>
  );
};

export default LabelInputTypeC;
