import React from "react";
import "./LabelInputTypeB.scss";

const LabelInputTypeB = ({ labelText, onToggleFocus, style }) => {
  return (
    <div className="label-input-typeB" style={style}>
      <label>{labelText}</label>
      <div
        className="input-box input-plain"
        onFocus={onToggleFocus}
        onBlur={onToggleFocus}
      >
        <div className="input-box-row">
          <input type="text" placeholder="Placeholder" />
        </div>
      </div>
    </div>
  );
};

export default LabelInputTypeB;
