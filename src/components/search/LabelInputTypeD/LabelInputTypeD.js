import React, { useState } from "react";
import cx from "classnames";

import SearchKeywordsPanel from "components/search/SearchKeywordsPanel";

import {
  IconSearchMedium,
  IconArrowUpInput,
  IconArrowDownInput,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./LabelInputTypeD.scss";

const LabelInputTypeD = ({
  labelText,
  keywords,
  setKeywords,
  searchType,
  onInputClick,
  setKeyboardFocus,
  style,
  necessary,
}) => {
  const [isExpand, setIsExpand] = useState(false);
  const [isActiveFocus, setIsActiveFocus] = useState(false);
  const [value, setValue] = useState("");

  const onChange = (e) => {
    const {
      target: { value: inputValue },
    } = e;
    setValue(inputValue);
  };

  const onInputFocus = () => {
    if (setIsExpand) setIsExpand(true);
    if (setIsActiveFocus) setIsActiveFocus(true);
    if (setKeyboardFocus) setKeyboardFocus(true);
  };

  const onInputBlur = () => {
    if (setIsExpand) setIsExpand(false);
    if (setIsActiveFocus) setIsActiveFocus(false);
    if (setKeyboardFocus) setKeyboardFocus(false);
  };

  const onExpandCollapse = () => {
    setIsExpand(!isExpand);
    setIsActiveFocus(!isActiveFocus);
  };

  const onClick = (e) => {
    onInputClick(e, searchType, value);
  };

  // TODO: input key enter call onInputClick
  const onEnterKeyPress = (e) => {
    if (e.key === "Enter") {
      onInputClick(e, searchType, value);
      setValue("");
    }
  };

  return (
    <div className="label-input-typeD" style={style}>
      <label>
        {labelText}
        {necessary}
      </label>
      <div
        className={cx(
          "input-box",
          "input-search-icon",
          isActiveFocus && "active"
        )}
        onBlur={onInputBlur}
      >
        <div className="input-box-row">
          {!isExpand && (
            <SearchKeywordsPanel
              keywords={keywords}
              setKeywords={setKeywords}
              searchType={searchType}
            />
          )}
          <input
            type="text"
            onKeyPress={onEnterKeyPress}
            onFocus={onInputFocus}
            placeholder={
              keywords?.filter((keyword) => keyword.type === searchType)
                .length === 0 || isExpand
                ? "Placeholder"
                : ""
            }
            value={value}
            onChange={onChange}
          />
          <IconBase className="icon-left" onClick={onClick}>
            <IconSearchMedium />
          </IconBase>
          <IconBase className="icon-right" onClick={onExpandCollapse}>
            {isExpand ? <IconArrowUpInput /> : <IconArrowDownInput />}
          </IconBase>
        </div>
        {isExpand && (
          <div className="input-box-row no-icon">
            <div className="input-tag-item-wrapper">
              <SearchKeywordsPanel
                keywords={keywords}
                setKeywords={setKeywords}
                searchType={searchType}
                isWrap={true}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default LabelInputTypeD;
