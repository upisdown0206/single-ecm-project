import React from "react";
import cx from "classnames";

import SearchKeywordsPanel from "components/search/SearchKeywordsPanel";

import { IconSearchSmall } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SearchConditionPanelTypeB.scss";

const SearchConditionPanelTypeB = ({
  onSearchClick,
  keywords,
  setKeywords,
  searchType,
  isBorderBot = true,
  ...rest
}) => {
  return (
    <div
      className={cx("search-panel-typeB", !isBorderBot && "none-bord")}
      {...rest}
    >
      <div className="search-panel-overflow">
        <SearchKeywordsPanel
          keywords={keywords}
          setKeywords={setKeywords}
          searchType={searchType}
        />
      </div>
      {onSearchClick && (
        <IconBase className="search-icon" onClick={onSearchClick}>
          <IconSearchSmall />
        </IconBase>
      )}
    </div>
  );
};

export default SearchConditionPanelTypeB;
