import React from "react";
import "./SearchResultHeaderTypeA.scss";

const SearchResultHeaderTypeA = ({ label1, label2, label3, label4 }) => {
  return (
    <div className="search-result-header-typeA">
      <div className="header-row">
        <span className="em-no">{label1}</span>
        <span className="owning-dept">{label2}</span>
      </div>
      <div className="header-row">
        <span className="desc">{label3}</span>
        <span className="maker">{label4}</span>
      </div>
    </div>
  );
};

export default SearchResultHeaderTypeA;
