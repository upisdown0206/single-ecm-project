import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import ScrollableModal from "components/base/ScrollableModal";
import BaseMain from "components/base/BaseMain";
import NoResult from "components/common/NoResult";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";
import ModalItemRow from "components/common/ModalItemRow";

import SearchConditionPanelTypeA from "components/search/SearchConditionPanelTypeA";
import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";
import SearchResultHeaderTypeA from "components/search/SearchResultHeaderTypeA";
import SearchResultCardTypeA from "components/search/SearchResultCardTypeA";
import SearchResultCardTypeB from "components/search/SearchResultCardTypeB";
import LabelDivTypeA from "components/search/LabelDivTypeA";
import LabelDivTypeB from "components/search/LabelDivTypeB";
import LabelDivTypeC from "components/search/LabelDivTypeC";
import SearchResultItemTypeA from "components/search/SearchResultItemTypeA";

import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SearchEquipment.scss";

const SearchEquipment = ({ onSearchClick }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "owningDept", text: "조립1팀" },
    { id: "2", type: "owningDept", text: "조립2팀" },
    { id: "3", type: "owningDept", text: "조립3팀" },
    { id: "4", type: "owningDept", text: "조립4팀" },
    { id: "5", type: "owningDept", text: "조립5팀" },
    { id: "6", type: "owningDept", text: "조립6팀" },
    { id: "7", type: "equipmentId", text: "123444" },
    { id: "8", type: "equipmentId", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);
  const [results, setResults] = useState([
    {
      equipmentId: "P8TCVD0101",
      owningDept: "M0011-01",
      description: "PACKAGE",
      maker: "CA",
    },
    {
      equipmentId: "P8TCVD0102",
      owningDept: "M0011-01",
      description: "PACKAGE",
      maker: "CA",
    },
    {
      equipmentId: "P8TCVD0103",
      owningDept: "M0011-01",
      description: "PACKAGE",
      maker: "CA",
    },
    {
      equipmentId: "P8TCVD0104",
      owningDept: "M0011-01",
      description: "PACKAGE",
      maker: "CA",
    },
    {
      equipmentId: "P8TCVD0105",
      owningDept: "M0011-01",
      description: "PACKAGE",
      maker: "CA",
    },
    {
      equipmentId: "P8TCVD0106",
      owningDept: "M0011-01",
      description: "PACKAGE",
      maker: "CA",
    },
  ]);

  const [selectedIdx, setSelectedIdx] = useState(-1);

  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  const onSelect = (e, idx) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { id },
    } = currentTarget;

    if (idx === selectedIdx) {
      setSelectedIdx(-1);
    } else {
      setSelectedIdx(idx);
    }
  };

  const onCancelSelect = (e) => {
    console.log(e);
    setSelectedIdx(-1);
  };

  return (
    <div className="search-equipment">
      <FixedHeader
        left={
          <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
            {!isMenuOpen && <IconHamburger />}
          </IconBase>
        }
        center={
          <HeaderTitle text="Equipment">
            {results && <CircleText text={results.length} />}
          </HeaderTitle>
        }
      />

      <BaseMain
        isMenuOpen={isMenuOpen}
        isModalOpen={selectedIdx > -1}
        onMenuClick={onMenuClick}
        style={{
          height: `calc(100vh - (56px + 57px + 56px + 60px))`,
          marginTop: `calc(56px + 57px + 56px)`,
        }}
      >
        <FixedWrapper style={{ top: "56px" }}>
          <SearchConditionPanelTypeB
            onSearchClick={onSearchClick}
            keywords={keywords}
            setKeywords={setKeywords}
          />
          <SearchResultHeaderTypeA
            label1="Equipment ID"
            label2="Owning dept"
            label3="Description"
            label4="Maker"
          />
        </FixedWrapper>
        <div className="result-panel">
          {results?.length === 0 ? (
            <NoResult title1="No Result!" subTitle1="항목이 없습니다!" />
          ) : (
            <ul className="result-items">
              {results.map((result, idx) => (
                <li key={idx}>
                  <SearchResultItemTypeA
                    isActive={idx % 2 === 0 && idx === selectedIdx}
                    data-id={result.equipmentId}
                    onClick={(e) => onSelect(e, idx)}
                  >
                    {idx % 2 === 0 ? (
                      <SearchResultCardTypeA
                        value1={result.equipmentId}
                        value2={result.owningDept}
                        value3={result.description}
                        value4={result.maker}
                      />
                    ) : (
                      <SearchResultCardTypeB
                        value1={result.equipmentId}
                        value2={result.owningDept}
                        value3={result.description}
                        value4={result.maker}
                        isChecked={idx === selectedIdx}
                      />
                    )}
                  </SearchResultItemTypeA>
                </li>
              ))}
            </ul>
          )}
        </div>
      </BaseMain>
      {selectedIdx > -1 && (
        <ScrollableModal onTouchHandler={onCancelSelect}>
          <ModalItemRow>
            <LabelDivTypeA
              label="Description"
              value="Package-챔버-01-ABSDDGDD"
            />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeB label="Structure No" value="M0011-01" />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeB label="MES No" value="MESNO-01" />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeA
              label="Equipment type"
              value="Package-챔버-01-ABSDDGDD"
            />
          </ModalItemRow>
          <ModalItemRow isRowFlexBox={true}>
            <LabelDivTypeC label="WO List" value="url1" />
            <LabelDivTypeC label="S.P Issue history" value="url2" />
            <LabelDivTypeC label="Push history" value="url3" />
          </ModalItemRow>
        </ScrollableModal>
      )}
      <FixedFooter>
        <AppNavigation />
      </FixedFooter>
    </div>
  );
};

export default SearchEquipment;
