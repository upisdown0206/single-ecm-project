import React from "react";
import "./SearchResultCardTypeB.scss";

const SearchResultCardTypeB = ({
  value1,
  value2,
  value3,
  value4,
  isChecked = false,
}) => {
  return (
    <div className="search-result-card-typeB">
      <div className="check-box">
        <input
          type="checkbox"
          id={`cb${value1}`}
          checked={isChecked}
          readOnly
        />
        <label htmlFor={`cb${value1}`}></label>
      </div>
      <div className="item-col">
        <div className="item-row">
          <span className="value1">{value1}</span>
          <span className="value2">{value2}</span>
        </div>
        <div className="item-row">
          <span className="value3">{value3}</span>
          <span className="value4">{value4}</span>
        </div>
      </div>
    </div>
  );
};

export default SearchResultCardTypeB;
