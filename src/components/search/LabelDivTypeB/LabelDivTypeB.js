import React from "react";

import "./LabelDivTypeB.scss";

const LabelDivTypeB = ({ label, value }) => {
  return (
    <div className="label-div-typeB">
      <span className="info-label">{label}</span>
      <div className="info-gray-box">
        <span>{value}</span>
      </div>
    </div>
  );
};

export default LabelDivTypeB;
