import React from "react";

import Button from "components/common/Button";

import "./SearchButtonPanelTypeA.scss";

const SearchButtonPanelTypeA = ({
  onInputAllClearClick,
  clearNumber,
  btnColor,
  btnText,
  onClick,
  ...rest
}) => {
  return (
    <div className="search-button-panel-typeA">
      <div className="state-txt" onClick={onInputAllClearClick}>
        초기화 {clearNumber}
      </div>
      <Button
        color={btnColor}
        style={{ width: "156px", height: "48px" }}
        onClick={onClick}
      >
        {btnText}
      </Button>
    </div>
  );
};

export default SearchButtonPanelTypeA;
