import React from "react";
import cx from "classnames";

import { IconDelete12 } from "components/common/Icons";
import "./SearchKeywordsPanel.scss";

const SearchKeywordsPanel = ({
  keywords,
  setKeywords,
  searchType,
  isAddMargin = false,
  isWrap = false,
}) => {
  const onDeleteClick = (e) => {
    e.stopPropagation();
    const { currentTarget } = e;
    const {
      dataset: { id },
    } = currentTarget;
    if (id && setKeywords) {
      setKeywords(keywords?.filter((keyword) => keyword.id !== id));
    }
  };

  return (
    <div
      className={cx(
        "search-panel",
        keywords?.filter((keyword) =>
          searchType ? keyword.type === searchType : false
        ).length > 0 && "flex-auto"
      )}
    >
      <ul className={cx("search-items", isWrap && "wrap-box")}>
        {keywords
          ?.filter((keyword) =>
            searchType ? keyword.type === searchType : true
          )
          .map((keyword, idx) => (
            <li
              key={idx}
              className={cx("search-item", isAddMargin && "add-margin")}
            >
              <div className="search-circle-keyword">
                <span className="keyword-text">
                  <span>{keyword.text}</span>
                  {keyword.subText && (
                    <span className="search-sub-text">{keyword.subText}</span>
                  )}
                </span>
                <span
                  className="keyword-icon"
                  data-id={keyword.id}
                  onClick={onDeleteClick}
                >
                  <IconDelete12 />
                </span>
              </div>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default SearchKeywordsPanel;
