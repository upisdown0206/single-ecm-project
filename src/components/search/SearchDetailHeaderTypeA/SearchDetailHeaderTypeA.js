import React from "react";

import "./SearchDetailHeaderTypeA.scss";

const SearchDetailHeaderTypeA = ({
  searchInputValue,
  onChange,
  onEnterKeyPress,
  onFocus,
  onBlur,
}) => {
  return (
    <div className="search-detail-header-typeA">
      <input
        type="text"
        value={searchInputValue}
        onChange={onChange}
        onKeyPress={onEnterKeyPress}
        onFocus={onFocus}
        onBlur={onBlur}
        autoFocus
      />
    </div>
  );
};

export default SearchDetailHeaderTypeA;
