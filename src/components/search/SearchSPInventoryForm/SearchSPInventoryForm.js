import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeD from "components/search/LabelInputTypeD";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";
import FormDetail from "components/search/FormDetail";

import ModalFullPage from "components/common/ModalFullPage";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import { IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./SearchSPInventoryForm.scss";

const SearchSPInventoryForm = ({ onClose }) => {
  const [mode, setMode] = useState("init");
  const onInnerClose = () => {
    setMode("");
  };
  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");

  const [keywords, setKeywords] = useState([
    { id: "1", type: "storeroom", text: "central" },
    { id: "2", type: "storeroom", text: "central22" },
    { id: "7", type: "itemCode", text: "123444" },
    { id: "8", type: "itemCode", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const onBackClick = () => {
    // hide page
    onClose();
  };

  const onInputClick = (e, type, initValue = "") => {
    // render form-detail (type)
    setMode("FORM_DETAIL");
    setFormType(type);
    setSearchInitValue(initValue);
  };

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const onInputAllClearClick = () => {
    setKeywords([]);
  };

  return (
    <>
      <ModalFullPageAni
        className="search-sp-inventory-form"
        modeState={mode}
        isBaseComponent={true}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Search" />}
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + ${isKeyboardFocus ? `0px` : `79px`})`,
            marginTop: `calc(56px)`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeD
              labelText="Item Code"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="itemCode"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB
              labelText="Description"
              onInputClick={onInputClick}
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeD
              labelText="Bin"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="bin"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetail
          type={formType}
          onClose={onInnerClose}
          initValue={searchInitValue}
        />
      </ModalFullPageAni>
    </>
  );
};

export default SearchSPInventoryForm;
