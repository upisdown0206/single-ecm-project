import React from "react";

import { IconLocation } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import thumbnailImg from "asset/images/inventory-default-thumbnail.png";

import "./SearchResultCardInventoryTypeB.scss";

const SearchResultCardInventoryTypeB = ({
  itemCode,
  storeroom,
  description,
  quantity,
  quantityAvailable,
  thumbnail,
  ...rest
}) => {
  return (
    <div className="search-result-card-inventory-typeB" {...rest}>
      <div className="result-row">
        <div className="result-col">
          <div className="item-row">
            <span className="item-code">
              {itemCode?.length > 15
                ? itemCode.substr(0, 15) + "..."
                : itemCode}
            </span>
            <div className="store-room">
              <IconBase>
                <IconLocation width="16" height="16" fill="#E0205C" />
              </IconBase>
              <span className="text">
                {storeroom?.length > 12
                  ? storeroom.substr(0, 12) + "..."
                  : storeroom}
              </span>
            </div>
          </div>
          <div className="item-row">
            <span className="desc">
              {description?.length > 40
                ? description.substr(0, 40) + "..."
                : description}
            </span>
          </div>
        </div>
        <div className="result-col">
          {thumbnail && (
            <div className="item-thumbnail-box">
              <img src={thumbnailImg} alt="item thumbnail" />
            </div>
          )}
        </div>
      </div>
      <div className="result-row">
        <div className="item-row flex-row">
          <div className="item-col">
            <span className="quantity-label">Quantity</span>
            <span className="quantity-value">{quantity}</span>
          </div>
          <div className="splitter" />
          <div className="item-col">
            <span className="quantity-label">Quantity Available</span>
            <span className="quantity-value">{quantityAvailable}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchResultCardInventoryTypeB;
