import React from "react";
import cx from "classnames";

import "./SearchResultItemTypeA.scss";

const SearchResultItemTypeA = ({ isActive, children, ...rest }) => {
  return (
    <div
      className={cx("search-result-item-typeA", isActive && "active")}
      {...rest}
    >
      {children}
    </div>
  );
};

export default SearchResultItemTypeA;
