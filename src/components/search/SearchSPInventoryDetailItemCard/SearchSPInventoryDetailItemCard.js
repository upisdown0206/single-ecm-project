import React from "react";

import "./SearchSPInventoryDetailItemCard.scss";

const SearchSPInventoryDetailItemCard = ({
  bin,
  lotId,
  conditionCode,
  quantity,
  quantityAvailable,
}) => {
  return (
    <div className="search-sp-inventory-detail-item-card">
      <div className="row">
        <div className="col">
          <span className="label">Condition Code</span>
          <span className="value">{conditionCode}</span>
        </div>
        <div className="col">
          <span className="label">Bin</span>
          <span className="value">{bin}</span>
        </div>
      </div>
      <div className="row-flex">
        <div className="col-sb">
          <span className="label">Quantity</span>
          <span className="value">{quantity}</span>
        </div>
        <div className="splitter" />
        <div className="col-sb">
          <span className="label">Quantity</span>
          <span className="value">{quantityAvailable}</span>
        </div>
      </div>
    </div>
  );
};

export default SearchSPInventoryDetailItemCard;
