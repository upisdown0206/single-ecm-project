import React, { useEffect, useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import FixedFooter from "components/base/FixedFooter";
import BaseFormDetailMain from "components/base/BaseFormDetailMain";

import { IconCommBack, IconSearchBig } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import SearchDetailHeaderTypeA from "components/search/SearchDetailHeaderTypeA";
import SearchKeywordsResultItem from "components/search/SearchKeywordsResultItem";
import SearchConditionPanelTypeA from "components/search/SearchConditionPanelTypeA";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";

//test
import { testData } from "lib/api/test";

import "./FormDetailOnlyOne.scss";

const FormDetailOnlyOne = ({
  type,
  onClose,
  initValue = "",
  isExpandMode = false,
}) => {
  const [keywords] = useState([
    { id: "1", type: "owningDept", text: "조립1팀" },
    { id: "2", type: "owningDept", text: "조립2팀" },
    { id: "3", type: "owningDept", text: "조립3팀" },
    { id: "4", type: "owningDept", text: "조립4팀" },
    { id: "5", type: "owningDept", text: "조립5팀" },
    { id: "6", type: "owningDept", text: "조립6팀" },
    { id: "7", type: "equipmentId", text: "123444" },
    { id: "8", type: "equipmentId", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);

  const [testApiData] = useState(testData);

  const [searchKeyword, setSearchKeyword] = useState("");
  const [searchInputValue, setSearchInputValue] = useState(initValue);
  const [searchType, setSearchType] = useState();
  const [searchTerm, setSearchTerm] = useState("");

  const onSearchFormBack = () => {
    // hide page
    onClose();
  };
  const onSearchConfirm = () => {
    // hide page
  };

  const onEnterKeyPress = (e) => {
    if (e.key === "Enter") {
      setSearchTerm(searchInputValue);
    }
  };

  const onSearch = () => {
    setSearchTerm(searchInputValue);
  };

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    setSearchInputValue(value);
  };

  const onAddClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { id },
    } = currentTarget;

    setSearchKeyword(id);
    onClose();
  };

  const initialize = () => {
    setSearchType(type);

    if (initValue) {
      onSearch();
    }
  };

  useEffect(() => {
    initialize();
  }, []);

  return (
    <div className="search-form">
      <FixedHeader
        left={
          <IconBase onClick={onSearchFormBack}>
            <IconCommBack />
          </IconBase>
        }
        center={
          keywords && (
            <SearchDetailHeaderTypeA
              searchInputValue={searchInputValue}
              onChange={onChange}
              onEnterKeyPress={onEnterKeyPress}
            />
          )
        }
        right={
          <IconBase onClick={onSearch}>
            <IconSearchBig />
          </IconBase>
        }
        centerScrollX={true}
      />
      <BaseFormDetailMain
        isAddedPanel={false}
        style={{
          height: "calc(100vh - (56px + 79px))",
        }}
      >
        {searchTerm && (
          <ul>
            {testApiData
              ?.filter(
                (item) =>
                  item.type === searchType && item.text.indexOf(searchTerm) > -1
              )
              .map((item, idx) => (
                <li key={idx}>
                  <SearchKeywordsResultItem
                    {...item}
                    isExpandMode={isExpandMode}
                    data-id={item.id}
                    onClick={onAddClick}
                  />
                </li>
              ))}
          </ul>
        )}
      </BaseFormDetailMain>
    </div>
  );
};

export default FormDetailOnlyOne;
