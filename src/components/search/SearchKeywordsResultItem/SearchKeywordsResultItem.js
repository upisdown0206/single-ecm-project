import React from "react";

import "./SearchKeywordsResultItem.scss";

const SearchKeywordsResultItem = ({
  id,
  text,
  subText,
  subId,
  isExpandMode = false,
  ...rest
}) => {
  return (
    <>
      {isExpandMode ? (
        <div className="search-keywords-result-item-expand-mode" {...rest}>
          {/* <span className="item-id">{id}</span> */}
          <div className="row">
            <span className="item-text">{text}</span>
            {/* <span className="box-text">{subId}</span> */}
          </div>
          <div className="row">
            <span className="sub-text">{subText}</span>
          </div>
        </div>
      ) : (
        <div className="search-keywords-result-item" {...rest}>
          <span className="item-id">{id}</span>
          <span className="item-text">{text}</span>
        </div>
      )}
    </>
  );
};

export default SearchKeywordsResultItem;
