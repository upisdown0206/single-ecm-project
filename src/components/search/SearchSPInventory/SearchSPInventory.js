import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";
import NoResult from "components/common/NoResult";

import SearchResultCardInventoryTypeA from "components/search/SearchResultCardInventoryTypeA";
import SearchResultCardInventoryTypeB from "components/search/SearchResultCardInventoryTypeB";
import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";
import SearchResultItemTypeA from "components/search/SearchResultItemTypeA";

import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./SearchSPInventory.scss";

const SearchSPInventory = ({ onSearchClick, onDetailClick }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "storeroom", text: "central" },
    { id: "BMBOLT0001", type: "itemCode", text: "BMBOLT0001" },
    { id: "BMBOLT0002", type: "itemCode", text: "BMBOLT0002" },
    { id: "BMBOLT0003", type: "itemCode", text: "BMBOLT0003" },
    // { id: "5", type: "quantity", 0: "aaa" },
    // { id: "6", type: "quantity", 0: "bbb" },
  ]);
  const [results, setResults] = useState([
    {
      itemCode: "BMBOLT000111111111111",
      storeroom: "centralcentralcentral",
      description: "Actuator, Fettle, Premium linear",
      quantity: "000",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0002",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000HBolt, Mobile, A109K TTTESTTE",
      quantity: "1",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0003",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantity: "000",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0004",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantity: "1",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0005",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantity: "000",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0006",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantity: "000",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0007",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantity: "000",
      quantityAvailable: "992",
    },
  ]);

  const [selectedIdx, setSelectedIdx] = useState(-1);

  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <div className="search-sp-inventory">
      <FixedHeader
        left={
          <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
            {!isMenuOpen && <IconHamburger />}
          </IconBase>
        }
        center={
          <HeaderTitle text="Inventory">
            {results && <CircleText text={results.length} />}
          </HeaderTitle>
        }
      />
      <BaseMain
        isMenuOpen={isMenuOpen}
        isModalOpen={selectedIdx > -1}
        onMenuClick={onMenuClick}
        style={{
          height: `calc(100vh - (56px + 56px + 60px))`,
          marginTop: `calc(56px + 56px)`,
        }}
      >
        <FixedWrapper style={{ top: "56px" }}>
          <SearchConditionPanelTypeB
            onSearchClick={onSearchClick}
            keywords={keywords}
            setKeywords={setKeywords}
          />
        </FixedWrapper>
        <div className="result-panel">
          {results?.length === 0 ? (
            <NoResult title1="No Result!" subTitle1="항목이 없습니다!" />
          ) : (
            <ul className="result-items">
              {results.map((result, idx) => (
                <li key={idx}>
                  <SearchResultItemTypeA
                    isActive={idx === selectedIdx}
                    data-id={result.equipmentId}
                    onClick={() => onDetailClick(result.itemCode)}
                  >
                    {idx >= results.length / 2 ? (
                      <SearchResultCardInventoryTypeA
                        itemCode={result.itemCode}
                        storeroom={result.storeroom}
                        description={result.description}
                        quantity={result.quantity}
                        quantityAvailable={result.quantityAvailable}
                        thumbnail={idx % 2 === 0 && "thumbnail-test"}
                      />
                    ) : (
                      <SearchResultCardInventoryTypeB
                        itemCode={result.itemCode}
                        storeroom={result.storeroom}
                        description={result.description}
                        quantity={result.quantity}
                        quantityAvailable={result.quantityAvailable}
                        thumbnail={idx % 2 === 0 && "thumbnail-test"}
                      />
                    )}
                  </SearchResultItemTypeA>
                </li>
              ))}
            </ul>
          )}
        </div>
      </BaseMain>
      <FixedFooter>
        <AppNavigation />
      </FixedFooter>
    </div>
  );
};

export default SearchSPInventory;
