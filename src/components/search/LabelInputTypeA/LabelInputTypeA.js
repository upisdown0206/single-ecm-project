import React from "react";

import SearchKeywordsPanel from "components/search/SearchKeywordsPanel";

import { IconSearchMedium } from "components/common/Icons";

import "./LabelInputTypeA.scss";

const LabelInputTypeA = ({
  labelText,
  keywords,
  keywordType,
  onInputClick,
  onToggleFocus,
  necessary
}) => {
  return (
    <div className="label-input-typeA">
      <label>{labelText}{necessary}</label>
      <div
        className="input-box input-search-icon"
        onClick={onInputClick}
        data-type={keywordType}
        onFocus={onToggleFocus}
        onBlur={onToggleFocus}
      >
        <div className="input-box-row">
          <SearchKeywordsPanel
            keywords={keywords?.filter(
              (keyword) => keyword.type === keywordType
            )}
          />
          <input
            type="text"
            placeholder={
              keywords?.filter((keyword) => keyword.type === keywordType)
                .length > 0
                ? ""
                : "Placeholder"
            }
          />
          <IconSearchMedium />
        </div>
      </div>
    </div>
  );
};

export default LabelInputTypeA;
