import React, { useState } from "react";
import StockTakingLanding from "components/stocktaking/StockTakingLanding";
import StockTakingSearchForm from "components/stocktaking/StockTakingSearchForm";

import ModalFullPageAni from "components/common/ModalFullPageAni";

const StockTaking = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };
  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };
  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <StockTakingLanding onSearchClick={onSearchClick} />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <StockTakingSearchForm onBackClick={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default StockTaking;
