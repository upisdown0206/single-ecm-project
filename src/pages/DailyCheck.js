import React, { useState } from "react";
import "components/dailyCheck/dc-common/DailyCheckCommon.scss";

import FixedHeader from "components/base/FixedHeader";
import IconBase from "components/common/IconBase";
import {
  IconList,
  IconHamburger,
  IconTabCheckFull,
  IconTabCheckLine,
} from "components/common/Icons";
import MenuNav from "components/base/MenuNav";
import ContentsBox from "components/common/ContentsBox";
import DailyCheckTab, {
  DailyCheckTabButton,
} from "components/dailyCheck/dc-common/DailyCheckTab/DailyCheckTab";
import DailyCheckEmpty from "components/dailyCheck/DailyCheckEmpty";
import cx from "classnames";
import DailyCheckOnLineList from "components/dailyCheck/DailyCheckOnLineList";
import DailyCheckModalDownload from "components/dailyCheck/DailyCheckModalDownload";
import DailyCheckWaitingInspection from "components/dailyCheck/DailyCheckWaitingInspection";
import NoticeAlarm from "components/common/NoticeAlarm";
import DailyCheckFilterSearchForm from "components/dailyCheck/DailyCheckFilterSearchForm";
import DailyCheckWaitingInspectionList from "components/dailyCheck/DailyCheckWaitingInspectionList";
import { SnackbarProvider, useSnackbar } from "notistack";

const DailyCheck = () => {
  const [mode, setMode] = useState("");
  const onCheckStart = () => {
    setMode("DAILY_CHECK_WAITING_INSPECTION");
  };

  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  const [activeTab, setActiveTab] = useState("EXECUTION");
  const handleTab = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  const [downloadConFirm, setDownloadConFirm] = useState(false);
  const onDownloadConfirmClick = () => {
    setTotalNumber(999);
    setDownloadConFirm(false);
  };
  const onLineDownloadPage = () => {
    setActiveTab("EXECUTION");
  };

  const [onLineFilter, setOnLineFilter] = useState(false);
  const onLineFilterPage = () => {
    setOnLineFilter(!onLineFilter);
  };

  const onModalCancel = () => {
    setDownloadConFirm(false);
  };
  const onToggle = () => {
    setDownloadConFirm(!downloadConFirm);
  };
  const onClose = () => {
    setOnLineFilter(!onLineFilter);
  };
  const onBackClick = () => {
    setMode("");
  };

  const [totalNumber, setTotalNumber] = useState(null);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = () => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar("[PM] WO-123675415:PSTART0205:7999", {
      autoHideDuration: 2000,
      anchorOrigin: { vertical: "top", horizontal: "center" },
      content: (key, message) => (
        <div
          id={key}
          style={{ position: "relative", top: 56, width: "100%" }}
          onClick={() => closeSnackbar(key)}
        >
          <NoticeAlarm alarmTitle={"WO 발생"} alarmText={message} blackType />
        </div>
      ),
    });
  };

  return (
    <>
      <FixedHeader
        left={
          <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
            {!isMenuOpen && <IconHamburger />}
          </IconBase>
        }
        center={
          <div className="daily-check-title" onClick={onShowMessage}>
            {/*알림 테스트 이벤트*/}
            <span>Daily Check Planning</span>
          </div>
        }
      />
      {isMenuOpen && <MenuNav onMenuClick={onMenuClick} />}

      <ContentsBox bgType={"bg-type"}>
        <DailyCheckTab tabType={activeTab === "LIST" ? "moving" : null}>
          <DailyCheckTabButton
            tabTitle="Execution"
            icon={
              activeTab === "EXECUTION" ? (
                <IconTabCheckFull />
              ) : (
                <IconTabCheckLine />
              )
            }
            className={cx(activeTab === "EXECUTION" && "on")}
            onClick={() => handleTab("EXECUTION")}
            totalNumbers={totalNumber}
          />

          <DailyCheckTabButton
            tabTitle="List"
            icon={
              <IconList
                fill={`${activeTab === "LIST" ? "#ffffff" : "#6B7682"}`}
              />
            }
            className={cx(activeTab === "LIST" && "on")}
            onClick={() => handleTab("LIST")}
            totalNumbers={totalNumber}
          />
        </DailyCheckTab>

        {/* Tab 1: EXECUTION */}
        {activeTab === "EXECUTION" && (
          <>
            {!totalNumber ? (
              <DailyCheckEmpty onClick={onToggle} />
            ) : (
              <>
                {/* 점검대기 */}
                <DailyCheckWaitingInspection onCheckStart={onCheckStart} />
                {/* 점검시작 => 점검디테일/목록 */}
                {mode === "DAILY_CHECK_WAITING_INSPECTION" && (
                  <DailyCheckWaitingInspectionList
                    onBackClick={onBackClick}
                    onLineDownloadPage={onLineDownloadPage}
                  />
                )}
              </>
            )}

            {/* 다운로드 대상 선택 모달
                :: 재 다운로드 시도 시  DailyCheckModalFail
                :: 인터넷 연갈 안될 시  DailyCheckModalConnect
            */}
            {downloadConFirm && (
              <DailyCheckModalDownload
                onCancel={onModalCancel}
                onClick={onDownloadConfirmClick}
              />
            )}
          </>
        )}
        {/* Tab 2: List */}
        {activeTab === "LIST" && (
          <>
            <DailyCheckOnLineList
              onClick={onLineFilterPage}
              onLineDownloadPage={onLineDownloadPage}
            />
            {onLineFilter && (
              <DailyCheckFilterSearchForm onClick={onLineFilterPage} />
            )}
            {/* 검색결과 없을 시 <DailyCheckResultEmpty onClick={onToggle} />*/}
          </>
        )}
      </ContentsBox>
    </>
  );
};

export default DailyCheck;
