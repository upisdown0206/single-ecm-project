import React, { useState } from "react";
import ReturnLanding from "components/return/ReturnLanding";
import ReturnSearchForm from "components/return/ReturnSearchForm";

import ModalFullPageAni from "components/common/ModalFullPageAni";

const Return = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };
  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };
  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <ReturnLanding onSearchClick={onSearchClick} />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <ReturnSearchForm onBackClick={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default Return;
