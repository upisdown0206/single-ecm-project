# 공상평 ECM 프로젝트 (Digital Prototype)

## module install

```bash
yarn
```

```bash
npm install
```

## react run

```bash
yarn start
```

```bash
npm run start
```

## libs

- node-sass
- react-router-dom
- classnames
- styled-components ( for guidefile )
- rc-time-picker, react-datepicker (date input, calendar)
- react-lottie (animation .json)
- react-elastic-carousel
- react-minimal-pie-chart
- react-use-gesture, object-assign (signature)
